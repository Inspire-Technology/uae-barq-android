package com.uaebarq.uaebarq;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.Window;
import android.view.WindowManager;

import com.google.firebase.perf.FirebasePerformance;
import com.google.firebase.perf.metrics.AddTrace;
import com.google.firebase.perf.metrics.Trace;
import com.uaebarq.uaebarq.models.MobiSdkModel.MobiSdkModel;
import com.uaebarq.uaebarq.utils.FireBaseEvents;
import com.uaebarq.uaebarq.utils.Utils;

import retrofit2.Call;

public class SplashActivity extends AppCompatActivity {

    private Intent intent;
    public MobiSdkModel mobiSdkModel;
    Call<MobiSdkModel> mobiSdkModelCall;
    public static final String tag = "SplashActivity";

    @Override
    @AddTrace(name = "onCreateTrace", enabled = true /* optional */)
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        super.onCreate(savedInstanceState);

        Utils.trackEvent(this.getClass().getSimpleName(), FireBaseEvents.splashStartApp);

        setContentView(R.layout.splash_activty);
        //Crashlytics.getInstance().crash();
        Trace myTrace = FirebasePerformance.getInstance().newTrace("test_trace");
        myTrace.start();

        setUp();
    }

    private void setUp() {
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                homeScreen();
            }
        }, 1000);
    }


    private void homeScreen() {

        Utils.trackEvent(this.getClass().getSimpleName(), FireBaseEvents.splashHomeScreenOpen);
        intent = new Intent(SplashActivity.this, HomeActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    protected void onResume() {
        super.onResume();
        Utils.trackScreenNew(this.getClass().getSimpleName(), this);
    }
}
