package com.uaebarq.uaebarq;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.github.silvestrpredko.dotprogressbar.DotProgressBar;
import com.squareup.picasso.Picasso;
import com.uaebarq.uaebarq.adapter.AllCategoryListAdapter;
import com.uaebarq.uaebarq.models.AllCategoryListModel.AllCategoryListModel;
import com.uaebarq.uaebarq.models.AllCategoryListModel.Post;
import com.uaebarq.uaebarq.models.DashBoardModel.Category;
import com.uaebarq.uaebarq.retrofit.NetworkInterface.NetworkPostServices;
import com.uaebarq.uaebarq.singleton.Singleton;
import com.uaebarq.uaebarq.utils.Constant;
import com.uaebarq.uaebarq.utils.EndlessScrollListener;
import com.uaebarq.uaebarq.utils.FireBaseEvents;
import com.uaebarq.uaebarq.utils.UaeBarqUtils;
import com.uaebarq.uaebarq.utils.Utils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.uaebarq.uaebarq.ApplicationData.gcmCategory;

public class AllCategoryListActivity extends BaseActivity {

    @BindView(R.id.tv_name)
    TextView tvName;

    @BindView(R.id.iv_logo)
    ImageView ivLogo;

    @BindView(R.id.fv_list)
    ListView fvList;

    @BindView(R.id.progress_bar)
    LinearLayout progressBar;

    @BindView(R.id.rl_main)
    RelativeLayout rlMain;

    @BindView(R.id.progress_layout)
    View progressLayout;

    @BindView(R.id.empty_view)
    View emptyView;

    @BindView(R.id.hs_tabs)
    HorizontalScrollView hsTab;

    public static RelativeLayout rlBlack;
    private ArrayList<Post> categoryPayloadArrayList;
    private Category category;
    private Call<AllCategoryListModel> allCategoryListModelCall;
    private AllCategoryListAdapter allCategoryListAdapter;
    @BindView(R.id.dot_progress_bar)
    DotProgressBar dotProgressBar;
    private LayoutInflater layoutInflater;
    @BindView(R.id.ll_tabs)
    LinearLayout llTab;
    @BindView(R.id.iv_footer)
    ImageView ivFooter;
    @BindView(R.id.tv_footer_text)
    TextView tvFooter;
    @BindView(R.id.main_footer_ll)
    View main_footer_ll;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        layoutInflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
        rlBlack = findViewById(R.id.rl_black);
        ivLogo.setVisibility(View.GONE);
        tvName.setVisibility(View.VISIBLE);
        categoryPayloadArrayList = new ArrayList<>();
        Bundle intent = getIntent().getExtras();
        if (intent != null) {
            category = (Category) intent.getSerializable("catObject");
            if (category != null) {
                tvName.setText(category.getTitle());
                dotProgressBar.setEndColor(ContextCompat.getColor(this, R.color.tab_gray));
                dotProgressBar.setStartColor(ContextCompat.getColor(this, R.color.tab_gray));
                getAllCategoryListData(0);
            }
        }

        setTabs(Singleton.getInstance().tabCategoriesList);
        if (isEnglishLanguage()) {
            UaeBarqUtils.FontUtils.applyFonts(this, Constant.strTypefaceEnglishMedium, tvName);
        } else {
            UaeBarqUtils.FontUtils.applyFonts(this, Constant.strTypefaceArabicMedium, tvName);
        }

        fvList.setOnScrollListener(mEndlessScrollListener);
        setAdapter();
    }

    private EndlessScrollListener mEndlessScrollListener = new EndlessScrollListener() {
        @Override
        public void onLoadMore(int page, int totalItemsCount) {
            // Triggered only when new data needs to be appended to the list
            // Add whatever code is needed to append new items to your AdapterView
            int value = page;
            if (value >= 3) {
                progressBar.setVisibility(View.VISIBLE);
            }
            getAllCategoryListData(value);
            // or customLoadMoreDataFromApi(totalItemsCount);
        }
    };

    private void getAllCategoryListData(int page) {
        if (isOnline()) {
            getAllCategory(page);
        } else {
            UaeBarqUtils.getAppUtils(this).snakeBar(rlMain, getResources().getString(R.string.please_check_internet));
        }
    }

    private void getAllCategory(int offSet) {
        allCategoryListModelCall = ApplicationData.getInstance().getRestClient().createService(NetworkPostServices.class)
                .getAllCategoryListData(Integer.toString(Singleton.getInstance().limit),
                        Integer.toString(offSet),
                        UaeBarqUtils.getAppUtils(this).getCurrentLang(this),
                        String.valueOf(category.getId()), getUserId());
        allCategoryListModelCall.enqueue(new Callback<AllCategoryListModel>() {
            @Override
            public void onResponse(Call<AllCategoryListModel> call, Response<AllCategoryListModel> response) {
                try {
                    progressLayout.setVisibility(View.GONE);
                    if (response.body().getPayload().getPosts().size() > 0 && response.body() != null && response.body().getSuccess().equalsIgnoreCase("true")) {
                        categoryPayloadArrayList.addAll(response.body().getPayload().getPosts());
                        fvList.setVisibility(View.VISIBLE);
                        allCategoryListAdapter.notifyDataSetChanged();
                        if(isPremiumUser()){
                            main_footer_ll.setVisibility(View.GONE);
                        }else {
                            setFooter(response.body().getPayload().getBanner());
                        }
                    } else {
                        if (categoryPayloadArrayList.size() > 0) {
                            emptyView.setVisibility(View.GONE);
                        } else {
                            UaeBarqUtils.getAppUtils(getApplicationContext()).showToast(getApplicationContext(), response.body().getMessage());

                            Utils.trackEvent(this.getClass().getSimpleName(), FireBaseEvents.allcategoryScreenOpenSettingActivity);
                            Intent intent = new Intent(AllCategoryListActivity.this, SettingsActivity.class);
                            startActivity(intent);
                            emptyView.setVisibility(View.VISIBLE);
                        }
                    }
                    progressBar.setVisibility(View.GONE);
                } catch (Exception e) {
                    e.printStackTrace();
                    emptyView.setVisibility(View.VISIBLE);
                    progressBar.setVisibility(View.GONE);
                }
            }

            @Override
            public void onFailure(Call<AllCategoryListModel> call, Throwable t) {
                progressLayout.setVisibility(View.GONE);
                if(categoryPayloadArrayList.size() == 0){
                    emptyView.setVisibility(View.VISIBLE);
                }

            }
        });
    }

    void setAdapter() {
        allCategoryListAdapter = new AllCategoryListAdapter(this, 0, categoryPayloadArrayList);
        fvList.setAdapter(allCategoryListAdapter);
    }

    private void setFooter(List<com.uaebarq.uaebarq.models.AllCategoryListModel.Banner> banners) {
        if (banners != null ) {
            for (int i = 0; i < banners.size(); i++) {
                if (i == 0) {
                    tvFooter.setText(banners.get(0).getText());
                    Picasso.with(this)
                            .load(banners.get(0).getImge())
                            .into(ivFooter);
                }
                Singleton.getInstance().bannerImage = banners.get(0).getUrl();
                Singleton.getInstance().bannerText = banners.get(0).getText();
            }
        }else {
            main_footer_ll.setVisibility(View.GONE);
        }
        if (isEnglishLanguage()) {
            UaeBarqUtils.FontUtils.applyFonts(this, Constant.strTypefaceEnglishMedium, tvFooter);
        } else {
            UaeBarqUtils.FontUtils.applyFonts(this, Constant.strTypefaceArabicMedium, tvFooter);
        }
    }

    /*set Tabs item*/
    private void setTabs(final ArrayList<Category> categories) {
        View convertView;
        LinearLayout llMain;
        boolean isTrue = true;
        if (categories.size() > 0) {
            for (int value = 0; value < categories.size(); value++) {
                convertView = layoutInflater.inflate(R.layout.tabs_item, null);
                llMain = convertView.findViewById(R.id.ll_main);
                final TextView tvCategoryTab = convertView.findViewById(R.id.tv_category_tab);
                if (categories.get(value).getId().equals(category.getId())) {
                    tvCategoryTab.setTextColor(ContextCompat.getColor(this, R.color.tab_green));
                }
                tvCategoryTab.setText(categories.get(value).getTitle());
                final Category category = categories.get(value);
                convertView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (category.getId() == 13 ||  category.getId() == 8) {
                            Utils.trackEvent(this.getClass().getSimpleName(), FireBaseEvents.allcategoryScreenOpenWeatherActivity);
                            Intent intent = new Intent(AllCategoryListActivity.this, WeatherActivity.class);
                            startActivity(intent);
                        } else {

                            Utils.trackEvent(this.getClass().getSimpleName(), FireBaseEvents.allcategoryScreenRefreshOpenAllCategoryActivity);
                            Intent intent = new Intent(AllCategoryListActivity.this, AllCategoryListActivity.class);
                            intent.putExtra("catObject", category);
                            finish();
                            overridePendingTransition(0, 0);
                            startActivity(intent);
                            overridePendingTransition(0, 0);
                        }
                    }
                });
                if (isEnglishLanguage()) {
                    UaeBarqUtils.FontUtils.applyFonts(this, Constant.strTypefaceEnglishMedium, llMain);
                } else {
                    UaeBarqUtils.FontUtils.applyFonts(this, Constant.strTypefaceArabicMedium, llMain);
                    if (isTrue) {
                        hsTab.postDelayed(new Runnable() {
                            public void run() {
                                // Disable the animation First
                                hsTab.setSmoothScrollingEnabled(false);
                                hsTab.fullScroll(HorizontalScrollView.FOCUS_RIGHT);
                                hsTab.setSmoothScrollingEnabled(true);
                            }
                        }, 100L);
                        isTrue = false;
                    }
                }
                llTab.addView(convertView);
            }
        }
    }

    @OnClick(R.id.main_footer)
    void footerClick() {

        Utils.trackEvent(this.getClass().getSimpleName(), FireBaseEvents.allcategoryScreenFooterClickOpenWebView);
        Intent webIntent = new Intent(AllCategoryListActivity.this, WebViewActivity.class);
        webIntent.putExtra("index", 1);
        startActivity(webIntent);
    }

    @Override
    protected int setLayoutId() {
        return R.layout.all_cat_list_activity;
    }


    @OnClick(R.id.iv_back)
    public void backPress() {
        sendBack();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        sendBack();
    }


    private void sendBack() {
        gcmCategory = false;
        finish();
    }
}