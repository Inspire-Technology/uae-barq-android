package com.uaebarq.uaebarq.utils;

public class Constant {

    public static final String strTypefaceEnglishRegular = "Colfax-Regular.otf";
    public static final String strTypefaceEnglishMedium = "Colfax-Medium.otf";
    public static final String strTypefaceArabicRegular = "Dubai-Regular.ttf";
    public static final String strTypefaceArabicMedium = "Dubai-Medium.ttf";

    public static String API_KEY = "RgMoPQdfl0fawSPgSJ8mNXWZbk9WqSVS";
    public static String TOKEN = "2562836458236458728345";
    public static String DEVICE_TYPE = "android";

    public static String facebookLink = "https://www.facebook.com/uaebarq/";
    public static String FACEBOOK_PAGE_ID = "uaebarq";
    public static String twitterLink = "https://twitter.com/UAE_BARQ";
    public static String instagramLink = "https://www.instagram.com/uae_barq/";
    public static String youTubeLink = "https://www.youtube.com/channel/UCK9ytN_TgKhALRmYLFscpIA?sub_confirmation=1";
    public static String VIDEO = "video";
    public static String TEXT = "text";
    public static String IMAGE = "image";
    public static int REQUEST_CODE_PREM = 3;

    public static int REQUEST_CODE_FINNISH = 1;
    public static String AppsFlyer_Device_Key = "vit9RcTfmJeubfWMXWZ7Nd";

    public static char message_format_character='-';
    public static int PREMIUM_REQUEST_CODE  = 1;
    public static int BILLING_CODE = 11;

    public static String base64EncodedPublicKey = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAv7MJczcJcdvKxYu+v4R71j9wRFdyppONluoLivSbyUj/YtYbf9LFNLrYJrZ9R3BP/oMrdnlP+TaJBzezykdsul1R5FQX7ukyRsGRmE/4eFv+PdM759tpdEmYJsHeqI4ERQ359VPsTA6zE2pLLb/HxhncZ5nHmwFKkDOLT3POf1nJNaoAYEJvYxyuq/4dphm0DCM9WHlcbU0rrClgreQmZVDZPjHmfLAtgTrOHIi3eZjf8rvvlxpIVobGw48Eii2FpeXyl5cshi0JGTgRzq180KSVw6lccTnYK1Fyjgo3TBTCswtusucHHc8gHpkDJECnEbBFuwz4RYlntbIPVAOeGwIDAQAB";
}
