package com.uaebarq.uaebarq.utils;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.appsflyer.AFInAppEventParameterName;
import com.appsflyer.AFInAppEventType;
import com.appsflyer.AppsFlyerLib;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.uaebarq.uaebarq.ApplicationData;
import com.uaebarq.uaebarq.R;
import com.uaebarq.uaebarq.WebViewActivity;
import com.uaebarq.uaebarq.preferences.AppConstants;
import com.uaebarq.uaebarq.preferences.SharedPreferenceManager;
import com.uaebarq.uaebarq.singleton.Singleton;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


import fm.jiecao.jcvideoplayer_lib.JCVideoPlayer;

import static android.support.v4.app.ActivityCompat.requestPermissions;
import static android.support.v4.app.ActivityCompat.shouldShowRequestPermissionRationale;

public class UaeBarqUtils {


    private static UaeBarqUtils appUtils = null;
    private final Context context;
    private View decorView;

    public UaeBarqUtils(Context context) {
        this.context = context;
    }


    public static UaeBarqUtils getAppUtils(Context context) {
        if (appUtils == null) ;
        appUtils = new UaeBarqUtils(context);
        return appUtils;
    }

    public String getCurrentDate() {
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("E, d MMM yyyy");
        String formattedDate = df.format(c.getTime());
        return formattedDate;
    }

    public void setTextView(TextView tvDetail, final Context context) {
        tvDetail.setMovementMethod(LinkMovementMethod.getInstance());
        tvDetail.setLinkTextColor(Color.BLUE);

        String content = tvDetail.getText().toString();
        List<String> links = new ArrayList<>();
        Pattern p = Patterns.WEB_URL;
        Matcher m = p.matcher(content);
        while (m.find()) {
            String urlStr = m.group();
            links.add(urlStr);
        }
        SpannableString f = new SpannableString(content);
        for (int i = 0; i < links.size(); i++) {
            final String url = links.get(i);
            f.setSpan(new InternalURLSpan(new View.OnClickListener() {
                public void onClick(View v) {
                    String urlToOpen = url;
                    context.startActivity(new Intent(context, WebViewActivity.class).putExtra("url", urlToOpen));
                }
            }), content.indexOf(url), content.indexOf(url) + url.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        }
        tvDetail.setText(f.toString());
    }


    public class InternalURLSpan extends ClickableSpan {
        View.OnClickListener mListener;

        public InternalURLSpan(View.OnClickListener listener) {
            mListener = listener;
        }

        @Override
        public void onClick(View widget) {
            mListener.onClick(widget);
        }
    }

//    public boolean setArabicChecked(boolean value) {
//        return SharedPreferenceManager.getSharedInstance().setBooleanInPreferences(AppConstants.SharedPreferenceKeys.LANG_ARABIC.getKey(), value);
//    }
//
//    public boolean getArabicChecked() {
//        return SharedPreferenceManager.getSharedInstance().getBooleanFromPreferences(AppConstants.SharedPreferenceKeys.LANG_ARABIC.getKey());
//    }
//
//    public boolean setEnglishLanCheck() {
//        return SharedPreferenceManager.getSharedInstance().setBooleanInPreferences(AppConstants.SharedPreferenceKeys.LANG_ENGLISH.getKey(), false);
//    }
//
//    public boolean setEnglishChecked(boolean value) {
//        return SharedPreferenceManager.getSharedInstance().setBooleanInPreferences(AppConstants.SharedPreferenceKeys.LANG_ENGLISH.getKey(), value);
//    }


//    public void openFacebookPage(String packageName, String url, Context context) {
//        Uri uri = Uri.parse(url);
//        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
//        intent.setPackage(packageName);
//        try {
//            context.startActivity(intent);
//        } catch (ActivityNotFoundException e) {
//            context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(url)));
//        }
//    }

    //method to get the right URL to use in the intent
    public String openFacebookPage(Context context) {
        PackageManager packageManager = context.getPackageManager();
        try {
            int versionCode = packageManager.getPackageInfo("com.facebook.katana", 0).versionCode;
            if (versionCode >= 3002850) { //newer versions of fb app
                return "fb://facewebmodal/f?href=" + Constant.facebookLink;
            } else { //older versions of fb app
                return "fb://page/" + Constant.FACEBOOK_PAGE_ID;
            }
        } catch (PackageManager.NameNotFoundException e) {
            return Constant.facebookLink; //normal web url
        }
    }

    public void setInAppPurchasedAppFlyer(String userId, String mobileNumber,String price){
        Map<String, Object> eventValue = new HashMap<>();
        eventValue.put(AFInAppEventParameterName.REVENUE, price);
        eventValue.put(AFInAppEventParameterName.CONTENT_TYPE, "user_subscribed");
        eventValue.put(AFInAppEventParameterName.CONTENT_ID, mobileNumber);
        eventValue.put(AFInAppEventParameterName.CURRENCY, "AED");
        eventValue.put(AFInAppEventParameterName.CUSTOMER_USER_ID, userId);
        AppsFlyerLib.getInstance().trackEvent(context , AFInAppEventType.PURCHASE, eventValue);
    }

    public void openTwitterPage(Context context) {
        try {
            // Check if the Twitter app is installed on the phone.
            context.getPackageManager().getPackageInfo("com.twitter.android", 0);
            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setClassName("com.twitter.android", "com.twitter.android.ProfileActivity");
            // Don't forget to put the "L" at the end of the id.
            intent.putExtra("user_id", 251826832);
            context.startActivity(intent);
        } catch (PackageManager.NameNotFoundException e) {
            // If Twitter app is not installed, start browser.
            context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(Constant.twitterLink)));
        }
    }

    public Intent openInstagramPage(String url, Context context) {
        final Intent intent = new Intent(Intent.ACTION_VIEW);
        try {
            if (context.getPackageManager().getPackageInfo("com.instagram.android", 0) != null) {
                if (url.endsWith("/")) {
                    url = url.substring(0, url.length() - 1);
                }
                final String username = url.substring(url.lastIndexOf("/") + 1);
                intent.setData(Uri.parse("http://instagram.com/_u/" + username));
                intent.setPackage("com.instagram.android");
                context.startActivity(intent);
                return intent;
            }
        } catch (PackageManager.NameNotFoundException ignored) {
            context.startActivity(intent);
        }
        intent.setData(Uri.parse(url));
        context.startActivity(intent);
        return intent;
    }


    public void openYouTubePage(Context context) {
        Intent intent;
        try {
            intent = new Intent(Intent.ACTION_VIEW);
            intent.setPackage("com.google.android.youtube");
            intent.setData(Uri.parse(Constant.youTubeLink));
            context.startActivity(intent);
        } catch (ActivityNotFoundException e) {
            intent = new Intent(Intent.ACTION_VIEW);
            intent.setData(Uri.parse(Constant.youTubeLink));
            context.startActivity(intent);
        }
    }


    public void showToast(Context context, String text) {
        Toast.makeText(context, text, Toast.LENGTH_LONG).show();
    }

    public String getGCMToken() {
        return SharedPreferenceManager.getSharedInstance().getStringFromPreferances(AppConstants.SharedPreferenceKeys.GCM_TOKEN.getKey());
    }

    public void selectSpinnerValue(Spinner spinner, String myString) {
        for (int i = 0; i < spinner.getCount(); i++) {
            if (spinner.getItemAtPosition(i).toString().equalsIgnoreCase(myString.trim().toString())) {
                spinner.setSelection(i);
                break;
            }
        }
    }

    public boolean isPremiumUser() {
        return SharedPreferenceManager.getSharedInstance().getBooleanFromPreferences(AppConstants.SharedPreferenceKeys.PREMIUM.getKey());
    }

    public String appVersion() {
        PackageManager manager = context.getPackageManager();
        PackageInfo info = null;
        try {
            info = manager.getPackageInfo(context.getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return info.versionName;
    }

    public boolean isNullOrEmpty(String s) {
        return s == null || s.length() == 0 || s == "";
    }

    public int getStatusBarHeight(Context context) {
        int result = 0;
        int resourceId = context.getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result = context.getResources().getDimensionPixelSize(resourceId);
        }
        return result;
    }

    public String getCommaSeparatedGoalsIds(List<String> strIdNewList) {
        StringBuilder commaSepValueBuilder = new StringBuilder();
        for (int i = 0; i < strIdNewList.size(); i++) {
            commaSepValueBuilder.append(strIdNewList.get(i));
            if (i != strIdNewList.size() - 1) {
                commaSepValueBuilder.append(", ");
            }
        }
        String commaSeparatedGoalsId = commaSepValueBuilder.toString();
        return commaSeparatedGoalsId;
    }

    public String getCommaSeparatedGoalsIndex(ArrayList<Integer> strIdNewIndex) {
        StringBuilder commaSepValueBuilder = new StringBuilder();
        for (int i = 0; i < strIdNewIndex.size(); i++) {
            commaSepValueBuilder.append(strIdNewIndex.get(i));
            if (i != strIdNewIndex.size() - 1) {
                commaSepValueBuilder.append(", ");
            }
        }
        String commaSeparatedGoalsIndex = commaSepValueBuilder.toString();
        return commaSeparatedGoalsIndex;
    }

    public File createEmptyFile() {
        File directory = new File(Environment.getExternalStorageDirectory() + "/Fitness24");
        if (!directory.exists()) {
            directory.mkdir();
        }
        File destination = new File(directory, System.currentTimeMillis() + "fitness_temp.jpg");
        if (destination.exists()) {
            destination.delete();
        }
        try {
            destination.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return destination;
    }

    public File createTempFile(Bitmap b) {
        try {
            ByteArrayOutputStream bytes = new ByteArrayOutputStream();
            b.compress(Bitmap.CompressFormat.JPEG, 30, bytes);
            FileOutputStream fo;
            File destination = UaeBarqUtils.getAppUtils(context).createEmptyFile();
            try {
                fo = new FileOutputStream(destination);
                fo.write(bytes.toByteArray());
                fo.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return destination;
        } catch (Throwable e) {
            e.printStackTrace();
        }

        return null;
    }

    public Bitmap resize(Bitmap image, int maxWidth, int maxHeight) {
        try {
            if (maxHeight > 0 && maxWidth > 0) {
                int width = image.getWidth();
                int height = image.getHeight();
                float ratioBitmap = (float) width / (float) height;
                float ratioMax = (float) maxWidth / (float) maxHeight;

                int finalWidth = maxWidth;
                int finalHeight = maxHeight;
                if (ratioMax > 1) {
                    finalWidth = (int) ((float) maxHeight * ratioBitmap);
                } else {
                    finalHeight = (int) ((float) maxWidth / ratioBitmap);
                }
                image = Bitmap.createScaledBitmap(image, finalWidth, finalHeight, true);
                return image;
            } else {
                return image;
            }

        } catch (Throwable e) {
            e.printStackTrace();
        }

        return null;
    }

    public boolean isOnline(Context ctx) {
        ConnectivityManager connectivity = (ConnectivityManager) ctx.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Network[] networks = connectivity.getAllNetworks();
            NetworkInfo networkInfo;
            for (Network mNetwork : networks) {
                networkInfo = connectivity.getNetworkInfo(mNetwork);
                if (networkInfo.getState().equals(NetworkInfo.State.CONNECTED)) {
                    return true;
                }
            }
        } else {
            if (connectivity != null) {
                NetworkInfo[] info = connectivity.getAllNetworkInfo();
                if (info != null)
                    for (int i = 0; i < info.length; i++) {
                        if (info[i].getState() == NetworkInfo.State.CONNECTED) {
                            return true;
                        }
                    }
            }
        }
        return false;
    }

    public String getCurrentLang(Context context) {
        return LocaleHelper.getLanguage(context);
    }

    public boolean isEnglish(Context context) {
        return LocaleHelper.getLanguage(context).equalsIgnoreCase(Singleton.getInstance().english);
    }

    public boolean isArabic(Context context) {
        return LocaleHelper.getLanguage(context).equalsIgnoreCase(Singleton.getInstance().arabic);
    }

    public Context setEnglish(Context context) {
        return LocaleHelper.setLocale(context, Singleton.getInstance().english);
    }

    public Context setArabic(Context context) {
        return LocaleHelper.setLocale(context, Singleton.getInstance().arabic);
    }

  /*  public void checkLanguage(Context context){
        if (UaeBarqUtils.getAppUtils(context).isArabic(context)) {
            UaeBarqUtils.getAppUtils(context).setArabic(context);
            //LocaleHelper.onCreate(context, Singleton.getInstance().arabic);
        } else {
           UaeBarqUtils.getAppUtils(context).setEnglish(context);
            //LocaleHelper.onCreate(context, Singleton.getInstance().english);
        }
    }*/
    public void shareDataUsingIntent(View view, final String text) {
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent shareIntent = new Intent(Intent.ACTION_SEND);
                shareIntent.setType("text/plain");
                shareIntent.putExtra(Intent.EXTRA_TEXT, text + "\n" + context.getResources().getString(R.string.read_this_more) + " http://uaebarq.ae/app");
                context.startActivity(Intent.createChooser(shareIntent, context.getResources().getString(R.string.share_data)));
            }
        });
    }

    public boolean isMoreThanOneHours(long curr, long prev) {
        boolean isOneDay = false;
        long difference = curr - prev;
        long sec = difference / 1000;
        long mint = sec / 60;
        long hours = mint / 60;
        if (hours >= 1) {
            isOneDay = true;
        }
        return isOneDay;
    }

    public static long getToday() {
        Calendar c = Calendar.getInstance();
        c.setTimeInMillis(System.currentTimeMillis());
        c.set(Calendar.HOUR_OF_DAY, 0);
        c.set(Calendar.MINUTE, 0);
        c.set(Calendar.SECOND, 0);
        c.set(Calendar.MILLISECOND, 0);
        return c.getTimeInMillis();
    }

    public void snakeBar(View view, String str) {
        Snackbar snackbar = Snackbar.make(view, str, Snackbar.LENGTH_LONG);
        snackbar.show();
    }


    public void showKeyBoard(View view, Context context) {
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(view, InputMethodManager.SHOW_FORCED);
    }

    public void hideKeyBoard(View view, Context context) {
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    public static Bitmap getBitmapFromURL(String src) {
        try {
            URL url = new URL(src);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap myBitmap = BitmapFactory.decodeStream(input);
            return myBitmap;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public File storeImage(Bitmap image) {
        File pictureFile = getOutputMediaFile();
        if (pictureFile == null) {
            Log.d("", "Error creating media file, check storage permissions: ");// e.getMessage());
            return null;
        }
        try {
            FileOutputStream fos = new FileOutputStream(pictureFile);
            image.compress(Bitmap.CompressFormat.PNG, 90, fos);
            fos.close();
        } catch (FileNotFoundException e) {
            Log.d("", "File not found: " + e.getMessage());
        } catch (IOException e) {
            Log.d("", "Error accessing file: " + e.getMessage());
        }
        return pictureFile;
    }

    public boolean isEmailValid(CharSequence email) {
        return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

//    public void getUrlToFile(String strSocialProfilePic, Context context) {
//        if (!isNullOrEmpty(strSocialProfilePic)) {
//            Bitmap bitmap = UaeBarqUtils.getAppUtils(context).getBitmapFromURL(strSocialProfilePic);
//            ApplicationData.file = UaeBarqUtils.getAppUtils(context).storeImage(bitmap);
//        }
//    }

    /**
     * Create a File for saving an image or video
     */
    private File getOutputMediaFile() {
        // To be safe, you should check that the SDCard is mounted
        // using Environment.getExternalStorageState() before doing this.
        File mediaStorageDir = new File(Environment.getExternalStorageDirectory() + "/Android/data/"
                + context.getPackageName()
                + "/Fitness24");

        // This location works best if you want the created images to be shared
        // between applications and persist after your app has been uninstalled.

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                return null;
            }
        }
        // Create a media file name
        String timeStamp = new SimpleDateFormat("ddMM_yyyy_HHmm").format(new Date());
        File mediaFile;
        String mImageName = "MI_" + timeStamp + ".jpg";
        mediaFile = new File(mediaStorageDir.getPath() + File.separator + mImageName);
        return mediaFile;
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    public boolean checkPermission(final Context context, final String permissionName, final int requestcode) {
        int currentAPIVersion = Build.VERSION.SDK_INT;
        if (currentAPIVersion >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(context, permissionName) != PackageManager.PERMISSION_GRANTED) {
                if (shouldShowRequestPermissionRationale((Activity) context, permissionName)) {
                    requestPermissions((Activity) context, new String[]{permissionName}, requestcode);
                } else {
                    requestPermissions((Activity) context, new String[]{permissionName}, requestcode);
                }
                return false;
            } else {
                return true;
            }
        } else {
            return true;
        }
    }

//    public boolean hasNavBar(Context context) {
//        Point realSize = new Point();
//        Point screenSize = new Point();
//        boolean hasNavBar = false;
//        DisplayMetrics metrics = new DisplayMetrics();
//        ((Activity) context).getWindowManager().getDefaultDisplay().getRealMetrics(metrics);
//        realSize.x = metrics.widthPixels;
//        realSize.y = metrics.heightPixels;
//        ((Activity) context).getWindowManager().getDefaultDisplay().getSize(screenSize);
//        if (realSize.y != screenSize.y) {
//            int difference = realSize.y - screenSize.y;
//            Resources resources = context.getResources();
//            int resourceId = resources.getIdentifier("navigation_bar_height", "dimen", "android");
//            if (resourceId > 0) {
//                ApplicationData.navBarHeight = resources.getDimensionPixelSize(resourceId);
//            }
//            if (ApplicationData.navBarHeight != 0) {
//                if (difference == ApplicationData.navBarHeight) {
//                    hasNavBar = true;
//                }
//            }
//        }
//        return hasNavBar;
//    }

//    public int navBarHeight() {
//        return ApplicationData.navBarHeight;
//    }

    public int dpToPx(int dp) {
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        return Math.round(dp * (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));
    }

    public void hideStatusBarOnTouch(final Activity context) {
        decorView = context.getWindow().getDecorView();
        int uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION;
        decorView.setSystemUiVisibility(uiOptions);
        decorView.setOnSystemUiVisibilityChangeListener(new View.OnSystemUiVisibilityChangeListener() {
            @Override
            public void onSystemUiVisibilityChange(int visibility) {
                if ((visibility & View.SYSTEM_UI_FLAG_FULLSCREEN) != 0) {
                    // TODO: The system bars are visible. Make any desired
                    // adjustments to your UI, such as showing the action bar or
                    // other navigational controls.
                    // Hide Status Bar
                    //JCVideoPlayer.setTitleVisibility(View.INVISIBLE);
                } else {
                    // TODO: The system bars are NOT visible. Make any desired
                    // adjustments to your UI, such as hiding the action bar or
                    // other navigational controls.
                    // Show status bar
                    JCVideoPlayer.setTitleVisibility(View.VISIBLE);
                }
            }
        });
    }

    public void ThreadPolicy() {
        if (Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }
    }

    public int getColor(Context context, String ImageId) {
        int color = 0;
        switch (ImageId) {
            case "Latest News":
                color = ContextCompat.getColor(context, R.color.breaking_news);
                break;
            case "الأخبار العاجلة":
                color = ContextCompat.getColor(context, R.color.breaking_news);
                break;
            case "From UAE":
                color = ContextCompat.getColor(context, R.color.from_uae);
                break;
            case "Weather":
                color = ContextCompat.getColor(context, R.color.weather);
                break;
            case "الطقس":
                color = ContextCompat.getColor(context, R.color.weather);
                break;
            case "Health":
                color = ContextCompat.getColor(context, R.color.health);
                break;
            case "الصحة":
                color = ContextCompat.getColor(context, R.color.health);
                break;
            case "Prayertimes":
                color = ContextCompat.getColor(context, R.color.prayer_times);
                break;
            case "مواقيت الصلاة":
                color = ContextCompat.getColor(context, R.color.prayer_times);
                break;
            case "Breaking News":
                color = ContextCompat.getColor(context, R.color.breaking_news);
                break;
            case "آخر الأخبار":
                color = ContextCompat.getColor(context, R.color.breaking_news);
                break;
            case "Sports":
                color = ContextCompat.getColor(context, R.color.sports);
                break;
            case "رياضة":
                color = ContextCompat.getColor(context, R.color.sports);
                break;
            case "From UAE Articles":
                color = ContextCompat.getColor(context, R.color.from_uae);
                break;
            case "من الإمارات":
                color = ContextCompat.getColor(context, R.color.from_uae);
                break;
            case "City News":
                color = ContextCompat.getColor(context, R.color.city_news);
                break;
            case "أخبار المدن":
                color = ContextCompat.getColor(context, R.color.city_news);
                break;
            case "Islamic Services":
                color = ContextCompat.getColor(context, R.color.islamic_services);
                break;
            case "إسلاميات":
                color = ContextCompat.getColor(context, R.color.islamic_services);
                break;

        }
        return color;
    }

    public Drawable getDrawable(Context context, String ImageId) {
        Drawable drawable = null;
        switch (ImageId) {
            case "Latest News":
                drawable = ContextCompat.getDrawable(context, R.drawable.sports);
                break;
            case "الأخبار العاجلة":
                drawable = ContextCompat.getDrawable(context, R.drawable.sports);
                break;
            case "From UAE":
                drawable = ContextCompat.getDrawable(context, R.drawable.from_uae_articles);
                break;
            case "Health":
                drawable = ContextCompat.getDrawable(context, R.drawable.health);
                break;
            case "الصحة":
                drawable = ContextCompat.getDrawable(context, R.drawable.health);
                break;
            case "Prayertimes":
                drawable = ContextCompat.getDrawable(context, R.drawable.prayer_times);
                break;
            case "مواقيت الصلاة":
                drawable = ContextCompat.getDrawable(context, R.drawable.prayer_times);
                break;
            case "Weather":
                drawable = ContextCompat.getDrawable(context, R.drawable.weather);
                break;
            case "الطقس":
                drawable = ContextCompat.getDrawable(context, R.drawable.weather);
                break;
            case "Breaking News":
                drawable = ContextCompat.getDrawable(context, R.drawable.sports);
                break;
            case "آخر الأخبار":
                drawable = ContextCompat.getDrawable(context, R.drawable.sports);
                break;
            case "Sports":
                drawable = ContextCompat.getDrawable(context, R.drawable.sports);
                break;
            case "رياضة":
                drawable = ContextCompat.getDrawable(context, R.drawable.sports);
                break;
            case "From UAE Articles":
                drawable = ContextCompat.getDrawable(context, R.drawable.from_uae_articles);
                break;
            case "من الإمارات":
                drawable = ContextCompat.getDrawable(context, R.drawable.from_uae_articles);
                break;
            case "City News":
                drawable = ContextCompat.getDrawable(context, R.drawable.city_news);
                break;
            case "أخبار المدن":
                drawable = ContextCompat.getDrawable(context, R.drawable.city_news);
                break;
            case "Islamic Services":
                drawable = ContextCompat.getDrawable(context, R.drawable.islamic_services);
                break;
            case "إسلاميات":
                drawable = ContextCompat.getDrawable(context, R.drawable.islamic_services);
                break;
        }
        return drawable;
    }

    public static class FontUtils {

        private static Map<String, Typeface> TYPEFACE = new HashMap<String, Typeface>();

        public static Typeface getFonts(Context context, String name) {
            Typeface typeface = TYPEFACE.get(name);
            if (typeface == null) {
                typeface = Typeface.createFromAsset(context.getAssets(), name);
                TYPEFACE.put(name, typeface);
            }
            return typeface;
        }

        public void applyFonts(Context context, String fontName, TextView... textViews) {
            Typeface tf = getFonts(context, fontName);
            for (TextView tv : textViews) {
                tv.setTypeface(tf);
            }
        }

        public static void applyFonts(Context context, String fontName, final View v) {
            Typeface tf = getFonts(context, fontName);
            applyFonts(tf, v);
        }

        public static void applyFonts(Typeface fontToSet, final View v) {
            try {
                if (v instanceof ViewGroup) {
                    ViewGroup vg = (ViewGroup) v;
                    for (int i = 0; i < vg.getChildCount(); i++) {
                        View child = vg.getChildAt(i);
                        applyFonts(fontToSet, child);
                    }
                } else if (v instanceof TextView) {
                    ((TextView) v).setTypeface(fontToSet);
                }
            } catch (Exception e) {
                e.printStackTrace();
                // ignore
            }
        }
    }
}
