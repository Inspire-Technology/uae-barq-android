package com.uaebarq.uaebarq.utils;

public class FireBaseEvents {

    //Splash Screen
    public static String splashStartApp = "splash_Start_App";
    public static String splashHomeScreenOpen = "splash_open_home_screen";

    //Home SCREEN
    public static String homeOpenPremium = "home_screen_open_premium_activity";
    public static String homeDetailVideoActivity = "home_screen_open_video_activity";
    public static String homeOpenDetailTextActivity = "home_screen_open_detail_text_activity";
    public static String homeCategoryItemClick = "home_screen_category_item_click";
    public static String homeSelectTabOnHome = "home_screen_select_home_tab";
    public static String homelanguageChange = "home_screen_language_Change";
    public static String homeOpenWeatherActivity = "home_screen_open_weather_activity";
    public static String homeGetNotifications = "home_screen_get_notification";
    public static String homeOpenTutorialAcitivty = "home_screen_open_tutorial_activity";
    public static String homeTwitterClick = "home_screen_click_twitter";
    public static String homeInstagaramClick = "home_screen_click_instagaram";
    public static String homeSerachClick = "home_screen_click_search";
    public static String homeSettingActivity = "home_screen_open_setting_activity";
    public static String homeWebViewFooter = "home_screen_click_footer_webview";
    public static String homeTwitterFollowClick = "home_screen_click_twitter_follow";
    public static String homeAllCategoryList = "home_screen_all_category_list_activity";
    public static String homeOpenDetailImageActivity = "home_screen_open_detail_image_activity";

    //WEATHER SCREEN
    public static String weatherScreenItemClick = "weather_screen_item_click";

    //TUTORIAL SCREEN
    // public static String playScreen="PLAY_SCREEN";
    public static String tutorialScreenUnderstandClick = "tutorial_screen_click_understand";

    //SETTING SCREEN
    //public static String premierScreen="PREMIER_SCREEN";
    public static String settingScreenNotification = "setting_screen_notification";
    public static String settingScreenSaveSettings = "setting_screen_save_settings";
    public static String settingScreenOpenHomeActivity = "setting_screen_open_home_activity";

    //SEARCH Activity
    public static String searchScreenSearchClick = "search_screen_search_click";
    public static String searchScreenOpenPremiumActivity = "search_screen_open_premium_activity";
    public static String searchScreenDetailVideoActivity = "search_screen_open_detail_video_activity";
    public static String searchScreenDetailTextActivity = "search_screen_detail_text_activity";

    //Premium Activity
    public static String premiumScreenSubmitButton = "premium_screen_submit_button";
    public static String premiumScreenContactUs = "premium_screen_open_contact_us_activity";
    public static String premiumScreenOpenInAppProduct = "premium_screen_open_inapp_purchase_activity";
    public static String premiumScreenHomeActivity = "premium_screen_open_home_activity";
    public static String premiumScreenOKClick = "premium_screen_click_ok";

    //DetailVideo Activity
    public static String galleryScreenFooterClickOpenWebView = "gallery_screen_clickfooter_openwebview";

    //DetailTextImage SCREEN
    public static String detailTextImageScreenOpenPremiumActivity = "detail_text_image_screen_open_premium_activity";
    public static String detailTextImageScreenVideoLayoutClick = "detail_text_image_screen_click_videolayout";
    public static String detailTextImageScreenOpenWebViewActivity = "detail_text_image_screen_open_webview_activity";
    public static String detailTextImageScreenOpenGalleryActivity = "detail_text_image_screen_open_gallery_activity";

    //CONTACTUS SCREEN
    public static String contactUsInstagaramClick = "contact_us_screen_click_instagram";
    public static String contactUsTwitterClick = "contact_us_screen_click_twitter";
    public static String contactUsFacebookClick = "contact_us_screen_click_facebook";
    public static String contactUsSendClick = "contact_us_screen_click_send";

    //AllCategory SCREEN
    public static String allcategoryScreenFooterClickOpenWebView = "allcategory_screen_click_footer_open_webview";
    public static String allcategoryScreenRefreshOpenAllCategoryActivity = "allcategory_screen_refresh_allcategory";
    public static String allcategoryScreenOpenWeatherActivity = "allcategory_screen_open_weather_activity";
    public static String allcategoryScreenOpenSettingActivity = "allcategory_screen_setting_activity";

    //GCM SCREEN
    public static String notification = "gcm_notification_create";
    public static String gcmregistration = "gcm_registration";
    public static String gcmrefresh = "gcm_refresh";

    //INAPP SCREEN
    public static String inAppWeeklyButton = "in_app_weekly_button";
    public static String inAppMonthlyButton = "in_app_monthly_button";
    public static String inAppMonthlyPurchase = "in_app_monthly_purchase";
    public static String inAppWeeklyPurchase = "in_app_weekly_purchase";
    public static String inAppAutoRenewmonthly = "in_app_auto_renew_montlhy";
    public static String inAppAutoReneweekly = "in_app_auto_renew_weekly";
    public static String inAppconsumefinishedmonthly = "in_app_consume_finish_monthly";
    public static String inAppconsumefinishedwweekly = "in_app_consume_finish_weekly";


}
