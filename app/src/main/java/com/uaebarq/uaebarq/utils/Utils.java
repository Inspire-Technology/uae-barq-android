package com.uaebarq.uaebarq.utils;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.uaebarq.uaebarq.ApplicationData;

import java.io.File;
import java.io.IOException;

/**
 * Some utils methods.
 *
 * @author Alexey Danilov (danikula@gmail.com).
 */
public class Utils {

    public static File getVideoCacheDir(Context context) {
        return new File(context.getExternalCacheDir(), "video-cache");
    }

    public static void cleanVideoCacheDir(Context context) throws IOException {
        File videoCacheDir = getVideoCacheDir(context);
        cleanDirectory(videoCacheDir);
    }

    private static void cleanDirectory(File file) throws IOException {
        if (!file.exists()) {
            return;
        }
        File[] contentFiles = file.listFiles();
        if (contentFiles != null) {
            for (File contentFile : contentFiles) {
                delete(contentFile);
            }
        }
    }

    private static void delete(File file) throws IOException {
        if (file.isFile() && file.exists()) {
            deleteOrThrow(file);
        } else {
            cleanDirectory(file);
            deleteOrThrow(file);
        }
    }

    private static void deleteOrThrow(File file) throws IOException {
        if (file.exists()) {
            boolean isDeleted = file.delete();
            if (!isDeleted) {
                throw new IOException(String.format("File %s can't be deleted", file.getAbsolutePath()));
            }
        }
    }

    public static int[] printMatches(String text, char regex) {

        int[] points = new int[2];
        Boolean isStart = true, isEnd = true;
        int startPos = 0, endPos = 0;
        for (int q = 0; q < text.length(); q++) {
            if (isStart) {
                if (text.charAt(q) == regex) {
                    points[0] = q;
                    points[1] = q + 1;
                    startPos = q;
                    endPos = q + 1;
                    isStart = false;
                }
            } else if (isEnd) {
                if (text.charAt(q) == regex) {
                    points[1] = q;
                    endPos = q;
                    //  isEnd=false;
                }
            }
        }

      /*  String substr = text.substring(startPos, endPos + 1);
        System.out.print("Start index: " + startPos);
        System.out.print(" End index: " + endPos);*/

        return points;
    }


    public static void trackEvent(String screenName, String value) {
        Bundle b = new Bundle();
        if (value != null) {
            b.putString(FirebaseAnalytics.Param.CONTENT_TYPE, value);
        }
        ApplicationData.getFireBaseInstance().logEvent(screenName, b);
    }


    public static void trackScreenNew(String screenName, Activity activity){
        ApplicationData.getFireBaseInstance().setCurrentScreen(activity, screenName, activity.getClass().getSimpleName());
    }

    public static void trackEventNew1221(String id, String name, String type){
        Bundle bundle = new Bundle();
        bundle.putString(FirebaseAnalytics.Param.ITEM_ID, id);
        bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, name);
        bundle.putString(FirebaseAnalytics.Param.CONTENT_TYPE, type);
        ApplicationData.getFireBaseInstance().logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundle);
    }
}
