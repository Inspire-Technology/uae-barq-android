package com.uaebarq.uaebarq;

import android.os.Bundle;

import com.squareup.picasso.Picasso;
import com.uaebarq.uaebarq.utils.TouchImageView;

import butterknife.BindView;
import butterknife.OnClick;

public class GalleryViewActivity extends BaseActivity {

    @BindView(R.id.iv_gallery)
    TouchImageView ivGallery;

    Bundle intent;
    String strUrl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        intent = getIntent().getExtras();
        if (intent != null) {
            strUrl = intent.getString("image");
            Picasso.with(this)
                    .load(strUrl)
                    .into(ivGallery);
        }
    }

    @Override
    protected int setLayoutId() {
        return R.layout.activity_gallery;
    }

    @OnClick(R.id.iv_cross)
    void setImageClick() {
        finish();
    }
}
