package com.uaebarq.uaebarq.retrofit.NetworkInterface;


import com.uaebarq.uaebarq.models.AddPremiumModel.AddPremiumModel;
import com.uaebarq.uaebarq.models.AllCategoryListModel.AllCategoryListModel;
import com.uaebarq.uaebarq.models.DashBoardModel.DashboardModel;
import com.uaebarq.uaebarq.models.DetailModel.DetailModel;
import com.uaebarq.uaebarq.models.FetchSettingsModel.FetchSettingsModel;
import com.uaebarq.uaebarq.models.GetSettingsModel.GetSettingsModel;
import com.uaebarq.uaebarq.models.InstagramModel.InstagramModel;
import com.uaebarq.uaebarq.models.MobiSdkModel.MobiSdkModel;
import com.uaebarq.uaebarq.models.OTPModel.OtpModel;
import com.uaebarq.uaebarq.models.ResponseBase;
import com.uaebarq.uaebarq.models.SearchModel.SearchModel;
import com.uaebarq.uaebarq.models.TwitterModel.TwitterModel;
import com.uaebarq.uaebarq.models.WeatherModel.WeatherModel;
import com.uaebarq.uaebarq.models.YouTubeModel.YouTubeModel;
import com.uaebarq.uaebarq.models.Coupon.CouponModel;
import com.uaebarq.uaebarq.models.unsubcribe_errormodel.UnSubcribeErrorModel;

import java.util.Map;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;
import retrofit2.http.Url;

public interface NetworkPostServices {

    @FormUrlEncoded
    @POST("homeFeeds")
    Call<DashboardModel> getDashBoardData(@Field("Local") String local);

    @FormUrlEncoded
    @POST("youtube")
    Call<YouTubeModel> getYouTubeData(@Field("Local") String local);

    @FormUrlEncoded
    @POST("twitter")
    Call<TwitterModel> getTwitterData(@Field("Local") String local);

    @FormUrlEncoded
    @POST("instagram")
    Call<InstagramModel> getInstagramData(@Field("Local") String local);

    @FormUrlEncoded
    @POST("search")
    Call<SearchModel> getSearchData(@Field("api_key") String key,
                                    @Field("keyword") String keyWord,
                                    @Field("Local") String local);

    @FormUrlEncoded
    @POST("send_contact_email")
    Call<ResponseBase> postContactUsData(@Field("api_key") String strApiKey,
                                         @Field("name") String name,
                                         @Field("phone") String phone,
                                         @Field("text") String text,
                                         @Field("email") String email,
                                         @Field("token") String token);

    @FormUrlEncoded
    @POST("categoryPosts")
    Call<AllCategoryListModel> getAllCategoryListData(@Field("Limit") String limit,
                                                      @Field("Offset") String offset,
                                                      @Field("Local") String local,
                                                      @Field("category_id") String category,
                                                      @Field("user_id") String userId);


    @FormUrlEncoded
    @POST("PostDetail")
    Call<DetailModel> getDetailData(@Field("Local") String key,
                                    @Field("post_id") String keyWord,
                                    @Field("user_id") String userId);

    @FormUrlEncoded
    @POST("applyCoupon")
    Call<CouponModel> postCouponCode(@Field("user_id") String id,
                                     @Field("code") String code,
                                     @Field("Local") String local,
                                     @Field("api_key") String apiKey);


    @FormUrlEncoded
    @POST("addPremiumUser")
    Call<AddPremiumModel> postPremiumUser(@FieldMap Map<String, String> authData);

    @FormUrlEncoded
    @POST("settings")
    Call<FetchSettingsModel> fetchSettings(@Field("api_key") String apiKey,
                                           @Field("user_id") String userId,
                                           @Field("device_type") String deviceType,
                                           @Field("app_id") String appId,
                                           @Field("Local") String Local);


    @FormUrlEncoded
    @POST("getSettings")
    Call<GetSettingsModel> getSettings(@Field("api_key") String apiKey,
                                       @Field("user_id") String userId,
                                       @Field("app_id") String appId,
                                       @Field("Local") String Local);

    @FormUrlEncoded
    @POST("saveCatSettings")
    Call<ResponseBase> saveSettings(@FieldMap Map<String, String> authData);

    @FormUrlEncoded
    @POST("getWeatherAPI")
    Call<WeatherModel> getWeatherData(@Field("api_key") String apiKey,
                                      @Field("local") String local,
                                      @Field("city") String city);

    @POST("applePay")
    Call<ResponseBase> postGooglePlay(@Field("user_id") String id,
                                      @Field("signed_data") String code,
                                      @Field("type") String type,
                                      @Field("api_key") String apiKey,
                                      @Field("signature") String signature,
                                      @Field("device_type") String deviceTpe);

    @FormUrlEncoded
    @POST("deviceToken")
    Call<ResponseBase> postGcmToken(@Field("user_id") String id,
                                    @Field("device_type") String type,
                                    @Field("app_type") String appType,
                                    @Field("app_version") String appVersion,
                                    @Field("app_id") String appId,
                                    @Field("Local") String deviceType,
                                    @Field("device_token") String token);

    @FormUrlEncoded
    @POST("checksdk")
    Call<MobiSdkModel> checkNewSDKData(@Field("app_id") String apiId,
                                       @Field("device_type") String deviceTpe);

    @GET("callback/unsub_chk.php")
    Call<OtpModel> getsubIS(@Query("msisdn") String msisdn);

    @GET
    Call<UnSubcribeErrorModel> getErrorMessage(
            @Url String url,
            @Query("action") int action,
            @Query("service") int service,
            @Query("ref") String reference,
            @Query("msisdn") String mobile_nm);
}