package com.uaebarq.uaebarq.viewholder;

import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class ViewHolder {

    public class DataViewFirstItemHolder extends ViewHolder{
        public ImageView ivImage;
        public LinearLayout rlTag;
        public TextView tvTagTitle, tvTitle, tvDate;

    }

    public class DataViewSecondItemHolder extends ViewHolder {
        public ImageView ivCatImage, ivTriangle;
        public RelativeLayout rlTagImage;
        public TextView tvTagTitle, tvTitle, tvDate;
        public LinearLayout llMainImage;
    }
}
