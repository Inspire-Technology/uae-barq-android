package com.uaebarq.uaebarq.interfaceadapter;

public interface IMethodCaller {
    void yourDesiredMethod();
}
