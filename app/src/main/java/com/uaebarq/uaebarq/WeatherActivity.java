package com.uaebarq.uaebarq;

import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.github.silvestrpredko.dotprogressbar.DotProgressBar;
import com.squareup.picasso.Picasso;
import com.uaebarq.uaebarq.models.WeatherModel.Weather;
import com.uaebarq.uaebarq.models.WeatherModel.WeatherModel;
import com.uaebarq.uaebarq.retrofit.NetworkInterface.NetworkPostServices;
import com.uaebarq.uaebarq.utils.Constant;
import com.uaebarq.uaebarq.utils.FireBaseEvents;
import com.uaebarq.uaebarq.utils.UaeBarqUtils;
import com.uaebarq.uaebarq.utils.Utils;

import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class WeatherActivity extends BaseActivity {

    @BindView(R.id.dot_progress_bar)
    DotProgressBar dotProgressBar;
    private LayoutInflater layoutInflater;
    @BindView(R.id.ll_main)
    LinearLayout llMain;
    @BindView(R.id.progress_layout)
    View progressLayout;
    @BindView(R.id.hs)
    HorizontalScrollView hs;
    @BindView(R.id.spinner)
    Spinner spinner;
    @BindView(R.id.tv_current_temp)
    TextView tvCurrentTemp;
    @BindView(R.id.tv_cloud)
    TextView tvCloud;
    @BindView(R.id.iv_icon)
    ImageView ivIcon;
    @BindView(R.id.tv_name)
    TextView tvName;
    @BindView(R.id.iv_logo)
    ImageView ivLogo;
    @BindView(R.id.tv_date)
    TextView tvDate;
    @BindView(R.id.tv_up_number)
    TextView tvUpNumber;
    @BindView(R.id.tv_down_number)
    TextView tvDownNumber;
    @BindView(R.id.ll_main_view)
    LinearLayout llMainView;
    @BindView(R.id.iv_uae)
    ImageView ivUae;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        layoutInflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
        ivLogo.setVisibility(View.GONE);
        tvName.setVisibility(View.VISIBLE);
        tvName.setText(getResources().getString(R.string.weather));
        dotProgressBar.setEndColor(ContextCompat.getColor(this, R.color.tab_gray));
        dotProgressBar.setStartColor(ContextCompat.getColor(this, R.color.tab_gray));
        setLayout();
    }

    private void setLayout() {
        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter(this, R.layout.spinner_textview, getResources().getStringArray(R.array.city_list));
        spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(spinnerArrayAdapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2, long arg3) {

                Utils.trackEvent(this.getClass().getSimpleName(), FireBaseEvents.weatherScreenItemClick);
                String items = spinner.getSelectedItem().toString();
                getData(items);
                progressLayout.setVisibility(View.VISIBLE);
                hs.setVisibility(View.GONE);
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub
            }
        });
    }

    private void getData(String city) {
        if (isOnline()) {
            getWeatherDataFromServer(city);
        } else {
            UaeBarqUtils.getAppUtils(this).showToast(this, getResources().getString(R.string.please_check_internet));
        }
    }

    private void getWeatherDataFromServer(String strCity) {
         Call<WeatherModel> weatherModelCall = ApplicationData.getInstance().getRestClient().createService(NetworkPostServices.class)
                .getWeatherData(Constant.API_KEY, UaeBarqUtils.getAppUtils(this).getCurrentLang(this), strCity);
        weatherModelCall.enqueue(new Callback<WeatherModel>() {
            @Override
            public void onResponse(Call<WeatherModel> call, Response<WeatherModel> response) {
                try {
                    progressLayout.setVisibility(View.GONE);
                    if (response.body().getPayload().getWeather().size() > 0 && response.body() != null && response.body().getSuccess().equalsIgnoreCase("true")) {
                        Weather weather = response.body().getPayload().getWeather().get(0);
                        tvCurrentTemp.setText(weather.getCurrentTepm().toString());
                        tvCloud.setText(weather.getWeatherDescription());
                        tvDate.setText(UaeBarqUtils.getAppUtils(getApplicationContext()).getCurrentDate());
                        tvUpNumber.setText(weather.getMimTemp().toString());
                        tvDownNumber.setText(weather.getMaxTemp().toString());
                        Picasso.with(getApplicationContext())
                                .load(response.body().getPayload().getBg())
                                .into(ivUae);
                        Picasso.with(getApplicationContext())
                                .load(weather.getIcon())
                                .into(ivIcon);
                        setWeatherList(response.body().getPayload().getWeather());
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    progressLayout.setVisibility(View.GONE);
                }
            }

            @Override
            public void onFailure(Call<WeatherModel> call, Throwable t) {
                progressLayout.setVisibility(View.GONE);
            }
        });
        if (isEnglishLanguage()) {
            UaeBarqUtils.FontUtils.applyFonts(this, Constant.strTypefaceEnglishMedium, llMainView);
            UaeBarqUtils.FontUtils.applyFonts(this, Constant.strTypefaceEnglishRegular, tvDate);
        } else {
            UaeBarqUtils.FontUtils.applyFonts(this, Constant.strTypefaceArabicMedium, llMainView);
            UaeBarqUtils.FontUtils.applyFonts(this, Constant.strTypefaceEnglishRegular, tvDate);
        }
    }

    /*set Weather item*/
    private void setWeatherList(final List<Weather> weather) {
        hs.setVisibility(View.VISIBLE);
        View convertView;
        llMain.removeAllViews();
        boolean isTrue = true;
        if (weather.size() > 0) {
            try {
                for (int value = 0; value < weather.size(); value++) {
                    convertView = layoutInflater.inflate(R.layout.weather_item, null);
                    TextView tvNumber = convertView.findViewById(R.id.tv_number);
                    TextView tvDualNumber = convertView.findViewById(R.id.tv_dual_number);
                    ImageView ivWeather = convertView.findViewById(R.id.iv_weather);
                    if(value != 0) {
                        tvNumber.setText(weather.get(value).getCurrentTepm().toString());
                        tvDualNumber.setText(weather.get(value).getMimTemp().toString() + "/" + weather.get(value).getMaxTemp().toString());
                        Picasso.with(this)
                                .load(weather.get(value).getIcon())
                                .into(ivWeather);
                        if (isEnglishLanguage()) {
                            UaeBarqUtils.FontUtils.applyFonts(this, Constant.strTypefaceEnglishRegular, llMain);
                        } else {
                            UaeBarqUtils.FontUtils.applyFonts(this, Constant.strTypefaceArabicRegular, llMain);
                            if (isTrue) {
                                hs.postDelayed(new Runnable() {
                                    public void run() {
                                        // Disable the animation First
                                        hs.setSmoothScrollingEnabled(false);
                                        hs.fullScroll(HorizontalScrollView.FOCUS_RIGHT);
                                        hs.setSmoothScrollingEnabled(true);
                                    }
                                }, 100L);
                                isTrue = false;
                            }
                        }
                        llMain.addView(convertView);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @OnClick(R.id.iv_back)
    void click() {
        finish();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected int setLayoutId() {
        return R.layout.activity_weather;
    }
}
