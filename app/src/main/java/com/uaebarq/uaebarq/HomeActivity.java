package com.uaebarq.uaebarq;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.AdapterView;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.github.silvestrpredko.dotprogressbar.DotProgressBar;
import com.squareup.picasso.Picasso;
import com.uaebarq.uaebarq.adapter.CategoryListAdapter;
import com.uaebarq.uaebarq.adapter.InstagramAdapter;
import com.uaebarq.uaebarq.adapter.TwitterAdapter;
import com.uaebarq.uaebarq.adapter.ViewPagerAdapter;
import com.uaebarq.uaebarq.interfaceadapter.IMethodCaller;
import com.uaebarq.uaebarq.models.DashBoardModel.Banner;
import com.uaebarq.uaebarq.models.DashBoardModel.Category;
import com.uaebarq.uaebarq.models.DashBoardModel.CategoryPost;
import com.uaebarq.uaebarq.models.DashBoardModel.DashboardModel;
import com.uaebarq.uaebarq.models.DashBoardModel.Slider;
import com.uaebarq.uaebarq.models.InstagramModel.InstagramModel;
import com.uaebarq.uaebarq.models.TwitterModel.Feed;
import com.uaebarq.uaebarq.models.TwitterModel.TwitterModel;
import com.uaebarq.uaebarq.preferences.AppConstants;
import com.uaebarq.uaebarq.preferences.SharedPreferenceManager;
import com.uaebarq.uaebarq.retrofit.NetworkInterface.NetworkPostServices;
import com.uaebarq.uaebarq.singleton.Singleton;
import com.uaebarq.uaebarq.utils.Constant;
import com.uaebarq.uaebarq.utils.FireBaseEvents;
import com.uaebarq.uaebarq.utils.GridSpacingItemDecoration;
import com.uaebarq.uaebarq.utils.NonScrollListView;
import com.uaebarq.uaebarq.utils.UaeBarqUtils;
import com.uaebarq.uaebarq.utils.Utils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import cn.trinea.android.view.autoscrollviewpager.AutoScrollViewPager;
import me.relex.circleindicator.CircleIndicator;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.uaebarq.uaebarq.ApplicationData.gcmCategory;
import static com.uaebarq.uaebarq.ApplicationData.image;
import static com.uaebarq.uaebarq.ApplicationData.text;
import static com.uaebarq.uaebarq.ApplicationData.video;

public class HomeActivity extends BaseActivity implements IMethodCaller {

    @BindView(R.id.view_pager)
    AutoScrollViewPager viewPager;

    @BindView(R.id.indicator)
    CircleIndicator indicator;

    @BindView(R.id.dot_progress_bar)
    DotProgressBar dotProgressBar;

    @BindView(R.id.rl_progress_layout)
    RelativeLayout rlProgressLayout;

    private ArrayList<Slider> sliderList = new ArrayList<>();
    private Call<DashboardModel> dashBoardCall;
   // private Call<YouTubeModel> youTubeCall;
    private Call<TwitterModel> twitterCall;
    private Call<InstagramModel> instagramModelCall;
    SharedPreferenceManager sharedPreferenceManager;

    @BindView(R.id.iv_lang)
    ImageView ivLang;

    private LayoutInflater layoutInflater;

    @BindView(R.id.ll_tabs)
    LinearLayout llTab;

    @BindView(R.id.hs_tabs)
    HorizontalScrollView hsTab;

    CategoryListAdapter categoryListAdapter;

    @BindView(R.id.sc_main)
    ScrollView scrollView;

    @BindView(R.id.listView)
    NonScrollListView listView;

  /*  @BindView(R.id.hs_youtube)
    HorizontalScrollView hsYoutube;

    @BindView(R.id.ll_youtube)
    LinearLayout llYoutube;*/

/*    @BindView(R.id.rl_progress)
    RelativeLayout rlProgress;

    @BindView(R.id.tv_subscribe)
    TextView tvSubscribe;

    @BindView(R.id.tv_join_us)
    TextView tvJoinUs;

    @BindView(R.id.tv_subscribe_simple)
    TextView tvSubscribeSimple;*/

/*    @BindView(R.id.rl_youtube_main)
    LinearLayout rlYoutubeMain;*/

    @BindView(R.id.ll_footer)
    LinearLayout llFooter;

    @BindView(R.id.tw_social_recyclerView)
    RecyclerView twSocialRecyclerView;

    @BindView(R.id.in_social_recyclerView)
    RecyclerView inSocialRecyclerView;

    TwitterAdapter twitterAdapter;

    InstagramAdapter instagramAdapter;

    @BindView(R.id.ll_social_media)
    LinearLayout llSocialMedia;

    @BindView(R.id.tv_tweets)
    TextView tvTweets;

    @BindView(R.id.tv_tweets_no)
    TextView tvTweetsNo;

    @BindView(R.id.tv_followings)
    TextView tvFollowings;

    @BindView(R.id.tv_followings_no)
    TextView tvFollowingsNo;

    @BindView(R.id.tv_followers)
    TextView tvFollowers;

    @BindView(R.id.tv_followers_no)
    TextView tvFollowersNo;

    @BindView(R.id.iv_twitter_icon)
    ImageView ivTwitterIcon;

    @BindView(R.id.ll_twitter_bg)
    LinearLayout llTwitterBg;

    @BindView(R.id.ll_instagram_bg)
    LinearLayout llInstagramBg;

    @BindView(R.id.tv_twitter_name)
    TextView tvTwitterName;

    @BindView(R.id.tv_instagram_name)
    TextView tvInstagramName;

    @BindView(R.id.ll_twitter_view)
    LinearLayout llTwitterView;

    @BindView(R.id.ll_instagram_view)
    LinearLayout llInstagramView;

    @BindView(R.id.iv_instagarm_icon)
    ImageView ivInstagramIcon;

    @BindView(R.id.ll_line)
    LinearLayout llLine;
    private InstagramModel instagramModel;


    @BindView(R.id.ll_social_main)
    LinearLayout llSocialMain;

    ViewTreeObserver observer;

    int llSocialMainHeight, llFooterHeight;
    private DashboardModel dashboardModel;

    @BindView(R.id.iv_footer)
    ImageView ivFooter;

    @BindView(R.id.ll_follow)
    ImageView ll_follow;

    @BindView(R.id.main_footer)
    View main_footer;

    @BindView(R.id.tv_footer_text)
    TextView tvFooter;
    private TwitterModel twitterModel;
    int page_position = 0;
    Handler handler;
    final int delay = 3000;
    private Runnable runnable;
    ViewPagerAdapter viewPagerAdapter;
    private boolean generic;
    @BindView(R.id.ll_congratx)
    View llCongrats;
    @BindView(R.id.tv_purc)
    TextView tvPurchased;
    private String message;
    private int categoryId;
    Intent intent = null;
    private String premium;
    private String type;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getNotificationBundle();
        handler = new Handler();
        layoutInflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
        dotProgressBar.setEndColor(ContextCompat.getColor(this, R.color.twitter_color));
        dotProgressBar.setStartColor(ContextCompat.getColor(this, R.color.twitter_color));
        sharedPreferenceManager = SharedPreferenceManager.getSharedInstance();
        if (isEnglishLanguage()) {
            ivLang.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ar));
        } else {
            ivLang.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.en));
        }

        observer = llSocialMain.getViewTreeObserver();
        observer.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {

            @Override
            public void onGlobalLayout() {
                // TODO Auto-generated method stub
                init();
                llSocialMain.getViewTreeObserver().removeGlobalOnLayoutListener(this);
            }
        });

        observer = llFooter.getViewTreeObserver();
        observer.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {

            @Override
            public void onGlobalLayout() {
                // TODO Auto-generated method stub
                initFooter();
                llFooter.getViewTreeObserver().removeGlobalOnLayoutListener(this);
            }
        });

        if (getPreviousMilliSeconds() <= 0 || isMoreThanOneHours()) {
            getDashBoardData();
          //  getYouTubeData();
            getTwitterData();
            getInstagramData();
        } else {
            initializeSlider();
          //  initializeYouTube();
            initializeTwitter();
            initializeInstagram();
        }
    }

    protected void init() {
        llSocialMainHeight = llSocialMain.getHeight();
    }

    protected void initFooter() {
        llFooterHeight = llFooter.getHeight();
    }


    private void getDashBoardData() {
        if (isOnline()) {
            getDashBoardDataFromServer();
        } else {
            UaeBarqUtils.getAppUtils(this).showToast(this, getResources().getString(R.string.please_check_internet));
        }
    }

    private void getDashBoardDataFromServer() {
        dashBoardCall = ApplicationData.getInstance().getRestClient().createService(NetworkPostServices.class)
                .getDashBoardData(UaeBarqUtils.getAppUtils(this).getCurrentLang(this));
        dashBoardCall.enqueue(new Callback<DashboardModel>() {
            @Override
            public void onResponse(Call<DashboardModel> call, Response<DashboardModel> response) {
                try {
                    if (response.body() != null && response.body().getSuccess().equalsIgnoreCase("true")) {
                        sharedPreferenceManager.saveDashBoardModel(response.body());
                        SharedPreferenceManager.getSharedInstance().setLongInPreferences(AppConstants.SharedPreferenceKeys.DASHBOARD_REFRESH_DB_TIME.getKey(), System.currentTimeMillis());
                        initializeSlider();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<DashboardModel> call, Throwable t) {
                Log.e("error", "" + t.getMessage());
            }
        });
    }

    private void initializeSlider() {
        dashboardModel = sharedPreferenceManager.getDashBoardModelFromPreferences();
        if (isPremiumUser()) {
            main_footer.setVisibility(View.GONE);
        } else {
            setFooter();
        }
        ArrayList<Slider> slider = dashboardModel.getPayload().getSlider();
        for (int i = 0; i < slider.size(); i++) {
            sliderList.add(slider.get(i));
        }
        viewPagerAdapter = new ViewPagerAdapter(HomeActivity.this, sliderList);
        viewPager.setAdapter(viewPagerAdapter);
        indicator.setViewPager(viewPager);

        viewPager.setOnPageChangeListener(new MyOnPageChangeListener());

        viewPager.setInterval(2000);
        viewPager.startAutoScroll();
        //viewPager.setCurrentItem(Integer.MAX_VALUE / 2 - Integer.MAX_VALUE / 2 % ListUtils.getSize(sliderList));
        viewPager.setCurrentItem(0, true);
        viewPager.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        Log.w("touched", "down");
                        return true;
                    case MotionEvent.ACTION_UP:
                        Log.w("touched", "up");
                        if (viewPager.isStopScrollWhenTouch()) {
                            viewPager.setStopScrollWhenTouch(false);
                            viewPager.stopAutoScroll();
                        } else {
                            viewPager.setStopScrollWhenTouch(true);
                            viewPager.startAutoScroll();
                        }
                        return true;
                }

                return false;
            }
        });

//        viewPager.setOnItemClickListener(new ClickableViewPager.OnItemClickListener() {
//            @Override
//            public void onItemClick(int position) {
//                int currentItem = viewPager.getCurrentItem();
//                Bundle bundle = new Bundle();
//                if (sliderList.get(currentItem).isFree()) {
//                    passIntent(bundle, currentItem);
//                } else {
//                    if (isPremiumUser()) {
//                        passIntent(bundle, currentItem);
//                    } else {
//                        //premium session
//                        openPremiumActivity();
//                    }
//                }
//            }
//        });

        scrollView.setSmoothScrollingEnabled(false);
        scrollView.smoothScrollTo(0, 0);
        scrollView.setSmoothScrollingEnabled(true);

        ArrayList<Category> categories = dashboardModel.getPayload().getCategories();
        Singleton.getInstance().tabCategoriesList = categories;
        ArrayList<CategoryPost> categoryPosts = dashboardModel.getPayload().getCategoryPosts();
        setTabs(categories);
        setCategoryListView(categoryPosts);
        boolean saveTutorial = SharedPreferenceManager.getSharedInstance().getBooleanFromPreferences(AppConstants.SharedPreferenceKeys.SAVE_TUTORIAL.getKey());
        if (!saveTutorial) {
            openTutorial();
        }
    }

    public class MyOnPageChangeListener implements ViewPager.OnPageChangeListener {

        @Override
        public void onPageSelected(int position) {
//            indexText.setText(new StringBuilder().append((position) % ListUtils.getSize(sliderList) + 1).append("/")
//                    .append(ListUtils.getSize(sliderList)));
        }

        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
        }

        @Override
        public void onPageScrollStateChanged(int arg0) {
        }
    }

    public void openPremiumActivity() {

        Utils.trackEvent(this.getClass().getSimpleName(), FireBaseEvents.homeOpenPremium);
        Intent intentPremium = new Intent(HomeActivity.this, PremiumCodeActivity.class);
        startActivityForResult(intentPremium, Constant.PREMIUM_REQUEST_CODE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    public void passIntent(Bundle bundle, int currentItem) {
        bundle.putSerializable("catObjectDetail", sliderList.get(currentItem));
        if (sliderList.get(currentItem).getType().equalsIgnoreCase(Constant.VIDEO)) {

            Utils.trackEvent(this.getClass().getSimpleName(), FireBaseEvents.homeDetailVideoActivity);
            Intent intent = new Intent(HomeActivity.this, DetailActivityVideo.class);
            intent.putExtras(bundle);
            startActivity(intent);
        } else {
            Utils.trackEvent(this.getClass().getSimpleName(), FireBaseEvents.homeOpenDetailTextActivity);
            Intent intent = new Intent(HomeActivity.this, DetailActivityTextImage.class);
            intent.putExtras(bundle);
            startActivity(intent);
        }
    }

    private void setCategoryListView(ArrayList<CategoryPost> categoryPosts) {
        categoryListAdapter = new CategoryListAdapter(this, categoryPosts);
        listView.setAdapter(categoryListAdapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Utils.trackEvent(this.getClass().getSimpleName(), FireBaseEvents.homeCategoryItemClick);
                CategoryPost category = (CategoryPost) parent.getItemAtPosition(position);
                Bundle bundle = new Bundle();
                if (category != null) {
                    if (category.isFree()) {
                        passIntentCatPost(bundle, category);
                    } else {
                        if (isPremiumUser()) {
                            passIntentCatPost(bundle, category);
//                            Intent intent = new Intent(HomeActivity.this, InAppProductActivity.class);
//                            startActivity(intent);
                        } else {
                            openPremiumActivity();
                        }
                    }
                }
            }
        });
    }

    private void passIntentCatPost(Bundle bundle, CategoryPost category) {
        bundle.putSerializable("catPostDetail", category);
        if (category.getType().equalsIgnoreCase(Constant.VIDEO)) {

            Utils.trackEvent(this.getClass().getSimpleName(), FireBaseEvents.homeDetailVideoActivity);
            Intent intent = new Intent(HomeActivity.this, DetailActivityVideo.class);
            intent.putExtras(bundle);
            startActivity(intent);
        } else {

            Utils.trackEvent(this.getClass().getSimpleName(), FireBaseEvents.homeOpenDetailTextActivity);
            Intent intent = new Intent(HomeActivity.this, DetailActivityTextImage.class);
            intent.putExtras(bundle);
            startActivity(intent);
        }
    }

    /*set Tabs item*/
    private void setTabs(final ArrayList<Category> categories) {
        View convertView;
        LinearLayout llMain;
        boolean isTrue = true;
        if (categories.size() > 0) {
            for (int value = 0; value < categories.size(); value++) {
                convertView = layoutInflater.inflate(R.layout.tabs_item, null);
                llMain = convertView.findViewById(R.id.ll_main);
                final TextView tvCategoryTab = convertView.findViewById(R.id.tv_category_tab);
                if (value == 0) {
                    tvCategoryTab.setTextColor(ContextCompat.getColor(this, R.color.tab_green));
                }
                tvCategoryTab.setText(categories.get(value).getTitle());
                if (value != 0) {
                    final Category category = categories.get(value);
                    convertView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            Utils.trackEvent(this.getClass().getSimpleName(), FireBaseEvents.homeSelectTabOnHome);
                            Intent intent;
                            if (category.getId() == 13 || category.getId() == 8) {
                                intent = new Intent(HomeActivity.this, WeatherActivity.class);
                            } else {
                                intent = new Intent(HomeActivity.this, AllCategoryListActivity.class);
                                intent.putExtra("catObject", category);
                            }
                            startActivity(intent);
                        }
                    });
                }
                if (isEnglishLanguage()) {
                    UaeBarqUtils.FontUtils.applyFonts(this, Constant.strTypefaceEnglishMedium, llMain);
                } else {
                    UaeBarqUtils.FontUtils.applyFonts(this, Constant.strTypefaceArabicMedium, llMain);
                    if (isTrue) {
                        hsTab.postDelayed(new Runnable() {
                            public void run() {
                                // Disable the animation First
                                hsTab.setSmoothScrollingEnabled(false);
                                hsTab.fullScroll(HorizontalScrollView.FOCUS_RIGHT);
                                hsTab.setSmoothScrollingEnabled(true);
                            }
                        }, 100L);
                        isTrue = false;
                    }
                }
                llTab.addView(convertView);
            }
        }
    }

/*    // you tube data
    private void getYouTubeData() {
        if (isOnline()) {
            dotProgressBar.setEndColor(ContextCompat.getColor(this, R.color.white));
            dotProgressBar.setStartColor(ContextCompat.getColor(this, R.color.white));
            getYouTubeFromServer();
        } else {
            UaeBarqUtils.getAppUtils(this).showToast(this, getResources().getString(R.string.please_check_internet));
        }
    }*/

 /*   private void getYouTubeFromServer() {
        youTubeCall = ApplicationData.getInstance().getRestClient().createService(NetworkPostServices.class)
                .getYouTubeData(UaeBarqUtils.getAppUtils(this).getCurrentLang(this));
        youTubeCall.enqueue(new Callback<YouTubeModel>() {
            @Override
            public void onResponse(Call<YouTubeModel> call, Response<YouTubeModel> response) {
                try {
                    rlProgress.setVisibility(View.GONE);
                    if (response.body() != null && response.body().getSuccess().equalsIgnoreCase("true")) {
                        sharedPreferenceManager.saveYouTubeModel(response.body());
                        initializeYouTube();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    rlProgress.setVisibility(View.GONE);
                }
            }

            @Override
            public void onFailure(Call<YouTubeModel> call, Throwable t) {
                rlProgress.setVisibility(View.GONE);
            }
        });
    }*/

  /*  private void initializeYouTube() {
        rlProgress.setVisibility(View.GONE);
        YouTubeModel youTubeModel = sharedPreferenceManager.getYouTubeModelFromPreferences();
        YouTubeModel youTubeModelMain = youTubeModel;

        if (youTubeModelMain!=null&&!UaeBarqUtils.getAppUtils(this).isNullOrEmpty(youTubeModelMain.getPayload().getSubscribe())) {
            tvSubscribe.setText(youTubeModelMain.getPayload().getSubscribe());
        }
        setYouTubeList(youTubeModelMain);
        if (isEnglishLanguage()) {
            UaeBarqUtils.FontUtils.applyFonts(this, Constant.strTypefaceEnglishMedium, tvJoinUs);
            UaeBarqUtils.FontUtils.applyFonts(this, Constant.strTypefaceEnglishMedium, tvSubscribeSimple);
            UaeBarqUtils.FontUtils.applyFonts(this, Constant.strTypefaceEnglishRegular, tvSubscribe);
        } else {
            UaeBarqUtils.FontUtils.applyFonts(this, Constant.strTypefaceArabicMedium, tvJoinUs);
            UaeBarqUtils.FontUtils.applyFonts(this, Constant.strTypefaceArabicMedium, tvSubscribeSimple);
            UaeBarqUtils.FontUtils.applyFonts(this, Constant.strTypefaceArabicRegular, tvSubscribe);
        }
    }*/

   /* *//*set YouTube item*//*
    private void setYouTubeList(final YouTubeModel youTubeList) {
        View convertView;
        boolean isTrue = true;
        if (youTubeList != null) {
            for (int value = 0; value < youTubeList.getPayload().getVideos().size(); value++) {
                final Video videoList = youTubeList.getPayload().getVideos().get(value);
                convertView = layoutInflater.inflate(R.layout.youtube_list_item, null);
                TextView tvYoutubeTitle = convertView.findViewById(R.id.tv_youtube_title);
                ImageView ivRoundedImage = convertView.findViewById(R.id.iv_rounded_image);
                if (videoList != null && !UaeBarqUtils.getAppUtils(this).isNullOrEmpty(videoList.getImage())) {
                    Picasso.with(this)
                            .load(videoList.getImage())
                            .into(ivRoundedImage);
                }

                ivRoundedImage.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent webIntent = new Intent(HomeActivity.this, WebViewActivity.class);
                        webIntent.putExtra("index", 4);
                        webIntent.putExtra("url", videoList.getUrl());
                        startActivity(webIntent);
                    }
                });

                tvYoutubeTitle.setText(videoList.getTitle());
                if (isEnglishLanguage()) {
                    UaeBarqUtils.FontUtils.applyFonts(this, Constant.strTypefaceEnglishMedium, tvYoutubeTitle);
                } else {
                    UaeBarqUtils.FontUtils.applyFonts(this, Constant.strTypefaceArabicMedium, tvYoutubeTitle);
                    if (isTrue) {
                        hsTab.postDelayed(new Runnable() {
                            public void run() {
                                // Disable the animation First
                                hsYoutube.setSmoothScrollingEnabled(false);
                                hsYoutube.fullScroll(HorizontalScrollView.FOCUS_RIGHT);
                                hsYoutube.setSmoothScrollingEnabled(true);
                            }
                        }, 100L);
                        isTrue = false;
                    }
                }
                llYoutube.addView(convertView);
            }
        }
    }*/

    private void getTwitterData() {
        if (isOnline()) {
            getTwitterDataFromServer();
        } else {
            UaeBarqUtils.getAppUtils(this).showToast(this, getResources().getString(R.string.please_check_internet));
        }
    }

    private void getTwitterDataFromServer() {
        twitterCall = ApplicationData.getInstance().getRestClient().createService(NetworkPostServices.class)
                .getTwitterData(UaeBarqUtils.getAppUtils(this).getCurrentLang(this));
        twitterCall.enqueue(new Callback<TwitterModel>() {
            @Override
            public void onResponse(Call<TwitterModel> call, Response<TwitterModel> response) {
                try {
                    rlProgressLayout.setVisibility(View.GONE);
                    if (response.body() != null && response.body().getSuccess().equalsIgnoreCase("true")) {
                        sharedPreferenceManager.saveTwitterModel(response.body());
                        initializeTwitter();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<TwitterModel> call, Throwable t) {
                Log.e("", "" + t.getMessage());
            }
        });
    }

    private void initializeTwitter() {
        if (isArabicLanguage()) {
            ll_follow.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.flowbutton_ar));
        }
        rlProgressLayout.setVisibility(View.GONE);
        ArrayList<Feed> feedsList = null;
        twitterModel = sharedPreferenceManager.getTwitterModelFromPreferences();
        if (twitterModel != null) {
            feedsList = twitterModel.getPayload().getFeeds();
            //Twitter payLoadTwitter = twitterModel.getPayload().getTwitter();
            tvTweetsNo.setText(twitterModel.getPayload().getTweets().toString());
            tvFollowingsNo.setText(twitterModel.getPayload().getFollowing().toString());
            tvFollowersNo.setText(twitterModel.getPayload().getFollower().toString());
        }
        setTwitterList(feedsList);
        if (isEnglishLanguage()) {
            UaeBarqUtils.FontUtils.applyFonts(this, Constant.strTypefaceEnglishRegular, llSocialMedia);
        } else {
            UaeBarqUtils.FontUtils.applyFonts(this, Constant.strTypefaceArabicRegular, llSocialMedia);
        }
    }

    LinearLayoutManager mLayoutManager;

    private void setTwitterList(ArrayList<Feed> feedsList) {
        mLayoutManager = new LinearLayoutManager(this);
        if (isArabicLanguage()) {
            mLayoutManager.setReverseLayout(true);
            mLayoutManager.setStackFromEnd(true);
        } else {
            mLayoutManager.setReverseLayout(false);
            mLayoutManager.setStackFromEnd(false);
        }
        mLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        twSocialRecyclerView.setLayoutManager(mLayoutManager);
        if (feedsList != null && feedsList.size() > 0) {
            twitterAdapter = new TwitterAdapter(this, feedsList);
        }
        twSocialRecyclerView.setAdapter(twitterAdapter);
        if (twitterAdapter != null) {
            twitterAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void yourDesiredMethod() {
        final int pixelsToMove = twSocialRecyclerView.getWidth();
        if (isEnglishLanguage()) {
            twSocialRecyclerView.smoothScrollBy(pixelsToMove, 0);
        } else {
            twSocialRecyclerView.smoothScrollBy(-pixelsToMove, 0);
        }
        twSocialRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                int lastItem = mLayoutManager.findLastCompletelyVisibleItemPosition();
                if (lastItem == mLayoutManager.getItemCount() - 1) {
                    twSocialRecyclerView.setAdapter(null);
                    twSocialRecyclerView.setAdapter(twitterAdapter);
                }
            }
        });
    }

    private void getInstagramData() {
        if (isOnline()) {
            getInstagramDataFromServer();

        } else {
            UaeBarqUtils.getAppUtils(this).showToast(this, getResources().getString(R.string.please_check_internet));
        }
    }

    private void getInstagramDataFromServer() {
        instagramModelCall = ApplicationData.getInstance().getRestClient().createService(NetworkPostServices.class)
                .getInstagramData(UaeBarqUtils.getAppUtils(this).getCurrentLang(this));
        instagramModelCall.enqueue(new Callback<InstagramModel>() {
            @Override
            public void onResponse(Call<InstagramModel> call, Response<InstagramModel> response) {
                try {
                    rlProgressLayout.setVisibility(View.GONE);
                    if (response.body() != null && response.body().getSuccess().equalsIgnoreCase("true")) {
                        sharedPreferenceManager.saveInstagramModel(response.body());
                        initializeInstagram();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<InstagramModel> call, Throwable t) {
            }
        });
    }

    private void initializeInstagram() {
        rlProgressLayout.setVisibility(View.GONE);
        instagramModel = sharedPreferenceManager.getInstagramModelFromPreferences();
        if (instagramModel != null) {
            List<String> stringsList = instagramModel.getPayload();
            setInstagramList(stringsList);
        }
    }

    private void setInstagramList(List<String> setInstagramList) {
        GridLayoutManager gridLayoutManager;
        if (isArabicLanguage()) {
            gridLayoutManager = (new GridLayoutManager(this, 2, LinearLayoutManager.HORIZONTAL, true));
        } else {
            gridLayoutManager = new GridLayoutManager(this, 2, GridLayoutManager.HORIZONTAL, false);
        }

        inSocialRecyclerView.setHasFixedSize(true);
        inSocialRecyclerView.setLayoutManager(gridLayoutManager);
        int spanCount = 3; // 3 columns
        int spacing = 5; // 50px
        boolean includeEdge = true;
        inSocialRecyclerView.addItemDecoration(new GridSpacingItemDecoration(spanCount, spacing, includeEdge));
        if (setInstagramList != null && setInstagramList.size() > 0) {
            if (isArabicLanguage()) {
                Collections.reverse(setInstagramList);
                inSocialRecyclerView.scrollToPosition(setInstagramList.size() - 1);
            }
            instagramAdapter = new InstagramAdapter(this, setInstagramList);
        }
        inSocialRecyclerView.setAdapter(instagramAdapter);
        if (instagramAdapter != null) {
            instagramAdapter.notifyDataSetChanged();
        }
    }

    @OnClick(R.id.iv_lang)
    void languageClick() {
        Utils.trackEvent(this.getClass().getSimpleName(), FireBaseEvents.homelanguageChange);
        if (isEnglishLanguage()) {
            UaeBarqUtils.getAppUtils(this).setArabic(this);
        } else {
            UaeBarqUtils.getAppUtils(this).setEnglish(this);
        }
        sharedPreferenceManager.saveDashBoardModel(null);
        sharedPreferenceManager.saveYouTubeModel(null);
        refreshHome();
    }

    @Override
    protected void onPause() {
        super.onPause();
        handler.removeCallbacks(runnable);
    }

    private void getNotificationBundle() {

        Utils.trackEvent(this.getClass().getSimpleName(), FireBaseEvents.homeGetNotifications);
        Bundle intent = getIntent().getExtras();
        if (intent != null) {
            generic = intent.getBoolean("Generic");
            text = intent.getBoolean("text");
            type = intent.getString("type");
            image = intent.getBoolean("image");
            video = intent.getBoolean("video");
            message = intent.getString("message");
            gcmCategory = intent.getBoolean("Category");
            String Id = intent.getString("categoryId");
            premium = intent.getString("premium");
            if (!isNullOrEmpty(Id)) {
                categoryId = Integer.parseInt(Id);
            }
            if (generic) {
                llCongrats.setVisibility(View.VISIBLE);
                tvPurchased.setText(message);

            } else if (gcmCategory) {
               /*on resume */
            } else if (text) {
                // on Resume
            } else if (image) {
                // on Resume
            }
        }
    }

    @OnClick(R.id.iv_cross_image)
    void onClickOk() {
        llCongrats.setVisibility(View.GONE);
    }

    @OnClick(R.id.ll_ok)
    void onClickLLOk() {
        llCongrats.setVisibility(View.GONE);
    }

    private void openTutorial() {

        Utils.trackEvent(this.getClass().getSimpleName(), FireBaseEvents.homeOpenTutorialAcitivty);
        Intent intent = new Intent(HomeActivity.this, TutorialActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.ll_twitter)
    public void twitterClick() {

        Utils.trackEvent(this.getClass().getSimpleName(), FireBaseEvents.homeTwitterClick);
        llInstagramBg.setBackgroundResource(R.drawable.bg_instagram);
        ivInstagramIcon.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.instagram_logo_white));
        tvInstagramName.setTextColor(ContextCompat.getColor(this, android.R.color.white));
        llInstagramView.setVisibility(View.INVISIBLE);
        llTwitterView.setVisibility(View.VISIBLE);
        llFooter.setVisibility(View.VISIBLE);

        llTwitterBg.setBackgroundColor(ContextCompat.getColor(this, android.R.color.white));
        ivTwitterIcon.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.twitter));
        tvTwitterName.setTextColor(ContextCompat.getColor(this, R.color.twitter_color));
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT
        );
        params.setMargins(0, (int) getResources().getDimension(R.dimen.minus_three), 0, 0);
        llLine.setLayoutParams(params);
        dotProgressBar.setEndColor(ContextCompat.getColor(this, R.color.twitter_color));
        dotProgressBar.setStartColor(ContextCompat.getColor(this, R.color.twitter_color));
        if (twitterModel == null) {
            rlProgressLayout.setVisibility(View.VISIBLE);
            twSocialRecyclerView.setVisibility(View.VISIBLE);
            inSocialRecyclerView.setVisibility(View.GONE);
            getTwitterData();
        } else {
            twSocialRecyclerView.setVisibility(View.VISIBLE);
            inSocialRecyclerView.setVisibility(View.GONE);
        }
        LinearLayout.LayoutParams paramss = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT
                , (int) getResources().getDimension(R.dimen.one_fifty));
        llSocialMain.setLayoutParams(paramss);
    }

    @OnClick(R.id.ll_instagram)
    public void instagramClick() {

        Utils.trackEvent(this.getClass().getSimpleName(), FireBaseEvents.homeInstagaramClick);
        llTwitterBg.setBackgroundResource(R.drawable.twitter_btn_bg);
        ivTwitterIcon.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.twitter_wht));
        tvTwitterName.setTextColor(ContextCompat.getColor(this, android.R.color.white));
        llTwitterView.setVisibility(View.INVISIBLE);
        llInstagramView.setVisibility(View.VISIBLE);
        llFooter.setVisibility(View.GONE);
        llInstagramView.setBackgroundResource(R.drawable.instagram_line);

        llInstagramBg.setBackgroundColor(ContextCompat.getColor(this, android.R.color.white));
        ivInstagramIcon.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.instagram_logo_color));
        tvInstagramName.setTextColor(ContextCompat.getColor(this, R.color.instagram_color));
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT
        );
        params.setMargins(0, (int) getResources().getDimension(R.dimen.minus_seven), 0, 0);
        llLine.setLayoutParams(params);
        dotProgressBar.setEndColor(ContextCompat.getColor(this, R.color.instagram_color));
        dotProgressBar.setStartColor(ContextCompat.getColor(this, R.color.instagram_color));
        if (instagramModel == null) {
            rlProgressLayout.setVisibility(View.VISIBLE);
            twSocialRecyclerView.setVisibility(View.GONE);
            inSocialRecyclerView.setVisibility(View.VISIBLE);
            getInstagramData();
        } else {
            LinearLayout.LayoutParams paramss = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, llSocialMainHeight + llFooterHeight);
            llSocialMain.setLayoutParams(paramss);
            twSocialRecyclerView.setVisibility(View.GONE);
            inSocialRecyclerView.setVisibility(View.VISIBLE);
        }
    }

    private void setFooter() {
        ArrayList<Banner> banners = dashboardModel.getPayload().getBanner();
        if (banners != null && banners.size() != 0) {
            for (int i = 0; i < banners.size(); i++) {
                if (i == 0) {
                    tvFooter.setText(banners.get(0).getText());
                    Picasso.with(this)
                            .load(banners.get(0).getImge())
                            .into(ivFooter);
                }
                Singleton.getInstance().bannerImage = banners.get(0).getUrl();
                Singleton.getInstance().bannerText = banners.get(0).getText();
            }
        } else {
            main_footer.setVisibility(View.GONE);
        }
        if (isEnglishLanguage()) {
            UaeBarqUtils.FontUtils.applyFonts(this, Constant.strTypefaceEnglishMedium, tvFooter);
        } else {
            UaeBarqUtils.FontUtils.applyFonts(this, Constant.strTypefaceArabicMedium, tvFooter);
        }
    }


    @OnClick(R.id.iv_search)
    public void searchClick() {

        Utils.trackEvent(this.getClass().getSimpleName(), FireBaseEvents.homeSerachClick);
        Intent intent = new Intent(HomeActivity.this, SearchActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.iv_settings)
    void settingScreen() {

        Utils.trackEvent(this.getClass().getSimpleName(), FireBaseEvents.homeSettingActivity);
        Intent intent = new Intent(HomeActivity.this, SettingsActivity.class);
        startActivity(intent);
    }

   /* @OnClick(R.id.ll_sub_youtube)
    void youTubeSub() {
        UaeBarqUtils.getAppUtils(this).trackEvent(this.getClass().getSimpleName(),"youTube_videos", "image");
        UaeBarqUtils.getAppUtils(this).openYouTubePage(this);
    }*/

    @OnClick(R.id.main_footer)
    void footerClick() {
        Utils.trackEvent(this.getClass().getSimpleName(), FireBaseEvents.homeWebViewFooter);
        Intent webIntent = new Intent(HomeActivity.this, WebViewActivity.class);
        webIntent.putExtra("index", 1);
        startActivity(webIntent);
    }

    @OnClick(R.id.ll_follow)
    void llTwitterFollowClick() {
        Utils.trackEvent(this.getClass().getSimpleName(), FireBaseEvents.homeTwitterFollowClick);
        UaeBarqUtils.getAppUtils(this).openTwitterPage(this);
    }

    @Override
    protected int setLayoutId() {
        return R.layout.activity_main;
    }



    @Override
    protected void onResume() {
        super.onResume();
        fetchAppSettings();
        handler.postDelayed(runnable, delay);
        if (gcmCategory) {
            if (categoryId == 13 || categoryId == 8) {
                Utils.trackEvent(this.getClass().getSimpleName(), FireBaseEvents.homeOpenWeatherActivity);
                intent = new Intent(HomeActivity.this, WeatherActivity.class);
            } else {
                Utils.trackEvent(this.getClass().getSimpleName(), FireBaseEvents.homeAllCategoryList);
                intent = new Intent(HomeActivity.this, AllCategoryListActivity.class);
                Category category = new Category();
                category.setId(categoryId);
                category.setTitle("");
                intent.putExtra("catObject", category);
            }
            startActivity(intent);
        } else if (text) {
            checkUserAndOpenView();
        } else if (image) {
            checkUserAndOpenView();
        } else if (video) {
            checkUserAndOpenView();
        }
        if (Singleton.getInstance().fetchSettingsModel.getPayload() != null) {
            if (Singleton.getInstance().fetchSettingsModel.getPayload().isSocialMedia()) {
                llSocialMedia.setVisibility(View.VISIBLE);
            } else {
                llSocialMedia.setVisibility(View.GONE);
            }
        }else {
            llSocialMedia.setVisibility(View.GONE);
        }
    }

    private void checkUserAndOpenView() {
        if (premium.equalsIgnoreCase("true")) {
            Bundle bundle = new Bundle();
            CategoryPost categoryPost = new CategoryPost();
            categoryPost.setId(categoryId);
            bundle.putSerializable("catPostDetail", categoryPost);
            if (type.equalsIgnoreCase(Constant.VIDEO)) {
                Utils.trackEvent(this.getClass().getSimpleName(), FireBaseEvents.homeDetailVideoActivity);
                Intent video = new Intent(HomeActivity.this, DetailActivityVideo.class);
                video.putExtras(bundle);
                startActivity(video);
            } else {
                Utils.trackEvent(this.getClass().getSimpleName(), FireBaseEvents.homeOpenDetailImageActivity);
                Intent textImage = new Intent(HomeActivity.this, DetailActivityTextImage.class);
                textImage.putExtras(bundle);
                startActivity(textImage);
            }
        } else {
            openPremiumActivity();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }


}