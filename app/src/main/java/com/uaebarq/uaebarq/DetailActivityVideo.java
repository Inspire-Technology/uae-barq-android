package com.uaebarq.uaebarq;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatDelegate;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.danikula.videocache.CacheListener;
import com.danikula.videocache.HttpProxyCacheServer;
import com.github.silvestrpredko.dotprogressbar.DotProgressBar;
import com.greenfrvr.hashtagview.HashtagView;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.squareup.picasso.Picasso;
import com.uaebarq.uaebarq.models.AllCategoryListModel.Post;
import com.uaebarq.uaebarq.models.DashBoardModel.CategoryPost;
import com.uaebarq.uaebarq.models.DashBoardModel.Slider;
import com.uaebarq.uaebarq.models.DetailModel.Banner;
import com.uaebarq.uaebarq.models.DetailModel.DetailModel;
import com.uaebarq.uaebarq.models.DetailModel.Payload;
import com.uaebarq.uaebarq.retrofit.NetworkInterface.NetworkPostServices;
import com.uaebarq.uaebarq.singleton.Singleton;
import com.uaebarq.uaebarq.utils.Constant;
import com.uaebarq.uaebarq.utils.FireBaseEvents;
import com.uaebarq.uaebarq.utils.UaeBarqUtils;
import com.uaebarq.uaebarq.utils.Utils;

import java.io.File;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import butterknife.BindView;
import butterknife.OnClick;
import fm.jiecao.jcvideoplayer_lib.JCBuriedPoint;
import fm.jiecao.jcvideoplayer_lib.JCVideoPlayer;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.uaebarq.uaebarq.ApplicationData.video;

public class DetailActivityVideo extends BaseActivity implements CacheListener {

    Bundle bundle;
    Slider slider;

    @BindView(R.id.iv_logo)
    ImageView ivLogo;

    @BindView(R.id.tv_name)
    TextView tvName;

    @BindView(R.id.progress_layout)
    View progressLayout;

    @BindView(R.id.dot_progress_bar)
    DotProgressBar dotProgressBar;

    @BindView(R.id.main_detail)
    LinearLayout mainDetail;

    @BindView(R.id.fr_main)
    FrameLayout frMain;

    private Payload detailPayloadArrayList;

    @BindView(R.id.no_wifi_view)
    View noWifiView;

    @BindView(R.id.empty_view)
    View emptyView;

    @BindView(R.id.hash_tag)
    HashtagView hashTag;

    private String proxyUrl;

    @BindView(R.id.iv_share)
    ImageView ivShare;

    @BindView(R.id.video_controller)
    JCVideoPlayer videoController;

    @BindView(R.id.tv_counter_view)
    TextView tvCounterView;

    @BindView(R.id.tv_created)
    TextView tvCreated;

    @BindView(R.id.tv_title)
    TextView tvTitle;

    @BindView(R.id.tv_detail)
    TextView tvDetail;

    @BindView(R.id.toolbar_top)
    Toolbar toolBarTop;

    @BindView(R.id.iv_back)
    ImageView ivBack;

    @BindView(R.id.main_footer_ll)
    View mainFooter;

    @BindView(R.id.iv_footer)
    ImageView ivFooter;

    @BindView(R.id.tv_footer_text)
    TextView tvFooter;
    com.uaebarq.uaebarq.models.SearchModel.Payload searchPayload;
    CategoryPost categoryPost;
    Post post;
    Configuration config;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            Window w = getWindow();
            w.setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
        }
        super.onCreate(savedInstanceState);
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
        config = getResources().getConfiguration();
        if (config.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            mainDetail.setVisibility(View.GONE);
            mainFooter.setVisibility(View.GONE);
            RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT,
                    RelativeLayout.LayoutParams.MATCH_PARENT);
            frMain.setLayoutParams(params);
            FrameLayout.LayoutParams param = new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT,
                    FrameLayout.LayoutParams.MATCH_PARENT);
            videoController.setLayoutParams(param);
            UaeBarqUtils.getAppUtils(this).hideStatusBarOnTouch(DetailActivityVideo.this);
        }
        bundle = getIntent().getExtras();
        dotProgressBar.setEndColor(ContextCompat.getColor(this, R.color.tab_gray));
        dotProgressBar.setStartColor(ContextCompat.getColor(this, R.color.tab_gray));
        if (bundle != null) {
            slider = (Slider) bundle.getSerializable("catObjectDetail");
            categoryPost = (CategoryPost) bundle.getSerializable("catPostDetail");
            post = (Post) bundle.getSerializable("catPostListDetail");
            searchPayload = (com.uaebarq.uaebarq.models.SearchModel.Payload) bundle.getSerializable("searchObjDetail");
        }
        if (isArabicLanguage()) {
            Toolbar.LayoutParams l1 = new Toolbar.LayoutParams(Toolbar.LayoutParams.WRAP_CONTENT, Toolbar.LayoutParams.WRAP_CONTENT);
            l1.gravity = Gravity.LEFT;
            ivBack.setLayoutParams(l1);
            toolBarTop.removeAllViews();
            toolBarTop.addView(ivBack);
        }
        setVideoView();
        JCVideoPlayer.setJcBuriedPoint(jcBuriedPoint);
    }

    //init buried point listener whenever you want. in application is also ok
    JCBuriedPoint jcBuriedPoint = new JCBuriedPoint() {
        @Override
        public void POINT_START_ICON(String title, String url) {
            Log.i("Buried_Point", "POINT_START_ICON" + " title is : " + title + " url is : " + url);
            UaeBarqUtils.getAppUtils(getApplicationContext()).hideStatusBarOnTouch(DetailActivityVideo.this);

        }

        @Override
        public void POINT_START_THUMB(String title, String url) {
            Log.i("Buried_Point", "POINT_START_THUMB" + " title is : " + title + " url is : " + url);
        }

        @Override
        public void POINT_STOP(String title, String url) {
            Log.i("Buried_Point", "POINT_STOP" + " title is : " + title + " url is : " + url);
        }

        @Override
        public void POINT_STOP_FULLSCREEN(String title, String url) {
            Log.i("Buried_Point", "POINT_STOP_FULLSCREEN" + " title is : " + title + " url is : " + url);
        }

        @Override
        public void POINT_RESUME(String title, String url) {
            Log.i("Buried_Point", "POINT_RESUME" + " title is : " + title + " url is : " + url);
        }

        @Override
        public void POINT_RESUME_FULLSCREEN(String title, String url) {
            Log.i("Buried_Point", "POINT_RESUME_FULLSCREEN" + " title is : " + title + " url is : " + url);
        }

        @Override
        public void POINT_CLICK_BLANK(String title, String url) {
            Log.i("Buried_Point", "POINT_CLICK_BLANK" + " title is : " + title + " url is : " + url);
            final Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    UaeBarqUtils.getAppUtils(getApplicationContext()).hideStatusBarOnTouch(DetailActivityVideo.this);
                }
            }, 2000);
        }

        @Override
        public void POINT_CLICK_BLANK_FULLSCREEN(String title, String url) {
            Log.i("Buried_Point", "POINT_CLICK_BLANK_FULLSCREEN" + " title is : " + title + " url is : " + url);
        }

        @Override
        public void POINT_CLICK_SEEKBAR(String title, String url) {
            Log.i("Buried_Point", "POINT_CLICK_SEEKBAR" + " title is : " + title + " url is : " + url);
        }

        @Override
        public void POINT_CLICK_SEEKBAR_FULLSCREEN(String title, String url) {
            Log.i("Buried_Point", "POINT_CLICK_SEEKBAR_FULLSCREEN" + " title is : " + title + " url is : " + url);
        }

        @Override
        public void POINT_AUTO_COMPLETE(String title, String url) {
            Log.i("Buried_Point", "POINT_AUTO_COMPLETE" + " title is : " + title + " url is : " + url);
            UaeBarqUtils.getAppUtils(getApplicationContext()).hideStatusBarOnTouch(DetailActivityVideo.this);
        }

        @Override
        public void POINT_AUTO_COMPLETE_FULLSCREEN(String title, String url) {
            Log.i("Buried_Point", "POINT_AUTO_COMPLETE_FULLSCREEN" + " title is : " + title + " url is : " + url);
        }

        @Override
        public void POINT_ENTER_FULLSCREEN(String title, String url) {
            Log.i("Buried_Point", "POINT_ENTER_FULLSCREEN" + " title is : " + title + " url is : " + url);
        }

        @Override
        public void POINT_QUIT_FULLSCREEN(String title, String url) {
            Log.i("Buried_Point", "POINT_QUIT_FULLSCREEN" + " title is : " + title + " url is : " + url);
        }
    };

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        //  setBackGroundVisibility(true);
        setOrientationOfScreen();
    }

    private void setOrientationOfScreen() {
        config = getResources().getConfiguration();
        if (config.orientation == Configuration.ORIENTATION_PORTRAIT) {
            mainDetail.setVisibility(View.VISIBLE);
            mainFooter.setVisibility(View.VISIBLE);
            RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, UaeBarqUtils.getAppUtils(this).dpToPx(250));
            frMain.setLayoutParams(params);
            UaeBarqUtils.getAppUtils(this).hideStatusBarOnTouch(DetailActivityVideo.this);
        } else if (config.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            mainDetail.setVisibility(View.GONE);
            mainFooter.setVisibility(View.GONE);
            RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT,
                    RelativeLayout.LayoutParams.MATCH_PARENT);
            frMain.setLayoutParams(params);
            FrameLayout.LayoutParams param = new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT,
                    FrameLayout.LayoutParams.MATCH_PARENT);
            videoController.setLayoutParams(param);
            UaeBarqUtils.getAppUtils(this).hideStatusBarOnTouch(DetailActivityVideo.this);
        }
    }

    @Override
    protected int setLayoutId() {
        return R.layout.activity_detail_video;
    }

    private void setVideoView() {
        if (toolBarTop != null) {
            ivLogo.setVisibility(View.GONE);
            tvName.setVisibility(View.GONE);
            ivBack.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_arrow_back_trans));
        }
        toolBarTop.setBackgroundColor(ContextCompat.getColor(this, android.R.color.transparent));
        setData();
    }

    private void setData() {
        if (slider != null) {
            getDetailData(String.valueOf(slider.getId()));
        } else if (searchPayload != null) {
            getDetailData(String.valueOf(searchPayload.getId()));
        } else if (categoryPost != null) {
            getDetailData(String.valueOf(categoryPost.getId()));
        } else if (post != null) {
            getDetailData(String.valueOf(post.getId()));
        }
    }

    private void getDetailData(String id) {
        if (isOnline()) {
            getDetailVideos(id);
            if (noWifiView.getVisibility() == View.VISIBLE) {
                noWifiView.setVisibility(View.GONE);
                progressLayout.setVisibility(View.VISIBLE);
                emptyView.setVisibility(View.GONE);
            }
        } else {
            progressLayout.setVisibility(View.GONE);
            noWifiView.setVisibility(View.VISIBLE);
        }
    }

    public void getDetailVideos(String videoId) {
        Call<DetailModel> videosDetailModelCall = ApplicationData.getInstance().getRestClient().createService(NetworkPostServices.class)
                .getDetailData(UaeBarqUtils.getAppUtils(this).getCurrentLang(this), videoId, getUserId());
        videosDetailModelCall.enqueue(new Callback<DetailModel>() {
            @Override
            public void onResponse(Call<DetailModel> call, Response<DetailModel> response) {
                try {
                    progressLayout.setVisibility(View.GONE);
                    if (response.body() != null && response.body().getSuccess().equalsIgnoreCase("true")) {
                        mainDetail.setVisibility(View.VISIBLE);
                        detailPayloadArrayList = response.body().getPayload();
                        setDataFields();
                        if(isPremiumUser()){
                            mainFooter.setVisibility(View.GONE);
                        }else {
                            setFooter(response.body().getPayload().getBanner());
                        }
                    } else if (response.body().getMessage().equalsIgnoreCase("Your subscription is expired")) {
                        UaeBarqUtils.getAppUtils(getApplicationContext()).showToast(getApplicationContext(), getResources().getString(R.string.expired));
                        Intent intent = new Intent(DetailActivityVideo.this, PremiumCodeActivity.class);
                        startActivityForResult(intent, Constant.REQUEST_CODE_FINNISH);
                    } else {
                        emptyView.setVisibility(View.VISIBLE);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<DetailModel> call, Throwable t) {
                emptyView.setVisibility(View.VISIBLE);
                progressLayout.setVisibility(View.GONE);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Constant.REQUEST_CODE_FINNISH) {
            finish();
        }
    }
    private void setDataFields() {
        String title = "";
        if (!isNullOrEmpty(detailPayloadArrayList.getTitle())) {
            title = detailPayloadArrayList.getTitle();
        }
        HttpProxyCacheServer proxy = ApplicationData.getInstance().getProxy(this);
        proxy.registerCacheListener(this, detailPayloadArrayList.getVideo());
        proxyUrl = proxy.getProxyUrl(detailPayloadArrayList.getVideo());
        videoController.setUp(proxyUrl, title);
        ImageLoader.getInstance().displayImage(detailPayloadArrayList.getImage(), videoController.ivThumb);
        if (!isNullOrEmpty(detailPayloadArrayList.getTitle())) {
            tvTitle.setText(detailPayloadArrayList.getTitle());
        }
        if (!isNullOrEmpty(detailPayloadArrayList.getCreated())) {
            tvCreated.setText(detailPayloadArrayList.getCreated() + " | " + detailPayloadArrayList.getTimeAgo());
        }
        if (!isNullOrEmpty(detailPayloadArrayList.getDescriptions())) {
            tvDetail.setText(detailPayloadArrayList.getDescriptions().toString());
            UaeBarqUtils.getAppUtils(this).setTextView(tvDetail, DetailActivityVideo.this);
            UaeBarqUtils.getAppUtils(this).shareDataUsingIntent(ivShare, detailPayloadArrayList.getDescriptions().toString());
        }
    }


    private void setFooter(List<Banner> banners) {
        if (banners != null && banners.size() !=0) {
            for (int i = 0; i < banners.size(); i++) {
                if (i == 0) {
                    tvFooter.setText(banners.get(0).getText());
                    Picasso.with(this)
                            .load(banners.get(0).getImge())
                            .into(ivFooter);
                }
                Singleton.getInstance().bannerImage = banners.get(0).getUrl();
                Singleton.getInstance().bannerText = banners.get(0).getText();
            }
        }else{
            mainFooter.setVisibility(View.GONE);
        }
        if (isEnglishLanguage()) {
            UaeBarqUtils.FontUtils.applyFonts(this, Constant.strTypefaceEnglishMedium, tvFooter);
        } else {
            UaeBarqUtils.FontUtils.applyFonts(this, Constant.strTypefaceArabicMedium, tvFooter);
        }
    }

    @OnClick(R.id.main_footer)
    void footerClick() {

        Utils.trackEvent(this.getClass().getSimpleName(), FireBaseEvents.galleryScreenFooterClickOpenWebView);
        Intent webIntent = new Intent(DetailActivityVideo.this, WebViewActivity.class);
        webIntent.putExtra("index", 1);
        startActivity(webIntent);
    }

    @OnClick(R.id.iv_back)
    void setImageClick() {
//        if (JCVideoPlayer.isPlay) {
////            setBackGroundVisibility(false);
////            if (fromNotification) {
////                intentForHomeScreen();
////            }
        finish();
        video = false;
        //}
    }

    @Override
    protected void onPause() {
        super.onPause();
        JCVideoPlayer.releaseAllVideos();
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        // Reactivate sensor orientation after delay
        Timer mRestoreOrientation = new Timer();
        mRestoreOrientation.schedule(new TimerTask() {
            @Override
            public void run() {
                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR);
            }
        }, 5000);
    }

    @Override
    protected void onResume() {
        super.onResume();
//        if (!JCVideoPlayer.exitFullScreen) {
//            setBackGroundVisibility(true);
//        }
        UaeBarqUtils.getAppUtils(this).hideStatusBarOnTouch(DetailActivityVideo.this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        ApplicationData.getProxy(this).unregisterCacheListener(this);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        video = false;
    }

    @Override
    public void onCacheAvailable(File cacheFile, String url, int percentsAvailable) {
    }
}
