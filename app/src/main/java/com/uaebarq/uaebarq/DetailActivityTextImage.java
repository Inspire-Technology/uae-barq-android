package com.uaebarq.uaebarq;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.github.silvestrpredko.dotprogressbar.DotProgressBar;
import com.squareup.picasso.Picasso;
import com.uaebarq.uaebarq.models.AllCategoryListModel.Post;
import com.uaebarq.uaebarq.models.DashBoardModel.CategoryPost;
import com.uaebarq.uaebarq.models.DashBoardModel.Slider;
import com.uaebarq.uaebarq.models.DetailModel.DetailModel;
import com.uaebarq.uaebarq.models.DetailModel.Payload;
import com.uaebarq.uaebarq.retrofit.NetworkInterface.NetworkPostServices;
import com.uaebarq.uaebarq.singleton.Singleton;
import com.uaebarq.uaebarq.utils.Constant;
import com.uaebarq.uaebarq.utils.FireBaseEvents;
import com.uaebarq.uaebarq.utils.UaeBarqUtils;
import com.uaebarq.uaebarq.utils.Utils;

import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.uaebarq.uaebarq.ApplicationData.image;
import static com.uaebarq.uaebarq.ApplicationData.text;

public class DetailActivityTextImage extends BaseActivity {

    Bundle bundle;
    Slider slider;

    @BindView(R.id.iv_logo)
    ImageView ivLogo;

    @BindView(R.id.tv_name)
    TextView tvName;

    @BindView(R.id.progress_layout)
    View progressLayout;

    @BindView(R.id.dot_progress_bar)
    DotProgressBar dotProgressBar;

    @BindView(R.id.main_detail)
    LinearLayout mainDetail;

    Payload detailPayloadArrayList;

    @BindView(R.id.no_wifi_view)
    View noWifiView;

    @BindView(R.id.empty_view)
    View emptyView;

    @BindView(R.id.tv_created)
    TextView tvCreated;

    @BindView(R.id.tv_title)
    TextView tvTitle;

    @BindView(R.id.tv_detail)
    TextView tvDetail;

    @BindView(R.id.toolbar)
    Toolbar toolBarTop;

    @BindView(R.id.iv_back)
    ImageView ivBack;

    @BindView(R.id.fr_main)
    FrameLayout frMain;

    @BindView(R.id.tv_tag)
    TextView tvTag;

    @BindView(R.id.main_footer_ll)
    View mainFooter;

    @BindView(R.id.iv_image)
    ImageView ivImage;

    @BindView(R.id.rl_tag)
    LinearLayout rlTag;

    @BindView(R.id.ll_horizontal_view)
    LinearLayout llHorizontalView;

    @BindView(R.id.hs_horizontal_view)
    HorizontalScrollView hsHorizontalView;

    private LayoutInflater layoutInflater;

    @BindView(R.id.main)
    RelativeLayout main;

    @BindView(R.id.tv_social_text)
    TextView tvSocialText;

    @BindView(R.id.iv_share)
    ImageView ivShare;

    com.uaebarq.uaebarq.models.SearchModel.Payload searchPayload;

    @BindView(R.id.iv_footer)
    ImageView ivFooter;

    @BindView(R.id.tv_footer_text)
    TextView tvFooter;

    CategoryPost categoryPost;

    @BindView(R.id.ll_related_images)
    LinearLayout llRelatedImages;

    Post post;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        layoutInflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
        dotProgressBar.setEndColor(ContextCompat.getColor(this, R.color.tab_gray));
        dotProgressBar.setStartColor(ContextCompat.getColor(this, R.color.tab_gray));
        bundle = getIntent().getExtras();
        if (bundle != null) {
            slider = (Slider) bundle.getSerializable("catObjectDetail");
            categoryPost = (CategoryPost) bundle.getSerializable("catPostDetail");
            post = (Post) bundle.getSerializable("catPostListDetail");
            searchPayload = (com.uaebarq.uaebarq.models.SearchModel.Payload) bundle.getSerializable("searchObjDetail");
        }
        setView();
    }

    @Override
    protected int setLayoutId() {
        return R.layout.activity_detail_image_text;
    }

    private void setView() {
        if (toolBarTop != null) {
            ivLogo.setVisibility(View.GONE);
            tvName.setVisibility(View.VISIBLE);
        }
        setFont();
        setData();
    }

    private void setData() {
        if (slider != null) {
            getDetailData(String.valueOf(slider.getId()));
            if(!isNullOrEmpty(slider.getCategoryTitle())) {
                int color = UaeBarqUtils.getAppUtils(this).getColor(this, slider.getCategoryTitle());
                toolBarTop.setBackgroundColor(color);
                Drawable drawable = UaeBarqUtils.getAppUtils(this).getDrawable(this, slider.getCategoryTitle());
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                    rlTag.setBackground(drawable);
                }
                tvName.setText(this.slider.getCategoryTitle());
                tvTag.setText(this.slider.getCategoryTitle());
            }
        } else if (searchPayload != null) {
            getDetailData(String.valueOf(searchPayload.getId()));
            if(!isNullOrEmpty(searchPayload.getCategoryTitle())){
                int color = UaeBarqUtils.getAppUtils(this).getColor(this, searchPayload.getCategoryTitle());
                toolBarTop.setBackgroundColor(color);
                Drawable drawable = UaeBarqUtils.getAppUtils(this).getDrawable(this, searchPayload.getCategoryTitle());
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                    rlTag.setBackground(drawable);
                }
                tvName.setText(this.searchPayload.getCategoryTitle());
                tvTag.setText(this.searchPayload.getCategoryTitle());
            }

        } else if (categoryPost != null) {
            getDetailData(String.valueOf(categoryPost.getId()));
            setDataCategoryPost();
        } else if (post != null) {
            getDetailData(String.valueOf(post.getId()));
            if(!isNullOrEmpty(post.getCategoryTitle())) {
                int color = UaeBarqUtils.getAppUtils(this).getColor(this, post.getCategoryTitle());
                toolBarTop.setBackgroundColor(color);
                Drawable drawable = UaeBarqUtils.getAppUtils(this).getDrawable(this, post.getCategoryTitle());
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                    rlTag.setBackground(drawable);
                }
                tvName.setText(this.post.getCategoryTitle());
                tvTag.setText(this.post.getCategoryTitle());
            }
        }

    }

    private void setDataCategoryPost() {
        if(detailPayloadArrayList !=null && categoryPost !=null){
            categoryPost.setCategoryTitle(detailPayloadArrayList.getCategoryTitle());
        }else {
            return;
        }
        if(!isNullOrEmpty(categoryPost.getCategoryTitle())) {
            int color = UaeBarqUtils.getAppUtils(this).getColor(this, categoryPost.getCategoryTitle());
            toolBarTop.setBackgroundColor(color);
            Drawable drawable = UaeBarqUtils.getAppUtils(this).getDrawable(this, categoryPost.getCategoryTitle());
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                rlTag.setBackground(drawable);
            }
            tvName.setText(this.categoryPost.getCategoryTitle());
            tvTag.setText(this.categoryPost.getCategoryTitle());
        }
    }

    void setFont() {
        if (isEnglishLanguage()) {
            UaeBarqUtils.FontUtils.applyFonts(this, Constant.strTypefaceEnglishMedium, main);
            UaeBarqUtils.FontUtils.applyFonts(this, Constant.strTypefaceEnglishRegular, tvTag);
            UaeBarqUtils.FontUtils.applyFonts(this, Constant.strTypefaceEnglishRegular, tvCreated);
            UaeBarqUtils.FontUtils.applyFonts(this, Constant.strTypefaceEnglishRegular, tvDetail);
            UaeBarqUtils.FontUtils.applyFonts(this, Constant.strTypefaceEnglishRegular, tvSocialText);
        } else {
            UaeBarqUtils.FontUtils.applyFonts(this, Constant.strTypefaceArabicMedium, main);
            UaeBarqUtils.FontUtils.applyFonts(this, Constant.strTypefaceArabicRegular, tvTag);
            UaeBarqUtils.FontUtils.applyFonts(this, Constant.strTypefaceArabicRegular, tvCreated);
            UaeBarqUtils.FontUtils.applyFonts(this, Constant.strTypefaceArabicRegular, tvDetail);
            UaeBarqUtils.FontUtils.applyFonts(this, Constant.strTypefaceArabicRegular, tvSocialText);
        }
    }

    private void getDetailData(String id) {
        if (isOnline()) {
            getDetailTextImage(id);
            if (noWifiView.getVisibility() == View.VISIBLE) {
                noWifiView.setVisibility(View.GONE);
                progressLayout.setVisibility(View.VISIBLE);
                emptyView.setVisibility(View.GONE);
            }
        } else {
            progressLayout.setVisibility(View.GONE);
            noWifiView.setVisibility(View.VISIBLE);
        }
    }

    public void getDetailTextImage(String videoId) {
        Call<DetailModel> videosDetailModelCall = ApplicationData.getInstance().getRestClient().createService(NetworkPostServices.class)
                .getDetailData(UaeBarqUtils.getAppUtils(this).getCurrentLang(this), videoId, getUserId());
        videosDetailModelCall.enqueue(new Callback<DetailModel>() {
            @Override
            public void onResponse(Call<DetailModel> call, Response<DetailModel> response) {
                try {
                    progressLayout.setVisibility(View.GONE);
                    if (response.body() != null && response.body().getSuccess().equalsIgnoreCase("true")) {
                        mainDetail.setVisibility(View.VISIBLE);
                        detailPayloadArrayList = response.body().getPayload();
                        setDataFields();
                        if(isPremiumUser()){
                            mainFooter.setVisibility(View.GONE);
                        }else {
                            setFooter(response.body().getPayload().getBanner());
                        }
                    } else if (response.body().getMessage().equalsIgnoreCase("Your subscription is expired")) {
                        UaeBarqUtils.getAppUtils(getApplicationContext()).showToast(getApplicationContext(), getResources().getString(R.string.expired));

                        Utils.trackEvent(this.getClass().getSimpleName(), FireBaseEvents.detailTextImageScreenOpenPremiumActivity);
                        Intent intent = new Intent(DetailActivityTextImage.this, PremiumCodeActivity.class);
                        startActivityForResult(intent, Constant.REQUEST_CODE_FINNISH);
                    } else {
                        emptyView.setVisibility(View.VISIBLE);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<DetailModel> call, Throwable t) {
                emptyView.setVisibility(View.VISIBLE);
                progressLayout.setVisibility(View.GONE);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Constant.REQUEST_CODE_FINNISH) {
            finish();
        }
    }

    private void setFooter(List<com.uaebarq.uaebarq.models.DetailModel.Banner> banners) {
        if (banners != null && banners.size() !=0) {
            for (int i = 0; i < banners.size(); i++) {
                if (i == 0) {
                    tvFooter.setText(banners.get(0).getText());
                    Picasso.with(this)
                            .load(banners.get(0).getImge())
                            .into(ivFooter);
                }
                Singleton.getInstance().bannerImage = banners.get(0).getUrl();
                Singleton.getInstance().bannerText = banners.get(0).getText();
            }
        }else{
            mainFooter.setVisibility(View.GONE);
        }
        if (isEnglishLanguage()) {
            UaeBarqUtils.FontUtils.applyFonts(this, Constant.strTypefaceEnglishMedium, tvFooter);
        } else {
            UaeBarqUtils.FontUtils.applyFonts(this, Constant.strTypefaceArabicMedium, tvFooter);
        }
    }

    private void setDataFields() {
        setDataCategoryPost();
        if (!isNullOrEmpty(detailPayloadArrayList.getTitle())) {
            tvTitle.setText(detailPayloadArrayList.getTitle());
        }
        if (!isNullOrEmpty(detailPayloadArrayList.getCreated())) {
            tvCreated.setText(detailPayloadArrayList.getCreated() + " | " + detailPayloadArrayList.getTimeAgo());
        }
        if (!isNullOrEmpty(detailPayloadArrayList.getDescriptions())) {
            tvDetail.setText(detailPayloadArrayList.getDescriptions().toString());

            UaeBarqUtils.getAppUtils(this).setTextView(tvDetail, DetailActivityTextImage.this);
        }
        if (detailPayloadArrayList != null && !isNullOrEmpty(detailPayloadArrayList.getImage())) {
                Picasso.with(this)
                        .load(detailPayloadArrayList.getImage())
                        .into(ivImage);
            ivImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    setIntentForGallery(detailPayloadArrayList.getImage());
                }
            });
        }
        UaeBarqUtils.getAppUtils(this).shareDataUsingIntent(ivShare, detailPayloadArrayList.getDescriptions().toString());

        if (isArabicLanguage()) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                mainDetail.setLayoutDirection(View.LAYOUT_DIRECTION_RTL);
            }
            new Handler().postDelayed(new Runnable() {
                public void run() {
                    hsHorizontalView.fullScroll(HorizontalScrollView.FOCUS_RIGHT);
                }
            }, 100L);
        }
        setRelatedVideosLayout();
    }

    private void setRelatedVideosLayout() {
        View convertView;
        llHorizontalView.removeAllViews();
        if (detailPayloadArrayList.getRelatedImages().size() > 0) {
            for (int j = 0; j < detailPayloadArrayList.getRelatedImages().size(); j++) {
                convertView = layoutInflater.inflate(R.layout.related_image_item, null);
                ImageView ivRelatedVideo = convertView.findViewById(R.id.iv_related);
                Picasso.with(this)
                        .load(detailPayloadArrayList.getRelatedImages().get(j).toString())
                        .into(ivRelatedVideo);
                ivRelatedVideo.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        Utils.trackEvent(this.getClass().getSimpleName(), FireBaseEvents.detailTextImageScreenVideoLayoutClick);
                        String relatedImage = (String) view.getTag();
                        setIntentForGallery(relatedImage);
                    }
                });

                ivRelatedVideo.setTag(detailPayloadArrayList.getRelatedImages().get(j));
                llHorizontalView.addView(convertView);
            }
        } else {
            llRelatedImages.setVisibility(View.GONE);
        }
    }

    @OnClick(R.id.main_footer)
    void footerClick() {
        Utils.trackEvent(this.getClass().getSimpleName(), FireBaseEvents.detailTextImageScreenOpenWebViewActivity);
        Intent webIntent = new Intent(DetailActivityTextImage.this, WebViewActivity.class);
        webIntent.putExtra("index", 1);
        startActivity(webIntent);
    }

    private void setIntentForGallery(String image) {

        Utils.trackEvent(this.getClass().getSimpleName(), FireBaseEvents.detailTextImageScreenOpenGalleryActivity);
        Intent intent = new Intent(DetailActivityTextImage.this, GalleryViewActivity.class);
        intent.putExtra("image", image);
        startActivity(intent);
    }

    @OnClick(R.id.iv_back)
    void setImageClick() {
        sendBack();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        sendBack();
    }

    private void sendBack() {
        text = false;
        image = false;
        finish();
    }
}
