package com.uaebarq.uaebarq;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.github.silvestrpredko.dotprogressbar.DotProgressBar;
import com.uaebarq.uaebarq.adapter.SearchAdapter;
import com.uaebarq.uaebarq.models.SearchModel.Payload;
import com.uaebarq.uaebarq.models.SearchModel.SearchModel;
import com.uaebarq.uaebarq.retrofit.NetworkInterface.NetworkPostServices;
import com.uaebarq.uaebarq.utils.Constant;
import com.uaebarq.uaebarq.utils.FireBaseEvents;
import com.uaebarq.uaebarq.utils.UaeBarqUtils;
import com.uaebarq.uaebarq.utils.Utils;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SearchActivity extends BaseActivity implements AdapterView.OnItemClickListener {

    @BindView(R.id.topBar)
    LinearLayout topBar;

    public static RelativeLayout rlBlack;

    @BindView(R.id.et_search)
    EditText etSearch;

    @BindView(R.id.progress_layout)
    View progressLayout;

    @BindView(R.id.dot_progress_bar)
    DotProgressBar dotProgressBar;

    @BindView(R.id.search_list)
    ListView searchList;

    @BindView(R.id.rl_main)
    RelativeLayout rlMain;

    SearchAdapter searchAdapter;

    ArrayList<Payload> SearchModelArrayList;
    private Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        rlBlack = findViewById(R.id.rl_black);
        dotProgressBar.setEndColor(ContextCompat.getColor(this, R.color.tab_gray));
        dotProgressBar.setStartColor(ContextCompat.getColor(this, R.color.tab_gray));

        SearchModelArrayList = new ArrayList<>();
        searchList.setOnItemClickListener(this);
        etSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int actionId, KeyEvent keyEvent) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    searchClick();
                    return true;
                }
                return false;
            }
        });
    }

    @OnClick(R.id.iv_cross)
    void crossClick() {

        Utils.trackEvent(this.getClass().getSimpleName(), FireBaseEvents.searchScreenSearchClick);
        if (etSearch.getText().toString().trim().equalsIgnoreCase("")) {
            finish();
        } else {
            etSearch.setText("");
        }
    }


    @OnClick(R.id.iv_search_image)
    void searchClick() {
        if (isValidate()) {
            if (isOnline()) {
                progressLayout.setVisibility(View.VISIBLE);
                searchList.setVisibility(View.GONE);
                UaeBarqUtils.getAppUtils(this).hideKeyBoard(etSearch, this);
                String stringData = etSearch.getText().toString().trim();
                sendSearchCall(stringData);
            } else {
                UaeBarqUtils.getAppUtils(this).showToast(this, getResources().getString(R.string.please_check_internet));
            }
        }
    }

    public boolean isValidate() {
        boolean valid = true;
        String strEmail = etSearch.getText().toString().trim();
        if (isNullOrEmpty(strEmail)) {
            valid = false;
        }
        return valid;
    }

    private void sendSearchCall(String strEditTest) {
        Call<SearchModel> searchModelCall = ApplicationData.getInstance().getRestClient().createService(NetworkPostServices.class).
                getSearchData(Constant.API_KEY, strEditTest, UaeBarqUtils.getAppUtils(this).getCurrentLang(this));
        searchModelCall.enqueue(new Callback<SearchModel>() {
            @Override
            public void onResponse(Call<SearchModel> call, Response<SearchModel> response) {
                try {
                    progressLayout.setVisibility(View.GONE);
                    searchList.setVisibility(View.VISIBLE);
                    if (response.body().getPayload() != null) {
                        if (response.body().getSuccess().equalsIgnoreCase("true")) {
                            SearchModel searchModel = response.body();
                            if (searchModel != null && searchModel.getPayload().size() > 0) {
                                SearchModelArrayList.clear();
                                SearchModelArrayList.addAll(searchModel.getPayload());
                                setSearchAdapter(SearchModelArrayList);
                            }
                        }
                    } else {
                        UaeBarqUtils.getAppUtils(getApplicationContext()).showToast(getApplicationContext(), response.body().getMessage().toString());
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    progressLayout.setVisibility(View.GONE);
                    searchList.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onFailure(Call<SearchModel> call, Throwable t) {
                Log.e("Throwable = ", "" + t.getMessage());
                progressLayout.setVisibility(View.GONE);
                searchList.setVisibility(View.VISIBLE);
            }
        });
    }

    private void setSearchAdapter(ArrayList<Payload> SearchModelArrayList) {
        searchAdapter = new SearchAdapter(this, SearchModelArrayList);
        searchList.setAdapter(searchAdapter);
        // searchList.setBackgroundColor(ContextCompat.getColor(this, R.color.white));
        //rlMain.setBackgroundColor(ContextCompat.getColor(this, R.color.off_white));
    }

    public static void setBackGround(boolean isTrue) {
        if (isTrue) {
            rlBlack.setVisibility(View.VISIBLE);
        } else {
            rlBlack.setVisibility(View.GONE);
        }
    }


    @Override
    protected int setLayoutId() {
        return R.layout.search_layout;
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
        Payload itemAtPosition = (Payload) adapterView.getItemAtPosition(position);
        Bundle bundle = new Bundle();
        bundle.putSerializable("searchObjDetail", itemAtPosition);
        if (itemAtPosition.isFree()) {
            passIntent(itemAtPosition, bundle);
        } else {
            if (isPremiumUser()) {
                passIntent(itemAtPosition, bundle);
            } else {
                Utils.trackEvent(this.getClass().getSimpleName(), FireBaseEvents.searchScreenOpenPremiumActivity);
                Intent intent = new Intent(SearchActivity.this, PremiumCodeActivity.class);
                startActivity(intent);
            }
        }
    }

    private void passIntent(Payload payload, Bundle bundle) {
        if (payload.getType().equalsIgnoreCase(Constant.VIDEO)) {
            Utils.trackEvent(this.getClass().getSimpleName(), FireBaseEvents.searchScreenDetailVideoActivity);
            intent = new Intent(SearchActivity.this, DetailActivityVideo.class);
        }else {
            Utils.trackEvent(this.getClass().getSimpleName(), FireBaseEvents.searchScreenDetailTextActivity);
            intent = new Intent(SearchActivity.this, DetailActivityTextImage.class);
        }
        intent.putExtras(bundle);
        startActivity(intent);
    }

    @Override
    public void onBackPressed() {
        if (SearchModelArrayList.size() > 0) {
            SearchModelArrayList.clear();
            setSearchAdapter(SearchModelArrayList);
            // searchList.setBackgroundColor(ContextCompat.getColor(this, R.color.transparent));
            //rlMain.setBackgroundColor(ContextCompat.getColor(this, R.color.semi_transparent));
        } else {
            super.onBackPressed();
        }
    }
}
