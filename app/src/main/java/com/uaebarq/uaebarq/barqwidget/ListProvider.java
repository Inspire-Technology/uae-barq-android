package com.uaebarq.uaebarq.barqwidget;

import android.appwidget.AppWidgetManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.view.View;
import android.widget.RemoteViews;
import android.widget.RemoteViewsService.RemoteViewsFactory;

import com.squareup.picasso.Picasso;
import com.uaebarq.uaebarq.R;
import com.uaebarq.uaebarq.models.DashBoardModel.CategoryPost;
import com.uaebarq.uaebarq.models.DashBoardModel.DashboardModel;
import com.uaebarq.uaebarq.preferences.SharedPreferenceManager;
import com.uaebarq.uaebarq.utils.UaeBarqUtils;

import java.io.IOException;
import java.util.ArrayList;


/**
 * If you are familiar with Adapter of ListView,this is the same as adapter
 * with few changes
 */
public class ListProvider implements RemoteViewsFactory {
    private ArrayList<CategoryPost> listItemList = new ArrayList<>();
    private Context context = null;
    private int appWidgetId;

    public ListProvider(Context context, Intent intent) {
        this.context = context;
        DashboardModel userStatusModel = SharedPreferenceManager.getSharedInstance().getDashBoardModelFromPreferences();
        if (userStatusModel != null) {
            listItemList = userStatusModel.getPayload().getCategoryPosts();
        }
        appWidgetId = intent.getIntExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, AppWidgetManager.INVALID_APPWIDGET_ID);

    }

    @Override
    public int getCount() {
        return listItemList.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public RemoteViews getViewAt(int position) {
        final RemoteViews remoteViews = new RemoteViews(context.getPackageName(), R.layout.widget_list_row);//widget_list_row
        CategoryPost categoryPost = listItemList.get(position);

        if(!categoryPost.isFree()){
            remoteViews.setViewVisibility(R.id.btn_subscribe, View.GONE);
        }else {
            remoteViews.setViewVisibility(R.id.btn_subscribe, View.VISIBLE);
        }
        if(!UaeBarqUtils.getAppUtils(context).isNullOrEmpty(categoryPost.getCategoryTitle())){
            remoteViews.setTextViewText(R.id.tv_title, categoryPost.getCategoryTitle());
        }else {
            remoteViews.setTextViewText(R.id.tv_title, "Soon data will appeared and you will enjoy all in one place. Hope you will enjoy");
        }
//        Bundle extras = new Bundle();
//        extras.putInt(WidgetProvider.EXTRA_ITEM, app_status.getFileNo());
//        Intent fillInIntent = new Intent();
//        fillInIntent.putExtras(extras);
        //remoteViews.setOnClickFillInIntent(R.id.row, fillInIntent);
        try {
            Bitmap b = Picasso.with(context).load("http://courseware.codeschool.com/try_django/images/treasuregram-fools-gold.png").get();
            remoteViews.setImageViewBitmap(R.id.iv_image, b);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return remoteViews;
    }

    @Override
    public RemoteViews getLoadingView() {
        return null;
    }

    @Override
    public int getViewTypeCount() {
        return 1;
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public void onCreate() {
    }

    @Override
    public void onDataSetChanged() {
        listItemList = new ArrayList<>();
        DashboardModel dashboardModel = SharedPreferenceManager.getSharedInstance().getDashBoardModelFromPreferences();
        if (dashboardModel != null) {
            listItemList = dashboardModel.getPayload().getCategoryPosts();
        }
    }

    @Override
    public void onDestroy() {
    }
}
