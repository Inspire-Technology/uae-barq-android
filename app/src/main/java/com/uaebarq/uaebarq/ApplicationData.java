package com.uaebarq.uaebarq;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.multidex.MultiDex;
import android.support.multidex.MultiDexApplication;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

import com.appsflyer.AppsFlyerLib;
import com.danikula.videocache.HttpProxyCacheServer;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.nostra13.universalimageloader.cache.disc.naming.Md5FileNameGenerator;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import com.uaebarq.uaebarq.preferences.SharedPreferenceManager;
import com.uaebarq.uaebarq.retrofit.RestClient;
import com.uaebarq.uaebarq.singleton.Singleton;
import com.uaebarq.uaebarq.utils.Constant;
import com.uaebarq.uaebarq.utils.LocaleHelper;
import com.uaebarq.uaebarq.utils.UaeBarqUtils;
import com.uaebarq.uaebarq.utils.Utils;

import java.io.File;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;


@SuppressWarnings("deprecation")
public class ApplicationData extends MultiDexApplication {

    public static RestClient restClient;
    public static final int MAX_WIDTH = 1024;
    public static final int MAX_HEIGHT = 768;
    public static int size = (int) Math.ceil(Math.sqrt(MAX_WIDTH * MAX_HEIGHT));
    public static Bundle bundle;
    public static File file;
    private HttpProxyCacheServer proxy;
    private static ApplicationData mInstance;
    public static boolean gcmCategory;
    public static boolean text;
    public static boolean image;
    public static boolean video;
    public static FirebaseAnalytics mFirebaseAnalytics;
    public static boolean isPremium = false;
    public static boolean isActiveUser = false;
    public static boolean fromSDK = false;


    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;
        //Configure Mobibox
        String lang = Locale.getDefault().toString();
        boolean showLogs = false; //To be set to false before publishing
        // Obtain the FirebaseAnalytics instance.
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        initUniversalImageLoader();
        initSingletons();
        SharedPreferenceManager.getSharedInstance().initiateSharedPreferences(this);
//        if (this.getPackageName().equalsIgnoreCase(getResources().getString(R.string.package_uaebarq))) {
//            LocaleHelper.onCreate(this, Singleton.getInstance().arabic);
//        } else {
//            LocaleHelper.onCreate(this, Singleton.getInstance().english);
//        }
        AppsFlyerLib.getInstance().startTracking(this, Constant.AppsFlyer_Device_Key);
    }

    public static FirebaseAnalytics getFireBaseInstance() {
        return mFirebaseAnalytics;
    }


    @Override
    public Context getApplicationContext() {
        return super.getApplicationContext();
    }


    public static synchronized ApplicationData getInstance() {
        return mInstance;
    }

//    public synchronized Tracker getGoogleAnalyticsTracker() {
//        AnalyticsTrackers analyticsTrackers = AnalyticsTrackers.getInstance();
//        return analyticsTrackers.get(AnalyticsTrackers.Target.APP);
//    }

//    /***
//     * Tracking screen view
//     *
//     * @param screenName screen name to be displayed on GA dashboard
//     */
//    public void trackScreenView(String screenName) {
//        Tracker t = getGoogleAnalyticsTracker();
//
//        // Set screen name.
//        t.setScreenName(screenName);
//
//        // Send a screen view.
//        t.send(new HitBuilders.ScreenViewBuilder().build());
//
//        GoogleAnalytics.getInstance(this).dispatchLocalHits();
//    }

//    /***
//     * Tracking exception
//     *
//     * @param e exception to be tracked
//     */
//    public void trackException(Exception e) {
//        if (e != null) {
//            Tracker t = getGoogleAnalyticsTracker();
//
//            t.send(new HitBuilders.ExceptionBuilder()
//                    .setDescription(
//                            new StandardExceptionParser(this, null)
//                                    .getDescription(Thread.currentThread().getName(), e))
//                    .setFatal(false)
//                    .build()
//            );
//        }
//    }

//    /***
//     * Tracking event
//     *
//     * @param category event category
//     * @param action   action of the event
//     * @param label    label
//     */
//    public void trackEvent(String category, String action, String label) {
//        Tracker t = getGoogleAnalyticsTracker();
//
//        // Build and send an Event.
//        t.send(new HitBuilders.EventBuilder().setCategory(category).setAction(action).setLabel(label).build());
//    }

    @Override
    protected void attachBaseContext(Context base) {
        if (base.getPackageName().equalsIgnoreCase(base.getResources().getString(R.string.package_uaebarq))) {
            super.attachBaseContext(LocaleHelper.onAttach(base, "ar"));
        } else {
            super.attachBaseContext(LocaleHelper.onAttach(base, "en"));
        }
        MultiDex.install(this);
    }

    public static HttpProxyCacheServer getProxy(Context context) {
        ApplicationData app = (ApplicationData) context.getApplicationContext();
        return app.proxy == null ? (app.proxy = app.newProxy()) : app.proxy;
    }

    private HttpProxyCacheServer newProxy() {
        return new HttpProxyCacheServer.Builder(this)
                .cacheDirectory(Utils.getVideoCacheDir(this))
                .build();
    }

    protected void initSingletons() {
        // Initialize the instance of MySingleton
        Singleton.initInstance();
    }

    public static RestClient getRestClient() {
        restClient = new RestClient();
        return restClient;
    }

    public static void setFragment(Fragment fragment, FragmentManager fragmentManager, int container) {
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(container, fragment);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

    public static String getCurrentDate() {
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
        String formattedDate = df.format(c.getTime());
        return formattedDate;
    }

    public static String getConvertDateForUpdate(String dateStr) {
        DateFormat fromFormat = new SimpleDateFormat("dd-MM-yyyy");
        fromFormat.setLenient(false);
        DateFormat toFormat = new SimpleDateFormat("dd/MM/yyyy");
        toFormat.setLenient(false);
        Date date = null;
        try {
            date = fromFormat.parse(dateStr);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        String strDate = toFormat.format(date);
        return strDate;
    }

    private void initUniversalImageLoader() {
        ImageLoaderConfiguration.Builder config = new ImageLoaderConfiguration.Builder(getApplicationContext());
        config.threadPriority(Thread.NORM_PRIORITY - 2);
        config.denyCacheImageMultipleSizesInMemory();
        config.diskCacheFileNameGenerator(new Md5FileNameGenerator());
        config.diskCacheSize(50 * 1024 * 1024); // 50 MiB
        config.tasksProcessingOrder(QueueProcessingType.LIFO);
        config.writeDebugLogs(); // Remove for release app
        config.defaultDisplayImageOptions(getDefaultDisplayImageOption());
        // Initialize ImageLoader with configuration.
        ImageLoader.getInstance().init(config.build());
    }

    public static DisplayImageOptions getDefaultDisplayImageOption() {
        DisplayImageOptions options = new DisplayImageOptions.Builder()
                .showImageOnLoading(new ColorDrawable(Color.parseColor("#f0f0f0")))
                .resetViewBeforeLoading(true)
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .considerExifParams(true)
                .imageScaleType(ImageScaleType.EXACTLY_STRETCHED)
                .bitmapConfig(Bitmap.Config.RGB_565)
                .displayer(new FadeInBitmapDisplayer(500))
//                .delayBeforeLoading(300)
                .build();
        return options;
    }

    /*
     * Preparing the list data
     */

}
