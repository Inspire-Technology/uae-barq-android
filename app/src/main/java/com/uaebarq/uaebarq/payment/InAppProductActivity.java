package com.uaebarq.uaebarq.payment;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;

import butterknife.BindView;
import butterknife.OnClick;

import com.github.silvestrpredko.dotprogressbar.DotProgressBar;
import com.uaebarq.uaebarq.ApplicationData;
import com.uaebarq.uaebarq.BaseActivity;
import com.uaebarq.uaebarq.R;
import com.uaebarq.uaebarq.models.ResponseBase;
import com.uaebarq.uaebarq.payment.inapputils.IabHelper;
import com.uaebarq.uaebarq.payment.inapputils.IabResult;
import com.uaebarq.uaebarq.payment.inapputils.Inventory;
import com.uaebarq.uaebarq.payment.inapputils.Purchase;
import com.uaebarq.uaebarq.retrofit.NetworkInterface.NetworkPostServices;
import com.uaebarq.uaebarq.singleton.Singleton;
import com.uaebarq.uaebarq.utils.Constant;
import com.uaebarq.uaebarq.utils.FireBaseEvents;
import com.uaebarq.uaebarq.utils.UaeBarqUtils;
import com.uaebarq.uaebarq.utils.Utils;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class InAppProductActivity extends BaseActivity {

    static final String TAG = "UAEBARQ";
    @BindView(R.id.dot_progress_bar)
    DotProgressBar dotProgressBar;
    @BindView(R.id.ll_congratx)
    View llCongrats;
    @BindView(R.id.progress_bar)
    View progressBar;
    @BindView(R.id.first_view)
    LinearLayout first_view;
    IabHelper mHelper;
    static final int RC_REQUEST = 10001;
    static String SKU_WEEKLY;
    static String SKU_MONTHLY;

    String resetToken = "";

    @SuppressLint("NewApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        dotProgressBar.setEndColor(ContextCompat.getColor(this, R.color.red_coupn));
        dotProgressBar.setStartColor(ContextCompat.getColor(this, R.color.red_coupn));
        resetToken = Singleton.getInstance().fetchSettingsModel.getPayload().getPurchaseToken().toString();
        SKU_WEEKLY = Singleton.getInstance().fetchSettingsModel.getPayload().getWeeklyId();
        SKU_MONTHLY = Singleton.getInstance().fetchSettingsModel.getPayload().getMonthlyId();
        mHelper = new IabHelper(this, Constant.base64EncodedPublicKey);
        mHelper.startSetup(new IabHelper.OnIabSetupFinishedListener() {
            public void onIabSetupFinished(IabResult result) {
                if (!result.isSuccess()) {
                    Log.d(TAG, "In-app Billing setup failed: " + result);
                } else {
                    Log.d(TAG, "In-app Billing is set up OK");
                }
                Log.d(TAG, "Setup successful. Querying inventory.");
            }
        });
    }

    public void weeklyButtonClicked(View view) {
        try {
            Utils.trackEvent(this.getClass().getSimpleName(), FireBaseEvents.inAppWeeklyButton);
            mHelper.launchPurchaseFlow(this, SKU_WEEKLY, RC_REQUEST, mPurchaseFinishedListener, resetToken);
        } catch (IabHelper.IabAsyncInProgressException e) {
            e.printStackTrace();
        }
    }

    public void monthlyButtonClicked(View view) {
        try {
            Utils.trackEvent(this.getClass().getSimpleName(), FireBaseEvents.inAppMonthlyButton);
            mHelper.launchPurchaseFlow(this, SKU_MONTHLY, RC_REQUEST, mPurchaseFinishedListener, resetToken);
        } catch (IabHelper.IabAsyncInProgressException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (!mHelper.handleActivityResult(requestCode, resultCode, data)) {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    IabHelper.OnIabPurchaseFinishedListener mPurchaseFinishedListener = new IabHelper.OnIabPurchaseFinishedListener() {
        public void onIabPurchaseFinished(IabResult result, Purchase purchase) {
            if (result.isFailure()) {
                // Handle error
                return;
            } else if (purchase.getSku().equals(SKU_MONTHLY)) {

                Utils.trackEvent(this.getClass().getSimpleName(), FireBaseEvents.inAppMonthlyPurchase);
                consumeItem();
            } else if (purchase.getSku().equals(SKU_WEEKLY)) {
                Utils.trackEvent(this.getClass().getSimpleName(), FireBaseEvents.inAppWeeklyPurchase);
                consumeItem();
                // buyButton.setEnabled(false);
            }

        }
    };

    public void consumeItem() {
        try {
            mHelper.queryInventoryAsync(mReceivedInventoryListener);
        } catch (IabHelper.IabAsyncInProgressException e) {
            e.printStackTrace();
            Log.d(TAG, "Setup successful. Querying inventory.");
        }
    }

    IabHelper.QueryInventoryFinishedListener mReceivedInventoryListener = new IabHelper.QueryInventoryFinishedListener() {
        public void onQueryInventoryFinished(IabResult result, Inventory inventory) {
            if (result.isFailure()) {
                // Handle failure
            } else {
                try {
                    Purchase monthlyPurchase = inventory.getPurchase(SKU_MONTHLY);
                    Purchase weeklyPurchase = inventory.getPurchase(SKU_WEEKLY);
                    if (monthlyPurchase != null && monthlyPurchase.isAutoRenewing()) {
                        Utils.trackEvent(this.getClass().getSimpleName(), FireBaseEvents.inAppAutoRenewmonthly);
                        mHelper.consumeAsync(monthlyPurchase, mConsumeFinishedListener);
                    } else if (weeklyPurchase != null && weeklyPurchase.isAutoRenewing()) {
                        Utils.trackEvent(this.getClass().getSimpleName(), FireBaseEvents.inAppAutoReneweekly);
                        mHelper.consumeAsync(weeklyPurchase, mConsumeFinishedListener);
                    }
                } catch (IabHelper.IabAsyncInProgressException e) {
                    e.printStackTrace();
                }
            }
        }
    };

    IabHelper.OnConsumeFinishedListener mConsumeFinishedListener = new IabHelper.OnConsumeFinishedListener() {
        public void onConsumeFinished(Purchase purchase, IabResult result) {
            if (result.isSuccess() || purchase != null) {
                progressBar.setVisibility(View.VISIBLE);
                String type;
                String signedData = purchase.getOriginalJson();
                if (purchase.getSku().equalsIgnoreCase(SKU_MONTHLY)) {
                    Utils.trackEvent(this.getClass().getSimpleName(), FireBaseEvents.inAppconsumefinishedmonthly);
                    type = "monthly";
                } else {
                    Utils.trackEvent(this.getClass().getSimpleName(), FireBaseEvents.inAppconsumefinishedwweekly);
                    type = "weekly";
                }
                String signature = purchase.getSignature();
                sendPremiumDataToServer(signedData, type, signature);
            } else {
                // handle error
            }
        }
    };

    private void sendPremiumDataToServer(String signedData, String type, String signature) {
        Call<ResponseBase> contactModel = ApplicationData.getRestClient().createService(NetworkPostServices.class).
                postGooglePlay(getUserId(), signedData, type, Constant.API_KEY, signature, Constant.DEVICE_TYPE);
        contactModel.enqueue(new Callback<ResponseBase>() {
            @Override
            public void onResponse(Call<ResponseBase> call, Response<ResponseBase> response) {
                try {
                    progressBar.setVisibility(View.GONE);
                    if (response.body() != null && response.body().getSuccess().equalsIgnoreCase("true")) {
                        UaeBarqUtils.getAppUtils(getApplicationContext()).showToast(getApplicationContext(), response.body().getMessage().toString());
                        setPremiumUser(true);
                        llCongrats.setVisibility(View.VISIBLE);
                        first_view.setVisibility(View.GONE);
                    } else {
                        UaeBarqUtils.getAppUtils(getApplicationContext()).showToast(getApplicationContext(), response.body().getMessage().toString());
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    progressBar.setVisibility(View.GONE);
                }
            }

            @Override
            public void onFailure(Call<ResponseBase> call, Throwable t) {
                progressBar.setVisibility(View.GONE);
            }
        });
    }

    @OnClick(R.id.ll_ok)
    void okClick() {
        fetchAppSettings();
        setResult(RESULT_OK);
        finish();
    }

    @OnClick(R.id.iv_cross_image)
    void backClick() {
        finish();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mHelper != null) try {
            mHelper.dispose();
        } catch (IabHelper.IabAsyncInProgressException e) {
            e.printStackTrace();
        }
        mHelper = null;
    }

    @Override
    protected int setLayoutId() {
        return R.layout.activity_sub_code;
    }
}
