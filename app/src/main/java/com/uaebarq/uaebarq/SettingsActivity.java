package com.uaebarq.uaebarq;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatDelegate;
import android.support.v7.widget.SwitchCompat;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.github.silvestrpredko.dotprogressbar.DotProgressBar;
import com.uaebarq.uaebarq.adapter.CategoryWithChildAdapter;
import com.uaebarq.uaebarq.models.GetSettingsModel.CategoriesWithChild;
import com.uaebarq.uaebarq.models.GetSettingsModel.CategoriesWithoutChild;
import com.uaebarq.uaebarq.models.GetSettingsModel.GetSettingsModel;
import com.uaebarq.uaebarq.models.ResponseBase;
import com.uaebarq.uaebarq.retrofit.NetworkInterface.NetworkPostServices;
import com.uaebarq.uaebarq.singleton.Singleton;
import com.uaebarq.uaebarq.utils.Constant;
import com.uaebarq.uaebarq.utils.FireBaseEvents;
import com.uaebarq.uaebarq.utils.UaeBarqUtils;
import com.uaebarq.uaebarq.utils.Utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class SettingsActivity extends BaseActivity {


    @BindView(R.id.topbar)
    Toolbar toolbar;

    @BindView(R.id.iv_logo)
    ImageView ivLogo;

    @BindView(R.id.tv_name)
    TextView tvName;
    private Call<GetSettingsModel> getSettings;

    @BindView(R.id.dot_progress_bar)
    DotProgressBar dotProgressBar;

    @BindView(R.id.progress_layout)
    View progressLayout;

    @BindView(R.id.rl_top)
    View rlTop;

    @BindView(R.id.empty_view)
    View emptyView;

    @BindView(R.id.no_wifi_view)
    View noWifiView;
    private View convertView;
    private LayoutInflater layoutInflater;

    @BindView(R.id.ll_main)
    LinearLayout llMain;

    @BindView(R.id.rl_main_layout)
    RelativeLayout rlMainLayout;

    @BindView(R.id.tv_notification)
    TextView tvNotification;
    @BindView(R.id.tv_simple)
    TextView tv_simple;

    @BindView(R.id.listView)
    ExpandableListView expandableListView;

    private CategoryWithChildAdapter listAdapter;

    @BindView(R.id.switch_main)
    SwitchCompat switchMain;
    private Call<ResponseBase> saveSettings;
    ArrayList<Integer> parentArrayList = new ArrayList<>();
    private ArrayList<CategoriesWithoutChild> categoriesWithoutChildList;
    private GetSettingsModel getSettingsModel;
    int isNotification = 0;
    private boolean specialCase;
    private boolean latestNews=false,fromUAE=false,weather=false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
        Singleton.getInstance().childArrayList.clear();
        layoutInflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
        dotProgressBar.setStartColor(ContextCompat.getColor(this, R.color.tab_gray));
        dotProgressBar.setEndColor(ContextCompat.getColor(this, R.color.tab_gray));
        ivLogo.setVisibility(View.GONE);
        tvName.setVisibility(View.VISIBLE);
        tvName.setText(getResources().getString(R.string.settings));
        if (!isNullOrEmpty(getUserId())) {
            if (isOnline()) {
                getSettings();
            } else {
                UaeBarqUtils.getAppUtils(this).showToast(this, getResources().getString(R.string.please_check_internet));
            }
        } else {
            if (isOnline()) {
                fetchAppSettings();
            } else {
                UaeBarqUtils.getAppUtils(this).showToast(this, getResources().getString(R.string.please_check_internet));
            }
        }
        if (isEnglishLanguage()) {
            UaeBarqUtils.FontUtils.applyFonts(this, Constant.strTypefaceEnglishMedium, rlMainLayout);
            UaeBarqUtils.FontUtils.applyFonts(this, Constant.strTypefaceEnglishRegular, tv_simple);
        } else {
            UaeBarqUtils.FontUtils.applyFonts(this, Constant.strTypefaceArabicMedium, rlMainLayout);
            UaeBarqUtils.FontUtils.applyFonts(this, Constant.strTypefaceArabicRegular, tv_simple);
        }
    }

    private void getSettings() {
        getSettings = ApplicationData.getInstance().getRestClient().createService(NetworkPostServices.class).
                getSettings(Constant.API_KEY,
                        getUserId(),
                        getPakageName(),
                        UaeBarqUtils.getAppUtils(this).getCurrentLang(this));
        getSettings.enqueue(new Callback<GetSettingsModel>() {
            @Override
            public void onResponse(Call<GetSettingsModel> call, Response<GetSettingsModel> response) {
                try {
                    progressLayout.setVisibility(View.GONE);
                    rlTop.setVisibility(View.VISIBLE);
                    if (response.body() != null && response.body().getSuccess().equalsIgnoreCase("true")) {
                        getSettingsModel = response.body();
                        setSettingsLayout(getSettingsModel);
                        setCategoryWithChildListView(getSettingsModel.getPayload().getCategoriesWithChilds());
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    progressLayout.setVisibility(View.GONE);
                    emptyView.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onFailure(Call<GetSettingsModel> call, Throwable t) {
                progressLayout.setVisibility(View.GONE);
                emptyView.setVisibility(View.VISIBLE);
            }
        });
    }

    boolean isTrue = true;

    private void setSettingsLayout(final GetSettingsModel getSettingsModel) {
        if (getSettingsModel.getPayload().isIs_notification()) {
            switchMain.setChecked(true);
            isNotification = 1;
            isTrue = true;
        } else {
            switchMain.setChecked(false);
            isNotification = 0;
            isTrue = false;
        }

        switchMain.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {

                if (isChecked) {
                    switchMain.setChecked(true);
                    isNotification = 1;
                    specialCase = true;
                } else {
                    switchMain.setChecked(false);
                    isNotification = 0;
                    specialCase = false;
                }

                Utils.trackEvent(this.getClass().getSimpleName(), FireBaseEvents.settingScreenNotification+"_"+specialCase);
                isTrue = isChecked;
                setCategoryWithoutChild(getSettingsModel, isTrue);
            }
        });
        setCategoryWithoutChild(getSettingsModel, isTrue);
    }

    private void setCategoryWithoutChild(final GetSettingsModel getSettingsModel, final boolean isCheckedd) {
        categoriesWithoutChildList = getSettingsModel.getPayload().getCategoriesWithoutChilds();
        llMain.removeAllViews();
        parentArrayList.clear();
        if (categoriesWithoutChildList != null) {
            for (int i = 0; i < categoriesWithoutChildList.size(); i++) {
                final CategoriesWithoutChild categoriesWithoutChild = categoriesWithoutChildList.get(i);
                convertView = layoutInflater.inflate(R.layout.without_child_item, null);
                TextView tvParent = convertView.findViewById(R.id.tv_child);
                tvParent.setText(categoriesWithoutChild.getTitle());
                final SwitchCompat switchCompat = convertView.findViewById(R.id.switch_compat);
                switchCompat.setChecked(categoriesWithoutChild.isUserSelected());
                if (isCheckedd && switchCompat.isChecked()) {
                    switchCompat.setChecked(true);
                    parentArrayList.add(categoriesWithoutChild.getId());
                } else if (specialCase) {
                    switchCompat.setChecked(true);
                    parentArrayList.add(categoriesWithoutChild.getId());
                } else {
                    switchCompat.setChecked(false);
                }
                switchCompat.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
//                        if (getSettingsModel.getPayload().getIs_notification() == 1) {
//                            switchCompat.setChecked(false);
//                        } else {
//                            switchCompat.setChecked(isChecked);
//                        }

                        if (isChecked) {
                            if (isCheckedd ) {
                                switchCompat.setChecked(true);
                                parentArrayList.add(categoriesWithoutChild.getId());
                            } else if (specialCase) {
                                switchCompat.setChecked(true);
                                parentArrayList.add(categoriesWithoutChild.getId());
                            }else {
                                switchCompat.setChecked(false);
                            }
                           /* switchCompat.setChecked(true);
                            parentArrayList.add(categoriesWithoutChild.getId());*/
                        } else {
                            switchCompat.setChecked(false);
                            parentArrayList.remove(categoriesWithoutChild.getId());
                        }



                        int size=parentArrayList.size();
                        if(size==0)
                        {
                            switchMain.setChecked(false);
                        }
                        else
                        {
                            switchMain.setChecked(true);
                        }
                    }
                });
                if (isEnglishLanguage()) {
                    UaeBarqUtils.FontUtils.applyFonts(this, Constant.strTypefaceEnglishRegular, tvParent);
                } else {
                    UaeBarqUtils.FontUtils.applyFonts(this, Constant.strTypefaceArabicRegular, tvParent);
                }
                llMain.addView(convertView);
            }
        }
    }

    private void setCategoryWithChildListView(ArrayList<CategoriesWithChild> categoriesWithChildren) {
        listAdapter = new CategoryWithChildAdapter(this, categoriesWithChildren);
        expandableListView.setAdapter(listAdapter);
        //listAdapter.notifyDataSetChanged();
        for (int i = 0; i < listAdapter.getGroupCount(); i++) {
            expandableListView.expandGroup(i);
        }
        expandableListView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
            @Override
            public boolean onGroupClick(ExpandableListView expandableListView, View view, int i, long l) {
                expandableListView.expandGroup(i);
                return true;
            }
        });

    }

    @OnClick(R.id.btn_save)
    void saveOnClick() {
        if (isOnline()) {
            Utils.trackEvent(this.getClass().getSimpleName(), FireBaseEvents.settingScreenSaveSettings);
            saveSettings();
        } else {
            UaeBarqUtils.getAppUtils(this).showToast(this, getResources().getString(R.string.please_check_internet));
        }
    }

    @OnClick(R.id.btn_contact_us)
    void contactusActivity() {
        if (isOnline()) {
            //Utils.trackEvent(this.getClass().getSimpleName(), FireBaseEvents.con);
            //saveSettings();
            openContactus();
        } else {
            UaeBarqUtils.getAppUtils(this).showToast(this, getResources().getString(R.string.please_check_internet));
        }
    }

    private void saveSettings() {
        progressLayout.setBackgroundColor(ContextCompat.getColor(this, android.R.color.transparent));
        progressLayout.setVisibility(View.VISIBLE);
        Map<String, String> data = new HashMap<>();
        data.put("api_key", Constant.API_KEY);
        data.put("user_id", getUserId());
        data.put("app_id", getPakageName());
        data.put("device_type", Constant.DEVICE_TYPE);
        data.put("is_notification", String.valueOf(isNotification));
        for (int i = 0; i < Singleton.getInstance().childArrayList.size(); i++) {
            Integer list = Singleton.getInstance().childArrayList.get(i);
            data.put("Child[" + i + "]", list.toString());
        }
        if(parentArrayList.size()!=0){
            for (int i = 0; i < parentArrayList.size(); i++) {
                Integer list = parentArrayList.get(i);
                data.put("Parent[" + i + "]", list.toString());
            }
        }else{
            data.put("Parent[" + 0 + "]", "");

        }

        saveSettings = ApplicationData.getInstance().getRestClient().createService(NetworkPostServices.class).saveSettings(data);
        saveSettings.enqueue(new Callback<ResponseBase>() {
            @Override
            public void onResponse(Call<ResponseBase> call, Response<ResponseBase> response) {
                try {
                    progressLayout.setVisibility(View.GONE);
                    rlTop.setVisibility(View.VISIBLE);
                    if (response.body() != null && response.body().getSuccess().equalsIgnoreCase("true")) {
                        ResponseBase responseBase = response.body();
                        UaeBarqUtils.getAppUtils(getApplicationContext()).showToast(getApplicationContext(), responseBase.getMessage());
                        Utils.trackEvent(this.getClass().getSimpleName(), FireBaseEvents.settingScreenOpenHomeActivity);
                        Intent i = new Intent(SettingsActivity.this, HomeActivity.class);
                        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        finish();
                        overridePendingTransition(0, 0);
                        startActivity(i);
                        overridePendingTransition(0, 0);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    progressLayout.setVisibility(View.GONE);
                    emptyView.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onFailure(Call<ResponseBase> call, Throwable t) {
                progressLayout.setVisibility(View.GONE);
                emptyView.setVisibility(View.VISIBLE);
            }
        });
    }

    @Override
    protected int setLayoutId() {
        return R.layout.activity_settings;
    }

    @OnClick(R.id.iv_back)
    void onBackClick() {
        finish();
    }

    void openContactus()
    {
        Intent intent=new Intent(getApplicationContext(),ContactUsActivity.class);
        startActivity(intent);
        finish();
    }
}
