package com.uaebarq.uaebarq.singleton;


import com.uaebarq.uaebarq.models.DashBoardModel.Category;
import com.uaebarq.uaebarq.models.FetchSettingsModel.FetchSettingsModel;

import java.util.ArrayList;

public class Singleton {

    private static Singleton instance;
    public String arabic = "ar";
    public String english = "en";
    public int limit = 10;
    public int offSet = 0;
    public ArrayList<Category> tabCategoriesList = new ArrayList<>();
    public String bannerImage;
    public String bannerText;
    public FetchSettingsModel fetchSettingsModel = new FetchSettingsModel();
    public ArrayList<Integer> childArrayList = new ArrayList<>();


    public static void initInstance() {
        if (instance == null) {
            // Create the instance
            instance = new Singleton();
        }
    }

    public static Singleton getInstance() {
        // Return the instance
        return instance;
    }

    private Singleton() {
        // Constructor hidden because this is a singleton
    }
}
