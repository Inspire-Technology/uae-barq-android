
package com.uaebarq.uaebarq.models.GetSettingsModel;

import java.io.Serializable;
import java.util.ArrayList;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.uaebarq.uaebarq.R;

public class CategoriesWithChild implements Serializable {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("sub_categoryies")
    @Expose
    private ArrayList<SubCategoryie> subCategoryies = null;

    private Integer[] categoryEngImages = {
            R.drawable.health_color_bar,
            R.drawable.prayer_time_color_bar,};

    private Integer[] categoryArabicImages = {
            R.drawable.health_color_bar,
            R.drawable.city_new_bar_color,
            R.drawable.prayer_time_color_bar,
            R.drawable.islamic_services_bar_color,};

    /**
     * No args constructor for use in serialization
     */
    public CategoriesWithChild() {
    }

    /**
     * @param id
     * @param subCategoryies
     * @param title
     */
    public CategoriesWithChild(Integer id, String title, ArrayList<SubCategoryie> subCategoryies) {
        super();
        this.id = id;
        this.title = title;
        this.subCategoryies = subCategoryies;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public ArrayList<SubCategoryie> getSubCategoryies() {
        return subCategoryies;
    }

    public void setSubCategoryies(ArrayList<SubCategoryie> subCategoryies) {
        this.subCategoryies = subCategoryies;
    }

    public Integer[] getCategoryEngImages() {
        return categoryEngImages;
    }

    public void setCategoryEngImages(Integer[] categoryEngImages) {
        this.categoryEngImages = categoryEngImages;
    }

    public Integer[] getCategoryArabicImages() {
        return categoryArabicImages;
    }

    public void setCategoryArabicImages(Integer[] categoryArabicImages) {
        this.categoryArabicImages = categoryArabicImages;
    }
}
