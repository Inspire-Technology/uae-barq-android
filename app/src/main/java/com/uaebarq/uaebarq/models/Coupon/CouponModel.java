
package com.uaebarq.uaebarq.models.Coupon;

import com.uaebarq.uaebarq.models.ResponseBase;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class CouponModel extends ResponseBase {

    @SerializedName("Payload")
    @Expose
    private Payload Payload;

    public Payload getPayload() {
        return Payload;
    }

    public void setPayload(Payload payload) {
        Payload = payload;
    }
}
