
package com.uaebarq.uaebarq.models.GetSettingsModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.uaebarq.uaebarq.models.ResponseBase;

public class GetSettingsModel extends ResponseBase {

    @SerializedName("Payload")
    @Expose
    private Payload payload;
    /**
     * No args constructor for use in serialization
     */
    public GetSettingsModel() {
    }

    /**
     * @param message
     * @param payload
     * @param success
     */
    public GetSettingsModel(String success, Payload payload, String message) {
        super();
        this.payload = payload;
    }

    public void setPayload(Payload payload) {
        this.payload = payload;
    }

    public Payload getPayload() {
        return payload;
    }
}
