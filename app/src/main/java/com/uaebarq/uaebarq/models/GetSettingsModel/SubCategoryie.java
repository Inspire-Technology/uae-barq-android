
package com.uaebarq.uaebarq.models.GetSettingsModel;

import java.io.Serializable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SubCategoryie implements Serializable {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("is_user_selected")
    @Expose
    private boolean isUserSelected;

    /**
     * No args constructor for use in serialization
     */
    public SubCategoryie() {
    }

    /**
     * @param id
     * @param isUserSelected
     * @param title
     */
    public SubCategoryie(Integer id, String title,boolean isUserSelected) {
        super();
        this.id = id;
        this.title = title;
        this.isUserSelected = isUserSelected;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public boolean isUserSelected() {
        return isUserSelected;
    }

    public void setUserSelected(boolean userSelected) {
        isUserSelected = userSelected;
    }
}
