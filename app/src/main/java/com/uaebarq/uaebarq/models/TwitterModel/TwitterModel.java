
package com.uaebarq.uaebarq.models.TwitterModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.uaebarq.uaebarq.models.ResponseBase;

public class TwitterModel extends ResponseBase {

    @SerializedName("Payload")
    @Expose
    private Payload payload;

    /**
     * No args constructor for use in serialization
     */
    public TwitterModel() {
    }

    /**
     * @param message
     * @param payload
     * @param success
     */
    public TwitterModel(String success, Payload payload, String message) {
        super();
        this.payload = payload;
    }

    public Payload getPayload() {
        return payload;
    }

    public void setPayload(Payload payload) {
        this.payload = payload;
    }
}
