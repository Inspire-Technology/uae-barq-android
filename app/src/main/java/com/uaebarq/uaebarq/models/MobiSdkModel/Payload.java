package com.uaebarq.uaebarq.models.MobiSdkModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by naweed on 3/13/18.
 */

public class Payload implements Serializable{

    @SerializedName("sdk_setting")
    @Expose
    private boolean sdk_setting;


    public boolean isSdk_setting() {
        return sdk_setting;
    }

    public void setSdk_setting(boolean sdk_setting) {
        this.sdk_setting = sdk_setting;
    }
}
