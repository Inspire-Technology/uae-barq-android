
package com.uaebarq.uaebarq.models.FetchSettingsModel;

import java.io.Serializable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Payload implements Serializable {

    @SerializedName("user_id")
    @Expose
    private Integer userId;
    @SerializedName("is_notification")
    @Expose
    private boolean isNotification;
    @SerializedName("is_premium")
    @Expose
    private boolean isPremium;
    @SerializedName("apple")
    @Expose
    private boolean apple;
    @SerializedName("social_media")
    @Expose
    private boolean socialMedia;
    @SerializedName("device_type")
    @Expose
    private String deviceType;
    @SerializedName("app_id")
    @Expose
    private String appId;
    @SerializedName("monthly_id")
    @Expose
    private String monthlyId;
    @SerializedName("weekly_id")
    @Expose
    private String weeklyId;
    @SerializedName("android_sdk")
    @Expose
    private boolean androidSdk;
    @SerializedName("purchase_token")
    @Expose
    private String purchaseToken;

    @SerializedName("phone_num")
    @Expose
    private String phone_num;


    public Payload() {
    }

    public Payload(Integer userId, boolean isNotification, boolean isPremium, boolean apple, boolean socialMedia, String deviceType, String appId, String monthlyId, String weeklyId, boolean android_sdk, String purchase_token,String phone_num) {
        this.userId = userId;
        this.isNotification = isNotification;
        this.isPremium = isPremium;
        this.apple = apple;
        this.socialMedia = socialMedia;
        this.deviceType = deviceType;
        this.appId = appId;
        this.monthlyId = monthlyId;
        this.weeklyId = weeklyId;
        this.androidSdk = android_sdk;
        this.purchaseToken = purchase_token;
        this.phone_num=phone_num;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public boolean isNotification() {
        return isNotification;
    }

    public void setNotification(boolean notification) {
        isNotification = notification;
    }

    public boolean isPremium() {
        return isPremium;
    }

    public void setPremium(boolean premium) {
        isPremium = premium;
    }

    public boolean isApple() {
        return apple;
    }

    public void setApple(boolean apple) {
        this.apple = apple;
    }

    public boolean isSocialMedia() {
        return socialMedia;
    }

    public void setSocialMedia(boolean socialMedia) {
        this.socialMedia = socialMedia;
    }

    public String getDeviceType() {
        return deviceType;
    }

    public void setDeviceType(String deviceType) {
        this.deviceType = deviceType;
    }

    public String getAppId() {
        return appId;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }

    public String getMonthlyId() {
        return monthlyId;
    }

    public void setMonthlyId(String monthlyId) {
        this.monthlyId = monthlyId;
    }

    public String getWeeklyId() {
        return weeklyId;
    }

    public void setWeeklyId(String weeklyId) {
        this.weeklyId = weeklyId;
    }

    public boolean isAndroidSdk() {
        return androidSdk;
    }

    public void setAndroidSdk(boolean androidSdk) {
        this.androidSdk = androidSdk;
    }

    public String getPurchaseToken() {
        return purchaseToken;
    }

    public void setPurchaseToken(String purchaseToken) {
        this.purchaseToken = purchaseToken;
    }

    public String getPhone_num() {
        return phone_num;
    }

    public void setPhone_num(String phone_num) {
        this.phone_num = phone_num;
    }
}
