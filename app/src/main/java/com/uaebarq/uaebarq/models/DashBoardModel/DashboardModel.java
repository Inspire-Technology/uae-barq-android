
package com.uaebarq.uaebarq.models.DashBoardModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.uaebarq.uaebarq.models.ResponseBase;

public class DashboardModel extends ResponseBase {

    @SerializedName("Payload")
    @Expose
    private Payload payload;

    /**
     * No args constructor for use in serialization
     */
    public DashboardModel() {
    }

    /**
     * @param message
     * @param payload
     * @param success
     */
    public DashboardModel(String success, Payload payload, String message) {
        super();
        this.payload = payload;
    }

    public Payload getPayload() {
        return payload;
    }

    public void setPayload(Payload payload) {
        this.payload = payload;
    }

}
