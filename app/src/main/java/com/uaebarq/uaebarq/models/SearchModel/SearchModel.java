
package com.uaebarq.uaebarq.models.SearchModel;

import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.uaebarq.uaebarq.models.ResponseBase;

public class SearchModel extends ResponseBase {

    @SerializedName("Payload")
    @Expose
    private List<Payload> payload = null;

    /**
     * No args constructor for use in serialization
     */
    public SearchModel() {
    }

    /**
     * @param message
     * @param payload
     * @param success
     */
    public SearchModel(String success, List<Payload> payload, String message) {
        super();
        this.payload = payload;
    }

    public List<Payload> getPayload() {
        return payload;
    }

    public void setPayload(List<Payload> payload) {
        this.payload = payload;
    }

}
