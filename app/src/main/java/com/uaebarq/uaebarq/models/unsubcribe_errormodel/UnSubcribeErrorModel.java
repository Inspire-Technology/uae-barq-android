
package com.uaebarq.uaebarq.models.unsubcribe_errormodel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UnSubcribeErrorModel {




    @SerializedName("sucess")
    @Expose
    private boolean sucess;

    @SerializedName("error_code")
    @Expose
    private Integer errorCode;
    @SerializedName("error_msg")
    @Expose
    private String errorMsg;

    public Integer getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(Integer errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }

    public boolean isSucess() {
        return sucess;
    }

    public void setSucess(boolean sucess) {
        this.sucess = sucess;
    }
}
