
package com.uaebarq.uaebarq.models.DashBoardModel;

import java.io.Serializable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CategoryPost implements Serializable {

    @SerializedName("video")
    @Expose
    private String video;
    @SerializedName("image")
    @Expose
    private String image;
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("descriptions")
    @Expose
    private String descriptions;
    @SerializedName("is_free")
    @Expose
    private boolean isFree;
    @SerializedName("time_ago")
    @Expose
    private String timeAgo;
    @SerializedName("created")
    @Expose
    private String created;
    @SerializedName("category_title")
    @Expose
    private String categoryTitle;

    /**
     * No args constructor for use in serialization
     */
    public CategoryPost() {
    }

    /**
     * @param id
     * @param timeAgo
     * @param title
     * @param categoryTitle
     * @param created
     * @param isFree
     * @param image
     * @param type
     * @param descriptions
     * @param video
     */
    public CategoryPost(String video, String image, Integer id, String title, String type, String descriptions, boolean isFree, String timeAgo, String created, String categoryTitle) {
        super();
        this.video = video;
        this.image = image;
        this.id = id;
        this.title = title;
        this.type = type;
        this.descriptions = descriptions;
        this.isFree = isFree;
        this.timeAgo = timeAgo;
        this.created = created;
        this.categoryTitle = categoryTitle;
    }

    public String getVideo() {
        return video;
    }

    public void setVideo(String video) {
        this.video = video;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDescriptions() {
        return descriptions;
    }

    public void setDescriptions(String descriptions) {
        this.descriptions = descriptions;
    }

    public String getTimeAgo() {
        return timeAgo;
    }

    public void setTimeAgo(String timeAgo) {
        this.timeAgo = timeAgo;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public String getCategoryTitle() {
        return categoryTitle;
    }

    public void setCategoryTitle(String categoryTitle) {
        this.categoryTitle = categoryTitle;
    }

    public boolean isFree() {
        return isFree;
    }

    public void setFree(boolean free) {
        isFree = free;
    }
}
