
package com.uaebarq.uaebarq.models.FetchSettingsModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.uaebarq.uaebarq.models.ResponseBase;

public class FetchSettingsModel extends ResponseBase {

    @SerializedName("Payload")
    @Expose
    private Payload payload;


    public Payload getPayload() {
        return payload;
    }

    public void setPayload(Payload payload) {
        this.payload = payload;
    }
}
