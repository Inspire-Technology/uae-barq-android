
package com.uaebarq.uaebarq.models.InstagramModel;

import java.util.List;

import com.uaebarq.uaebarq.models.ResponseBase;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class InstagramModel extends ResponseBase{

    @SerializedName("Payload")
    @Expose
    private List<String> payload = null;

    /**
     * No args constructor for use in serialization
     * 
     */
    public InstagramModel() {
    }

    /**
     * 
     * @param message
     * @param payload
     * @param success
     */
    public InstagramModel(String success, List<String> payload, String message) {
        super();
        this.payload = payload;
    }

    public List<String> getPayload() {
        return payload;
    }

    public void setPayload(List<String> payload) {
        this.payload = payload;
    }
}
