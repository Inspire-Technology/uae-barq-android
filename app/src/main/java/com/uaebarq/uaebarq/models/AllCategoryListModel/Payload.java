
package com.uaebarq.uaebarq.models.AllCategoryListModel;

import java.io.Serializable;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Payload implements Serializable
{

    @SerializedName("Posts")
    @Expose
    private List<Post> posts = null;
    @SerializedName("Banner")
    @Expose
    private List<Banner> banner = null;

    /**
     * No args constructor for use in serialization
     * 
     */
    public Payload() {
    }

    /**
     * 
     * @param posts
     * @param banner
     */
    public Payload(List<Post> posts, List<Banner> banner) {
        super();
        this.posts = posts;
        this.banner = banner;
    }

    public List<Post> getPosts() {
        return posts;
    }

    public void setPosts(List<Post> posts) {
        this.posts = posts;
    }

    public List<Banner> getBanner() {
        return banner;
    }

    public void setBanner(List<Banner> banner) {
        this.banner = banner;
    }

}
