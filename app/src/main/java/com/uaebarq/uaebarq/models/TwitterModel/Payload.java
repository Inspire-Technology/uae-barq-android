
package com.uaebarq.uaebarq.models.TwitterModel;

import java.io.Serializable;
import java.util.ArrayList;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Payload implements Serializable {

    @SerializedName("Tweets")
    @Expose
    private String tweets;
    @SerializedName("Following")
    @Expose
    private String following;
    @SerializedName("Follower")
    @Expose
    private String follower;
    @SerializedName("Feeds")
    @Expose
    private ArrayList<Feed> feeds = null;

    /**
     * No args constructor for use in serialization
     */
    public Payload() {
    }

    /**
     * @param feeds
     * @param following
     * @param follower
     * @param tweets
     */
    public Payload(String tweets, String following, String follower, ArrayList<Feed> feeds) {
        super();
        this.tweets = tweets;
        this.following = following;
        this.follower = follower;
        this.feeds = feeds;
    }


    public String getTweets() {
        return tweets;
    }

    public void setTweets(String tweets) {
        this.tweets = tweets;
    }

    public String getFollowing() {
        return following;
    }

    public void setFollowing(String following) {
        this.following = following;
    }

    public String getFollower() {
        return follower;
    }

    public void setFollower(String follower) {
        this.follower = follower;
    }

    public ArrayList<Feed> getFeeds() {
        return feeds;
    }

    public void setFeeds(ArrayList<Feed> feeds) {
        this.feeds = feeds;
    }
}
