
package com.uaebarq.uaebarq.models.AddPremiumModel;

import java.io.Serializable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Payload implements Serializable {

    @SerializedName("Setting")
    @Expose
    private Setting setting;
    @SerializedName("user_id")
    @Expose
    private String user_id;

    /**
     * No args constructor for use in serialization
     */
    public Payload() {
    }

    /**
     * @param setting
     */
    public Payload(Setting setting) {
        super();
        this.setting = setting;
    }

    public Setting getSetting() {
        return setting;
    }

    public void setSetting(Setting setting) {
        this.setting = setting;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }
}
