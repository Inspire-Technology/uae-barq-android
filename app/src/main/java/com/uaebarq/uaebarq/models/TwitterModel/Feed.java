
package com.uaebarq.uaebarq.models.TwitterModel;

import java.io.Serializable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Feed implements Serializable {


    @SerializedName("screen_name")
    @Expose
    private String screenName;
    @SerializedName("profile_image")
    @Expose
    private String profileImage;
    @SerializedName("descriptions")
    @Expose
    private String descriptions;
    @SerializedName("created")
    @Expose
    private String created;
    @SerializedName("post_image")
    @Expose
    private String postImage;

    /**
     * No args constructor for use in serialization
     */
    public Feed() {
    }

    /**
     * @param descriptions
     * @param postImage
     * @param created
     */
    public Feed(String screenName, String profileImage, String descriptions, String created, String postImage) {
        super();
        this.screenName = screenName;
        this.profileImage = profileImage;
        this.descriptions = descriptions;
        this.created = created;
        this.postImage = postImage;
    }

    public String getScreenName() {
        return screenName;
    }

    public void setScreenName(String screenName) {
        this.screenName = screenName;
    }

    public String getProfileImage() {
        return profileImage;
    }

    public void setProfileImage(String profileImage) {
        this.profileImage = profileImage;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public String getPostImage() {
        return postImage;
    }

    public void setPostImage(String postImage) {
        this.postImage = postImage;
    }

    public String getDescriptions() {
        return descriptions;
    }

    public void setDescriptions(String descriptions) {
        this.descriptions = descriptions;
    }
}
