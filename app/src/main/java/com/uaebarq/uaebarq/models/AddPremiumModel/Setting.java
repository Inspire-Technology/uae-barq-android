
package com.uaebarq.uaebarq.models.AddPremiumModel;

import java.io.Serializable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Setting implements Serializable {

    @SerializedName("is_notification")
    @Expose
    private boolean isNotification;
    @SerializedName("premium_user_status")
    @Expose
    private boolean premiumUserStatus;



    @SerializedName("phone_num")
    @Expose
    private String phone_num;



    /**
     * No args constructor for use in serialization
     */
    public Setting() {
    }

    /**
     * @param isNotification
     * @param premiumUserStatus
     */
    public Setting(boolean isNotification, boolean premiumUserStatus,String phone_num) {
        super();
        this.isNotification = isNotification;
        this.premiumUserStatus = premiumUserStatus;
        this.phone_num = phone_num;
    }


    public boolean isNotification() {
        return isNotification;
    }

    public void setNotification(boolean notification) {
        isNotification = notification;
    }

    public boolean isPremiumUserStatus() {
        return premiumUserStatus;
    }

    public void setPremiumUserStatus(boolean premiumUserStatus) {
        this.premiumUserStatus = premiumUserStatus;
    }

    public String getPhone_num() {
        return phone_num;
    }

    public void setPhone_num(String phone_num) {
        this.phone_num = phone_num;
    }
}
