
package com.uaebarq.uaebarq.models.SearchModel;

import java.io.Serializable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.uaebarq.uaebarq.R;

public class Payload implements Serializable {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("descriptions")
    @Expose
    private String descriptions;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("category_title")
    @Expose
    private String categoryTitle;
    @SerializedName("category_id")
    @Expose
    private Integer categoryId;
    @SerializedName("is_free")
    @Expose
    private boolean isFree;
    @SerializedName("image")
    @Expose
    private String image;
    @SerializedName("video")
    @Expose
    private String video;
    @SerializedName("time_ago")
    @Expose
    private String timeAgo;
    @SerializedName("created")
    @Expose
    private String created;

    private Integer[] categoryTagImages = {
            R.drawable.health,
            R.drawable.sports,
            R.drawable.from_uae_articles,
            R.drawable.city_news,
            R.drawable.prayer_times,
            R.drawable.islamic_services,
            R.drawable.weather,};

    /**
     * No args constructor for use in serialization
     */
    public Payload() {
    }

    /**
     * @param id
     * @param timeAgo
     * @param categoryTitle
     * @param title
     * @param created
     * @param isFree
     * @param categoryId
     * @param image
     * @param type
     * @param video
     * @param descriptions
     */
    public Payload(Integer id, String title, String descriptions, String type, String categoryTitle, Integer categoryId, boolean isFree, String image, String video, String timeAgo, String created) {
        super();
        this.id = id;
        this.title = title;
        this.descriptions = descriptions;
        this.type = type;
        this.categoryTitle = categoryTitle;
        this.categoryId = categoryId;
        this.isFree = isFree;
        this.image = image;
        this.video = video;
        this.timeAgo = timeAgo;
        this.created = created;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescriptions() {
        return descriptions;
    }

    public void setDescriptions(String descriptions) {
        this.descriptions = descriptions;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getCategoryTitle() {
        return categoryTitle;
    }

    public void setCategoryTitle(String categoryTitle) {
        this.categoryTitle = categoryTitle;
    }

    public Integer getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Integer categoryId) {
        this.categoryId = categoryId;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getVideo() {
        return video;
    }

    public void setVideo(String video) {
        this.video = video;
    }

    public String getTimeAgo() {
        return timeAgo;
    }

    public void setTimeAgo(String timeAgo) {
        this.timeAgo = timeAgo;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public Integer[] getCategoryTagImages() {
        return categoryTagImages;
    }

    public void setCategoryTagImages(Integer[] categoryTagImages) {
        this.categoryTagImages = categoryTagImages;
    }

    public boolean isFree() {
        return isFree;
    }

    public void setFree(boolean free) {
        isFree = free;
    }
}
