
package com.uaebarq.uaebarq.models.DashBoardModel;

import java.io.Serializable;
import java.util.ArrayList;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Payload implements Serializable {

    @SerializedName("Slider")
    @Expose
    private ArrayList<Slider> slider = null;
    @SerializedName("CategoryPosts")
    @Expose
    private ArrayList<CategoryPost> categoryPosts = null;
    @SerializedName("Categories")
    @Expose
    private ArrayList<Category> categories = null;
    @SerializedName("Banner")
    @Expose
    private ArrayList<Banner> banner = null;

    /**
     * No args constructor for use in serialization
     */
    public Payload() {
    }

    /**
     * @param slider
     * @param categoryPosts
     * @param categories
     * @param banner
     */
    public Payload(ArrayList<Slider> slider, ArrayList<CategoryPost> categoryPosts, ArrayList<Category> categories, ArrayList<Banner> banner) {
        super();
        this.slider = slider;
        this.categoryPosts = categoryPosts;
        this.categories = categories;
        this.banner = banner;
    }

    public ArrayList<Slider> getSlider() {
        return slider;
    }

    public void setSlider(ArrayList<Slider> slider) {
        this.slider = slider;
    }

    public ArrayList<CategoryPost> getCategoryPosts() {
        return categoryPosts;
    }

    public void setCategoryPosts(ArrayList<CategoryPost> categoryPosts) {
        this.categoryPosts = categoryPosts;
    }

    public ArrayList<Category> getCategories() {
        return categories;
    }

    public void setCategories(ArrayList<Category> categories) {
        this.categories = categories;
    }

    public ArrayList<Banner> getBanner() {
        return banner;
    }

    public void setBanner(ArrayList<Banner> banner) {
        this.banner = banner;
    }

}
