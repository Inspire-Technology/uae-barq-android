
package com.uaebarq.uaebarq.models.WeatherModel;

import java.io.Serializable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Weather implements Serializable {

    @SerializedName("currentTepm")
    @Expose
    private Integer currentTepm;
    @SerializedName("mim_temp")
    @Expose
    private Integer mimTemp;
    @SerializedName("max_temp")
    @Expose
    private Integer maxTemp;
    @SerializedName("weather_description")
    @Expose
    private String weatherDescription;
    @SerializedName("icon")
    @Expose
    private String icon;
    private final static long serialVersionUID = 2420683383060302482L;

    /**
     * No args constructor for use in serialization
     */
    public Weather() {
    }

    /**
     * @param maxTemp
     * @param icon
     * @param mimTemp
     * @param currentTepm
     * @param weatherDescription
     */
    public Weather(Integer currentTepm, Integer mimTemp, Integer maxTemp, String weatherDescription, String icon) {
        super();
        this.currentTepm = currentTepm;
        this.mimTemp = mimTemp;
        this.maxTemp = maxTemp;
        this.weatherDescription = weatherDescription;
        this.icon = icon;
    }

    public Integer getCurrentTepm() {
        return currentTepm;
    }

    public void setCurrentTepm(Integer currentTepm) {
        this.currentTepm = currentTepm;
    }

    public Integer getMimTemp() {
        return mimTemp;
    }

    public void setMimTemp(Integer mimTemp) {
        this.mimTemp = mimTemp;
    }

    public Integer getMaxTemp() {
        return maxTemp;
    }

    public void setMaxTemp(Integer maxTemp) {
        this.maxTemp = maxTemp;
    }

    public String getWeatherDescription() {
        return weatherDescription;
    }

    public void setWeatherDescription(String weatherDescription) {
        this.weatherDescription = weatherDescription;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

}
