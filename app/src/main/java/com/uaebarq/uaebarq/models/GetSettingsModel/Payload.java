
package com.uaebarq.uaebarq.models.GetSettingsModel;

import java.io.Serializable;
import java.util.ArrayList;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Payload implements Serializable {

    @SerializedName("CategoriesWithoutChilds")
    @Expose
    private ArrayList<CategoriesWithoutChild> categoriesWithoutChilds = null;
    @SerializedName("CategoriesWithChilds")
    @Expose
    private ArrayList<CategoriesWithChild> categoriesWithChilds = null;
    @SerializedName("is_notification")
    @Expose
    private boolean is_notification;

    /**
     * No args constructor for use in serialization
     * 
     */
    public Payload() {
    }

    /**
     * 
     * @param categoriesWithoutChilds
     * @param categoriesWithChilds
     */
    public Payload(ArrayList<CategoriesWithoutChild> categoriesWithoutChilds, ArrayList<CategoriesWithChild> categoriesWithChilds) {
        super();
        this.categoriesWithoutChilds = categoriesWithoutChilds;
        this.categoriesWithChilds = categoriesWithChilds;
    }

    public ArrayList<CategoriesWithoutChild> getCategoriesWithoutChilds() {
        return categoriesWithoutChilds;
    }

    public void setCategoriesWithoutChilds(ArrayList<CategoriesWithoutChild> categoriesWithoutChilds) {
        this.categoriesWithoutChilds = categoriesWithoutChilds;
    }

    public ArrayList<CategoriesWithChild> getCategoriesWithChilds() {
        return categoriesWithChilds;
    }

    public void setCategoriesWithChilds(ArrayList<CategoriesWithChild> categoriesWithChilds) {
        this.categoriesWithChilds = categoriesWithChilds;
    }

    public boolean isIs_notification() {
        return is_notification;
    }

    public void setIs_notification(boolean is_notification) {
        this.is_notification = is_notification;
    }
}
