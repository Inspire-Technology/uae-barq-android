
package com.uaebarq.uaebarq.models.DashBoardModel;

import java.io.Serializable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Banner implements Serializable {

    @SerializedName("url")
    @Expose
    private String url;
    @SerializedName("text")
    @Expose
    private String text;
    @SerializedName("imge")
    @Expose
    private String imge;

    /**
     * No args constructor for use in serialization
     */
    public Banner() {
    }

    /**
     * @param imge
     * @param text
     * @param url
     */
    public Banner(String url, String text, String imge) {
        super();
        this.url = url;
        this.text = text;
        this.imge = imge;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getImge() {
        return imge;
    }

    public void setImge(String imge) {
        this.imge = imge;
    }

}
