
package com.uaebarq.uaebarq.models.WeatherModel;

import java.io.Serializable;
import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Payload implements Serializable {

    @SerializedName("cityName")
    @Expose
    private String cityName;
    @SerializedName("weather")
    @Expose
    private List<Weather> weather = null;
    @SerializedName("bg")
    @Expose
    private String bg;

    /**
     * No args constructor for use in serialization
     */
    public Payload() {
    }

    /**
     * @param cityName
     * @param weather
     */
    public Payload(String cityName, List<Weather> weather) {
        super();
        this.cityName = cityName;
        this.weather = weather;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public List<Weather> getWeather() {
        return weather;
    }

    public void setWeather(List<Weather> weather) {
        this.weather = weather;
    }

    public String getBg() {
        return bg;
    }

    public void setBg(String bg) {
        this.bg = bg;
    }
}
