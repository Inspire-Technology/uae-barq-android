
package com.uaebarq.uaebarq.models.YouTubeModel;

import com.uaebarq.uaebarq.models.ResponseBase;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class YouTubeModel extends ResponseBase {

    @SerializedName("Payload")
    @Expose
    private Payload payload;

    public Payload getPayload() {
        return payload;
    }

    public void setPayload(Payload payload) {
        this.payload = payload;
    }

}
