
package com.uaebarq.uaebarq.models.DetailModel;

import java.io.Serializable;
import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Payload implements Serializable {

    @SerializedName("video")
    @Expose
    private String video;
    @SerializedName("image")
    @Expose
    private String image;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("video_views")
    @Expose
    private Integer videoViews;
    @SerializedName("descriptions")
    @Expose
    private String descriptions;
    @SerializedName("is_free")
    @Expose
    private boolean isFree;
    @SerializedName("time_ago")
    @Expose
    private String timeAgo;
    @SerializedName("created")
    @Expose
    private String created;
    @SerializedName("category_title")
    @Expose
    private String categoryTitle;
    @SerializedName("related_images")
    @Expose
    private List<String> relatedImages = null;
    @SerializedName("Banner")
    @Expose
    private List<Banner> banner = null;
    private final static long serialVersionUID = -3191980125229036854L;

    /**
     * No args constructor for use in serialization
     */
    public Payload() {
    }

    /**
     * @param timeAgo
     * @param title
     * @param categoryTitle
     * @param created
     * @param isFree
     * @param videoViews
     * @param relatedImages
     * @param image
     * @param type
     * @param descriptions
     * @param video
     * @param banner
     */
    public Payload(String video, String image, String title, String type, Integer videoViews, String descriptions, boolean isFree, String timeAgo, String created, String categoryTitle, List<String> relatedImages, List<Banner> banner) {
        super();
        this.video = video;
        this.image = image;
        this.title = title;
        this.type = type;
        this.videoViews = videoViews;
        this.descriptions = descriptions;
        this.isFree = isFree;
        this.timeAgo = timeAgo;
        this.created = created;
        this.categoryTitle = categoryTitle;
        this.relatedImages = relatedImages;
        this.banner = banner;
    }

    public String getVideo() {
        return video;
    }

    public void setVideo(String video) {
        this.video = video;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Integer getVideoViews() {
        return videoViews;
    }

    public void setVideoViews(Integer videoViews) {
        this.videoViews = videoViews;
    }

    public String getDescriptions() {
        return descriptions;
    }

    public void setDescriptions(String descriptions) {
        this.descriptions = descriptions;
    }

    public boolean getIsFree() {
        return isFree;
    }

    public void setIsFree(boolean isFree) {
        this.isFree = isFree;
    }

    public String getTimeAgo() {
        return timeAgo;
    }

    public void setTimeAgo(String timeAgo) {
        this.timeAgo = timeAgo;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public String getCategoryTitle() {
        return categoryTitle;
    }

    public void setCategoryTitle(String categoryTitle) {
        this.categoryTitle = categoryTitle;
    }

    public List<String> getRelatedImages() {
        return relatedImages;
    }

    public void setRelatedImages(List<String> relatedImages) {
        this.relatedImages = relatedImages;
    }

    public List<Banner> getBanner() {
        return banner;
    }

    public void setBanner(List<Banner> banner) {
        this.banner = banner;
    }

}
