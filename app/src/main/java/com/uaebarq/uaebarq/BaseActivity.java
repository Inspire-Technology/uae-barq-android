package com.uaebarq.uaebarq;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.appsflyer.AppsFlyerLib;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.uaebarq.uaebarq.gcm.GCMRegistrationIntentService;
import com.uaebarq.uaebarq.models.AddPremiumModel.AddPremiumModel;
import com.uaebarq.uaebarq.models.FetchSettingsModel.FetchSettingsModel;
import com.uaebarq.uaebarq.models.ResponseBase;
import com.uaebarq.uaebarq.preferences.AppConstants;
import com.uaebarq.uaebarq.preferences.SharedPreferenceManager;
import com.uaebarq.uaebarq.retrofit.NetworkInterface.NetworkPostServices;
import com.uaebarq.uaebarq.singleton.Singleton;
import com.uaebarq.uaebarq.utils.Constant;
import com.uaebarq.uaebarq.utils.LocaleHelper;
import com.uaebarq.uaebarq.utils.UaeBarqUtils;
import com.uaebarq.uaebarq.utils.Utils;

import java.util.HashMap;
import java.util.Map;

import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public abstract class BaseActivity extends AppCompatActivity {

    public BroadcastReceiver mRegistrationBroadcastReceiver;
    protected View llFooter;
    private Call<FetchSettingsModel> fetchSettings;
    private String price = "0";
    AlertDialog alertDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_RTL);
        setContentView(setLayoutId());
        ButterKnife.bind(this);
        getGCMToken();
        AppsFlyerLib.getInstance().startTracking(this.getApplication(), Constant.AppsFlyer_Device_Key);
    }



    public boolean isArabicLanguage() {
        return UaeBarqUtils.getAppUtils(this).isArabic(this);
    }

    public boolean isOnline() {
        return UaeBarqUtils.getAppUtils(this).isOnline(this);
    }

    public boolean isEnglishLanguage() {
        return UaeBarqUtils.getAppUtils(this).isEnglish(this);
    }

    public boolean isNullOrEmpty(String s) {
        return UaeBarqUtils.getAppUtils(this).isNullOrEmpty(s);
    }

    public void refreshHome() {
        SharedPreferenceManager.getSharedInstance().setLongInPreferences(AppConstants.SharedPreferenceKeys.DASHBOARD_REFRESH_DB_TIME.getKey(), 0);
        Intent intent = new Intent(this, HomeActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        overridePendingTransition(0, 0);
        this.finish();
    }

    public boolean isAndroidSDK(){
        boolean isSDK = false;
        if(SharedPreferenceManager.getSharedInstance().getIntFromPreferences(AppConstants.SharedPreferenceKeys.SDK.getKey()) == 1){
            isSDK = true;
        }
        return isSDK;
    }

//    public int isSwitchOn() {
//        return SharedPreferenceManager.getSharedInstance().getIntFromPreferences(AppConstants.SharedPreferenceKeys.VALUE_NOTI.getKey());
//    }
//
//    public boolean setSwitchOn(int value) {
//        return SharedPreferenceManager.getSharedInstance().setIntInPreferences(AppConstants.SharedPreferenceKeys.VALUE_NOTI.getKey(), value);
//    }


//    public void setToolBarHeight(LinearLayout toolBarHeight) {
//        int value = UaeBarqUtils.getAppUtils(this).getStatusBarHeight(this);
//        LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) toolBarHeight.getLayoutParams();
//        params.setMargins(0, value, 0, 0);
//        toolBarHeight.setLayoutParams(params);
//    }
//
//    public void setToolBarHeightWithRelative(LinearLayout toolBarHeight) {
//        int value = UaeBarqUtils.getAppUtils(this).getStatusBarHeight(this);
//        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) toolBarHeight.getLayoutParams();
//        params.setMargins(0, value, 0, 0);
//        toolBarHeight.setLayoutParams(params);
//    }

    public void setToolbarText(TextView tvToolBar, String strName) {
        tvToolBar.setText(strName);
    }

    public void setToolbarImageVisibility(ImageView ivHamburg, ImageView ivBack) {
        ivHamburg.setVisibility(View.GONE);
        ivBack.setVisibility(View.VISIBLE);
    }

    public boolean setPremiumUser(boolean value) {
        return SharedPreferenceManager.getSharedInstance().setBooleanInPreferences(AppConstants.SharedPreferenceKeys.PREMIUM.getKey(), value);
    }

    public boolean isPremiumUser() {
        return UaeBarqUtils.getAppUtils(this).isPremiumUser();
    }


    private void getGCMToken() {
        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            //When the broadcast received
            //We are sending the broadcast from GCMRegistrationIntentService
            @Override
            public void onReceive(Context context, Intent intent) {
                if (intent.getAction().equals(GCMRegistrationIntentService.REGISTRATION_SUCCESS)) {
                    String token = intent.getStringExtra("token");
                    if (!UaeBarqUtils.getAppUtils(context).isNullOrEmpty(token)) {
                        SharedPreferenceManager.getSharedInstance().setStringInPreferences(AppConstants.SharedPreferenceKeys.GCM_TOKEN.getKey(), token);
                        fetchAppSettings();
                    }
                    //if the intent is not with success then displaying error messages
                } else if (intent.getAction().equals(GCMRegistrationIntentService.REGISTRATION_ERROR)) {
                    UaeBarqUtils.getAppUtils(context).showToast(getApplicationContext(), "GCM registration error!");
                } else {
                    UaeBarqUtils.getAppUtils(context).showToast(getApplicationContext(), "Error occurred");
                }
            }
        };
        //Checking play service is available or not
        int resultCode = GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(this);
        //if play service is not available
        if (ConnectionResult.SUCCESS != resultCode) {
            //If play service is supported but not installed
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                UaeBarqUtils.getAppUtils(this).showToast(getApplicationContext(), "Google Play Service is not install/enabled in this device!");
                //Displaying message that play service is not installed
                GooglePlayServicesUtil.showErrorNotification(resultCode, getApplicationContext());
                //If play service is not supported
                //Displaying an error message
            } else {
                UaeBarqUtils.getAppUtils(this).showToast(getApplicationContext(), "This device does not support for Google Play Service!");
            }
            //If play service is available
        } else {
            //Starting intent to register device
            Intent itent = new Intent(this, GCMRegistrationIntentService.class);
            startService(itent);
        }
    }

    public void fetchAppSettings() {
        fetchSettings = ApplicationData.getInstance().getRestClient().createService(NetworkPostServices.class)
                .fetchSettings(Constant.API_KEY,
                        getUserId(),
                        Constant.DEVICE_TYPE,
                        getPakageName(),
                        UaeBarqUtils.getAppUtils(this).getCurrentLang(this));
        fetchSettings.enqueue(new Callback<FetchSettingsModel>() {
            @Override
            public void onResponse(Call<FetchSettingsModel> call, Response<FetchSettingsModel> response) {
                try {
                    if (response.body() != null && response.body().getSuccess().equalsIgnoreCase("true")) {
                        Singleton.getInstance().fetchSettingsModel = response.body();
                        SharedPreferenceManager.getSharedInstance().saveSettingsModel(response.body());
                        saveUserId(String.valueOf(Singleton.getInstance().fetchSettingsModel.getPayload().getUserId()));

                        String phoneNumber=response.body().getPayload().getPhone_num();
                        if(phoneNumber!=null)
                        {
                            SharedPreferenceManager.getSharedInstance().savePhoneNumber(phoneNumber);
                        }

                        setPremiumUser(Singleton.getInstance().fetchSettingsModel.getPayload().isPremium());
                        setUpForNotification();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            @Override
            public void onFailure(Call<FetchSettingsModel> call, Throwable t) {
                Log.e("", "" + t.getMessage());
            }
        });
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        if (UaeBarqUtils.getAppUtils(this).getCurrentLang(newBase).equalsIgnoreCase(Singleton.getInstance().arabic)) {
            super.attachBaseContext(LocaleHelper.onAttach(newBase, Singleton.getInstance().arabic));
        } else {
            super.attachBaseContext(LocaleHelper.onAttach(newBase, Singleton.getInstance().english));
        }
    }

    public void setUpForNotification() {
        String token = UaeBarqUtils.getAppUtils(this).getGCMToken();
        if (!isNullOrEmpty(token)) {
            String userId;
            if (Singleton.getInstance().fetchSettingsModel != null) {
                userId = String.valueOf(SharedPreferenceManager.getSharedInstance().getSettingsModelFromPreferences().getPayload().getUserId());
                if (!isNullOrEmpty(userId)) {
                    registerDevice(userId, token);
                }
            }
        }
    }

    public void registerDevice(String userId, String gcmToken) {
        Call<ResponseBase> registerDeviceCall = ApplicationData.getRestClient().createService(NetworkPostServices.class).
                postGcmToken(
                        userId,
                        Constant.DEVICE_TYPE,
                        Constant.DEVICE_TYPE,
                        UaeBarqUtils.getAppUtils(this).appVersion(),
                        getPakageName(),
                        UaeBarqUtils.getAppUtils(this).getCurrentLang(this),
                        gcmToken);
        registerDeviceCall.enqueue(new Callback<ResponseBase>() {
            @Override
            public void onResponse(Call<ResponseBase> call, Response<ResponseBase> response) {
                if (response.body() != null) {
                    if (response.body().getSuccess().equalsIgnoreCase("true")) {
                        if (!getApplicationContext().getPackageName().equalsIgnoreCase(getResources().getString(R.string.package_barqbundle))) {
                            if(ApplicationData.getInstance().fromSDK) {
                                sendUserPhoneNumber();
                            }
                        }
                    } else if (response.body().getMessage().equalsIgnoreCase("Update your app version")) {
                        openAppUpdateDialog();
                    } else if (isPremiumUser() && response.body().getMessage().equalsIgnoreCase("Your subscription is expired")) {

                            startActivity(new Intent(BaseActivity.this, PremiumCodeActivity.class));

                        UaeBarqUtils.getAppUtils(getApplicationContext()).showToast(getApplicationContext(), response.body().getMessage());
                    }
                } else {
                    UaeBarqUtils.getAppUtils(getApplicationContext()).showToast(getApplicationContext(), response.message().toString());
                }
            }

            @Override
            public void onFailure(Call<ResponseBase> call, Throwable t) {
                return;
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Constant.BILLING_CODE) {
            if (resultCode == RESULT_OK) {
                ApplicationData.getInstance().fromSDK = true;
            }
            finish();
        }
    }

    public void openAppUpdateDialog() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setTitle(getResources().getString(R.string.update_availabe));
        alertDialogBuilder.setMessage(getResources().getString(R.string.update_availabe_message));
        alertDialogBuilder.setPositiveButton(getResources().getString(R.string.dialog_yes), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface arg0, int arg1) {
                final String appPackageName = getPackageName();
                try {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                } catch (android.content.ActivityNotFoundException anfe) {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                }
                arg0.dismiss();
            }
        });
        alertDialog = alertDialogBuilder.create();
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setOnKeyListener(new DialogInterface.OnKeyListener() {
            @Override
            public boolean onKey(DialogInterface arg0, int keyCode, KeyEvent event) {
                // TODO Auto-generated method stub
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    if(alertDialog !=null && alertDialog.isShowing()){
                        alertDialog.dismiss();
                    }
                    finish();
                }
                return true;
            }
        });
        alertDialog.show();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if(alertDialog !=null && alertDialog.isShowing()){
            alertDialog.dismiss();
        }
    }
    private void sendUserPhoneNumber() {
        Map<String, String> data = new HashMap<>();
      //  data.put("phone_num", MobileArts_Billing.getCorrectedPhoneNumber(BaseActivity.this));
        data.put("device_token", UaeBarqUtils.getAppUtils(this).getGCMToken());
        data.put("api_key", Constant.API_KEY);
       // data.put("msisdn", MobileArts_Billing.getCorrectedPhoneNumber(BaseActivity.this));
        if (!isNullOrEmpty(getUserId())) {
            data.put("user_id", getUserId());
        }

        Call<AddPremiumModel> loginModelCall = ApplicationData.getRestClient().createService(NetworkPostServices.class)
                .postPremiumUser(data);
        loginModelCall.enqueue(new Callback<AddPremiumModel>() {
            @Override
            public void onResponse(Call<AddPremiumModel> call, Response<AddPremiumModel> response) {
                try {
                    if (response.body().getSuccess().equalsIgnoreCase("true")) {
                        AddPremiumModel resultModel = response.body();
                        saveUserId(resultModel.getPayload().getUser_id());
                        setPremiumUser(resultModel.getPayload().getSetting().isPremiumUserStatus());
                        //Use AppsFlyer in App purchase
                        if (response.code() == 200) {
                            ApplicationData.getInstance().fromSDK = false;

                          //  UaeBarqUtils.getAppUtils(getApplicationContext()).setInAppPurchasedAppFlyer(MobileArts_Billing.getCorrectedPhoneNumber(BaseActivity.this), getUserId(), price);
                        }
                    } else {
                        UaeBarqUtils.getAppUtils(getApplicationContext()).showToast(getApplicationContext(), response.body().getMessage());
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<AddPremiumModel> call, Throwable t) {
                Log.e("", "Log", t);
            }
        });
    }

    public long getPreviousMilliSeconds() {
        long milliSecondsPrevious = SharedPreferenceManager.getSharedInstance().getLongFromPreferences(AppConstants.SharedPreferenceKeys.DASHBOARD_REFRESH_DB_TIME.getKey());
        return milliSecondsPrevious;
    }

    public String getPakageName() {
        return getApplicationContext().getPackageName();
    }

    public boolean isMoreThanOneHours() {
        return UaeBarqUtils.getAppUtils(this).isMoreThanOneHours(System.currentTimeMillis(), getPreviousMilliSeconds());
    }

    protected abstract int setLayoutId();

    @Override
    protected void onResume() {
        super.onResume();
        Utils.trackScreenNew(this.getClass().getSimpleName(), this);
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver, new IntentFilter(GCMRegistrationIntentService.REGISTRATION_SUCCESS));
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver, new IntentFilter(GCMRegistrationIntentService.REGISTRATION_ERROR));
    }

    @Override
    protected void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mRegistrationBroadcastReceiver);
    }

    public void saveUserId(String userId) {
        SharedPreferenceManager.getSharedInstance().setStringInPreferences(AppConstants.SharedPreferenceKeys.USER_ID.getKey(), userId);
    }

    public String getUserId() {
        return SharedPreferenceManager.getSharedInstance().getStringFromPreferances(AppConstants.SharedPreferenceKeys.USER_ID.getKey());
    }

}
