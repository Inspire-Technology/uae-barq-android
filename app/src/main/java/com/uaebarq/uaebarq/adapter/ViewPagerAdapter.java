package com.uaebarq.uaebarq.adapter;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.uaebarq.uaebarq.BaseActivity;
import com.uaebarq.uaebarq.HomeActivity;
import com.uaebarq.uaebarq.R;
import com.uaebarq.uaebarq.models.DashBoardModel.Slider;
import com.uaebarq.uaebarq.utils.Constant;
import com.uaebarq.uaebarq.utils.UaeBarqUtils;

import java.util.ArrayList;

/**
 * Created by naweed on 8/6/17.
 */

public class ViewPagerAdapter extends PagerAdapter {

    private ArrayList<Slider> images;
    private LayoutInflater inflater;
    private Context context;

    public ViewPagerAdapter(Context context, ArrayList<Slider> images) {
        this.context = context;
        this.images=images;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    @Override
    public int getCount() {
        return images.size();
    }

    @Override
    public Object instantiateItem(ViewGroup view, final int position) {
        View myImageLayout = inflater.inflate(R.layout.image_slider_item, view, false);
        LinearLayout rlTag = myImageLayout.findViewById(R.id.rl_tag);
        Drawable drawable = UaeBarqUtils.getAppUtils(context).getDrawable(context, images.get(position).getCategoryTitle());
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            rlTag.setBackground(drawable);
        }
        ImageView myImage = myImageLayout.findViewById(R.id.image);
        TextView tvTag = myImageLayout.findViewById(R.id.tv_tag);
        TextView tvTitle = myImageLayout.findViewById(R.id.tv_title);
        TextView tvDate = myImageLayout.findViewById(R.id.tv_date);
        if(!UaeBarqUtils.getAppUtils(context).isNullOrEmpty(images.get(position).getCategoryTitle())){
            tvTag.setText(images.get(position).getCategoryTitle());
        }
        if(!UaeBarqUtils.getAppUtils(context).isNullOrEmpty(images.get(position).getTitle())){
            tvTitle.setText(images.get(position).getTitle());
        }
        if(!UaeBarqUtils.getAppUtils(context).isNullOrEmpty(images.get(position).getCreated())){
            tvDate.setText(images.get(position).getCreated()+" | "+ images.get(position).getTimeAgo());
        }
        if(!UaeBarqUtils.getAppUtils(context).isNullOrEmpty(images.get(position).getImage())){
            Picasso.with(context)
                    .load(images.get(position).getImage().toString())
                    .into(myImage);
        }

        if (UaeBarqUtils.getAppUtils(context).isEnglish(context)) {
            UaeBarqUtils.FontUtils.applyFonts(context, Constant.strTypefaceEnglishRegular, tvTag);
            UaeBarqUtils.FontUtils.applyFonts(context, Constant.strTypefaceEnglishMedium, tvTitle);
            UaeBarqUtils.FontUtils.applyFonts(context, Constant.strTypefaceEnglishRegular, tvDate);
        } else {
            UaeBarqUtils.FontUtils.applyFonts(context, Constant.strTypefaceArabicRegular, tvTag);
            UaeBarqUtils.FontUtils.applyFonts(context, Constant.strTypefaceArabicMedium, tvTitle);
            UaeBarqUtils.FontUtils.applyFonts(context, Constant.strTypefaceArabicRegular, tvDate);
        }

        myImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               // int currentItem = viewPager.getCurrentItem();
                Bundle bundle = new Bundle();
                if (images.get(position).isFree()) {
                    ((HomeActivity) context).passIntent(bundle, position);
                } else {
                    if (((BaseActivity) context).isPremiumUser()) {
                        ((HomeActivity) context).passIntent(bundle, position);
                    } else {
                        //premium session
                        ((HomeActivity) context).openPremiumActivity();
                    }
                }
            }
        });

        view.addView(myImageLayout, 0);
        return myImageLayout;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view.equals(object);
    }
}

