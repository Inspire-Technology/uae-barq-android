package com.uaebarq.uaebarq.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import com.uaebarq.uaebarq.BaseActivity;
import com.uaebarq.uaebarq.DetailActivityTextImage;
import com.uaebarq.uaebarq.DetailActivityVideo;
import com.uaebarq.uaebarq.PremiumCodeActivity;
import com.uaebarq.uaebarq.R;
import com.uaebarq.uaebarq.models.AllCategoryListModel.Payload;
import com.uaebarq.uaebarq.models.AllCategoryListModel.Post;
import com.uaebarq.uaebarq.utils.Constant;
import com.uaebarq.uaebarq.utils.UaeBarqUtils;
import com.uaebarq.uaebarq.viewholder.ViewHolder;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class AllCategoryListAdapter extends ArrayAdapter<Payload> {

    private final int VIEW_TYPE_IMAGE = 0;
    private final int VIEW_TYPE_TEXT = 1;

    private ArrayList<Post> mList = new ArrayList<>();
    private LayoutInflater mInflater;
    private Activity context;

    public AllCategoryListAdapter(Activity context, int resource, ArrayList<Post> objects) {
        super(context, resource);
        this.context = context;
        this.mList = objects;
        mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public View getView(final int position, View convertView, final ViewGroup parent) {

        ViewHolder.DataViewFirstItemHolder dataViewFirstItemHolder;
        ViewHolder.DataViewSecondItemHolder dataViewSecondItemHolder;
        final Post payload = mList.get(position);
        int type = getItemViewType(position);
        if (type == VIEW_TYPE_IMAGE) {
            if (convertView == null) {
                convertView = mInflater.inflate(R.layout.all_cat_first_list_item, parent, false);
                dataViewFirstItemHolder = new ViewHolder().new DataViewFirstItemHolder();
                dataViewFirstItemHolder.ivImage = convertView.findViewById(R.id.iv_main);
                dataViewFirstItemHolder.rlTag = convertView.findViewById(R.id.rl_tag);
                dataViewFirstItemHolder.tvTagTitle = convertView.findViewById(R.id.tv_tag);
                dataViewFirstItemHolder.tvTitle = convertView.findViewById(R.id.tv_title);
                dataViewFirstItemHolder.tvDate = convertView.findViewById(R.id.tv_date);
                convertView.setTag(dataViewFirstItemHolder);
            } else {
                dataViewFirstItemHolder = (ViewHolder.DataViewFirstItemHolder) convertView.getTag();
            }
            dataViewFirstItemHolder.ivImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    setClickListener(payload);
                }
            });
            if (payload != null && !UaeBarqUtils.getAppUtils(getContext()).isNullOrEmpty(payload.getImage())) {
                Picasso.with(getContext())
                        .load(payload.getImage())
                        .into(dataViewFirstItemHolder.ivImage);
            }

            if (payload != null && !UaeBarqUtils.getAppUtils(context).isNullOrEmpty(payload.getCategoryTitle())) {
                dataViewFirstItemHolder.tvTagTitle.setText(payload.getCategoryTitle());
            }

            Drawable drawable = UaeBarqUtils.getAppUtils(context).getDrawable(context, payload.getCategoryTitle());
            dataViewFirstItemHolder.rlTag.setBackground(drawable);

            if (payload != null && !UaeBarqUtils.getAppUtils(context).isNullOrEmpty(payload.getTitle())) {
                dataViewFirstItemHolder.tvTitle.setText(payload.getTitle());
            }

            if (payload != null && !UaeBarqUtils.getAppUtils(context).isNullOrEmpty(payload.getCreated())) {
                dataViewFirstItemHolder.tvDate.setText(payload.getCreated() + " | " + payload.getTimeAgo());
            }
            if (UaeBarqUtils.getAppUtils(context).isEnglish(context)) {
                UaeBarqUtils.FontUtils.applyFonts(context, Constant.strTypefaceEnglishMedium, dataViewFirstItemHolder.tvTitle);
                UaeBarqUtils.FontUtils.applyFonts(context, Constant.strTypefaceEnglishRegular, dataViewFirstItemHolder.tvDate);
                UaeBarqUtils.FontUtils.applyFonts(context, Constant.strTypefaceEnglishRegular, dataViewFirstItemHolder.tvTagTitle);
            } else {
                UaeBarqUtils.FontUtils.applyFonts(context, Constant.strTypefaceArabicMedium, dataViewFirstItemHolder.tvTitle);
                UaeBarqUtils.FontUtils.applyFonts(context, Constant.strTypefaceArabicRegular, dataViewFirstItemHolder.tvDate);
                UaeBarqUtils.FontUtils.applyFonts(context, Constant.strTypefaceArabicRegular, dataViewFirstItemHolder.tvTagTitle);
            }

        } else if (type == VIEW_TYPE_TEXT) {
            if (convertView == null) {
                convertView = mInflater.inflate(R.layout.all_cat_second_list_item, parent, false);
                dataViewSecondItemHolder = new ViewHolder().new DataViewSecondItemHolder();
                dataViewSecondItemHolder.llMainImage = convertView.findViewById(R.id.ll_main_image);
                dataViewSecondItemHolder.ivCatImage = convertView.findViewById(R.id.iv_image);
                dataViewSecondItemHolder.tvTagTitle = convertView.findViewById(R.id.tv_tag_title);
                dataViewSecondItemHolder.rlTagImage = convertView.findViewById(R.id.rl_tag_image);
                dataViewSecondItemHolder.tvTitle = convertView.findViewById(R.id.tv_title);
                dataViewSecondItemHolder.ivTriangle = convertView.findViewById(R.id.iv_triangle);
                dataViewSecondItemHolder.tvDate = convertView.findViewById(R.id.tv_date);
                convertView.setTag(dataViewSecondItemHolder);
            } else {
                dataViewSecondItemHolder = (ViewHolder.DataViewSecondItemHolder) convertView.getTag();
            }
            dataViewSecondItemHolder.llMainImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    setClickListener(payload);
                }
            });

            if (payload != null && !UaeBarqUtils.getAppUtils(context).isNullOrEmpty(payload.getImage())) {
                Picasso.with(context)
                        .load(payload.getImage())
                        .into(dataViewSecondItemHolder.ivCatImage);
            }
            if (payload != null && !UaeBarqUtils.getAppUtils(context).isNullOrEmpty(payload.getCategoryTitle())) {
                dataViewSecondItemHolder.tvTagTitle.setText(payload.getCategoryTitle());
            }

            Drawable drawable = UaeBarqUtils.getAppUtils(context).getDrawable(context, payload.getCategoryTitle());
            dataViewSecondItemHolder.rlTagImage.setBackground(drawable);

            if (payload != null && !UaeBarqUtils.getAppUtils(context).isNullOrEmpty(payload.getTitle())) {
                dataViewSecondItemHolder.tvTitle.setText(payload.getTitle());
            }

            if (payload != null && !UaeBarqUtils.getAppUtils(context).isNullOrEmpty(payload.getCreated())) {
                dataViewSecondItemHolder.tvDate.setText(payload.getCreated() + " | " + payload.getTimeAgo());
            }
            if (payload.isFree()) {
                dataViewSecondItemHolder.ivTriangle.setVisibility(View.INVISIBLE);
            } else {
                dataViewSecondItemHolder.ivTriangle.setVisibility(View.VISIBLE);
            }
            if (UaeBarqUtils.getAppUtils(context).isEnglish(context)) {
                UaeBarqUtils.FontUtils.applyFonts(context, Constant.strTypefaceEnglishMedium, dataViewSecondItemHolder.tvTitle);
                UaeBarqUtils.FontUtils.applyFonts(context, Constant.strTypefaceEnglishRegular, dataViewSecondItemHolder.tvDate);
                UaeBarqUtils.FontUtils.applyFonts(context, Constant.strTypefaceEnglishRegular, dataViewSecondItemHolder.tvTagTitle);
            } else {
                UaeBarqUtils.FontUtils.applyFonts(context, Constant.strTypefaceArabicMedium, dataViewSecondItemHolder.tvTitle);
                UaeBarqUtils.FontUtils.applyFonts(context, Constant.strTypefaceArabicRegular, dataViewSecondItemHolder.tvDate);
                UaeBarqUtils.FontUtils.applyFonts(context, Constant.strTypefaceArabicRegular, dataViewSecondItemHolder.tvTagTitle);
            }

        }

        return convertView;
    }

    private void setClickListener(Post payload) {
        Bundle bundle = new Bundle();
        if (payload != null) {
            if (payload.isFree()) {
                passIntentPayload(bundle, payload);
            } else {
                if (context instanceof BaseActivity) {
                    boolean value = ((BaseActivity) context).isPremiumUser();
                    if (value) {
                        passIntentPayload(bundle, payload);
                    }else {
                        Intent inten = new Intent(context, PremiumCodeActivity.class);
                        context.startActivity(inten);
                    }
                }
            }
        }
    }


    private void passIntentPayload(Bundle bundle, Post payload) {
        bundle.putSerializable("catPostListDetail", payload);
        if (payload.getType().equalsIgnoreCase(Constant.VIDEO)) {
            Intent intent = new Intent(context, DetailActivityVideo.class);
            intent.putExtras(bundle);
            context.startActivity(intent);
        } else {
            Intent intent = new Intent(context, DetailActivityTextImage.class);
            intent.putExtras(bundle);
            context.startActivity(intent);
        }
    }


    @Override
    public int getViewTypeCount() {
        return 2;
    }

    @Override
    public int getItemViewType(int position) {
        return (position == 0 ? VIEW_TYPE_IMAGE : VIEW_TYPE_TEXT);
    }

    @Override
    public int getCount() {
        return null == mList ? 0 : mList.size();
    }

    @Nullable
    @Override
    public Payload getItem(int position) {
        return super.getItem(position);
    }
}

