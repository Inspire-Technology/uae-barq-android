package com.uaebarq.uaebarq.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.uaebarq.uaebarq.GalleryViewActivity;
import com.uaebarq.uaebarq.R;
import com.uaebarq.uaebarq.interfaceadapter.IMethodCaller;
import com.uaebarq.uaebarq.models.TwitterModel.Feed;
import com.uaebarq.uaebarq.utils.Constant;
import com.uaebarq.uaebarq.utils.UaeBarqUtils;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by naweed on 8/8/17.
 */

public class TwitterAdapter extends RecyclerView.Adapter<TwitterAdapter.RecyclerViewHolder> {

    // recyclerview adapter
    private ArrayList<Feed> arrayList;
    private Context context;

    public TwitterAdapter(Context context, ArrayList<Feed> arrayList) {
        this.context = context;
        this.arrayList = arrayList;

    }

    @Override
    public int getItemCount() {
        return (null != arrayList ? arrayList.size() : 0);

    }

    @Override
    public void onBindViewHolder(final RecyclerViewHolder holder, int position) {
        final Feed model = arrayList.get(position);

        RecyclerViewHolder mainHolder = holder;// holder
        mainHolder.tvTwitterSimple.setText(model.getScreenName());
        mainHolder.tvTwitterTitle.setText(model.getDescriptions());
        mainHolder.tvTwitterDate.setText(model.getCreated());
        UaeBarqUtils.getAppUtils(context).setTextView(mainHolder.tvTwitterTitle, context);

        if (!model.getProfileImage().equalsIgnoreCase("")) {
            Picasso.with(context)
                    .load(model.getProfileImage())
                    .into(mainHolder.ivTwitter);
        }
        if (!model.getPostImage().equalsIgnoreCase("")) {
            Picasso.with(context)
                    .load(model.getPostImage())
                    .into(mainHolder.ivPostImage);
        }
        mainHolder.ivPostImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, GalleryViewActivity.class);
                intent.putExtra("image", model.getPostImage());
                context.startActivity(intent);
            }
        });
        mainHolder.llForward.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (context instanceof IMethodCaller) {
                    ((IMethodCaller) context).yourDesiredMethod();
                }
            }
        });
        if (UaeBarqUtils.getAppUtils(context).isEnglish(context)) {
            UaeBarqUtils.FontUtils.applyFonts(context, Constant.strTypefaceEnglishRegular, mainHolder.mainTwitter);
        } else {
            UaeBarqUtils.FontUtils.applyFonts(context, Constant.strTypefaceArabicMedium, mainHolder.mainTwitter);
        }
    }

    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {

        // This method will inflate the custom layout and return as viewholder
        LayoutInflater mInflater = LayoutInflater.from(viewGroup.getContext());
        ViewGroup mainGroup = (ViewGroup) mInflater.inflate(R.layout.twitter_list_item, viewGroup, false);
        RecyclerViewHolder listHolder = new RecyclerViewHolder(mainGroup);
        return listHolder;
    }


    public class RecyclerViewHolder extends RecyclerView.ViewHolder {
        public TextView tvTwitterTitle, tvTwitterSimple;
        public TextView tvTwitterDate;
        public CircleImageView ivTwitter;
        public LinearLayout llForward;
        public LinearLayout mainTwitter;
        public ImageView ivPostImage;

        public RecyclerViewHolder(View view) {
            super(view);
            // Find all views ids
            this.mainTwitter = view.findViewById(R.id.main_twitter);
            this.tvTwitterTitle = view.findViewById(R.id.tv_twitter_title);
            this.tvTwitterSimple = view.findViewById(R.id.tv_twitter_simple);
            this.tvTwitterDate = view.findViewById(R.id.tv_twitter_date);
            this.ivTwitter = view.findViewById(R.id.iv_twitter);
            this.llForward = view.findViewById(R.id.ll_forward);
            this.ivPostImage = view.findViewById(R.id.iv_post_image);
        }
    }
}
