package com.uaebarq.uaebarq.adapter;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.AppCompatCheckBox;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.uaebarq.uaebarq.R;
import com.uaebarq.uaebarq.SettingsActivity;
import com.uaebarq.uaebarq.models.GetSettingsModel.CategoriesWithChild;
import com.uaebarq.uaebarq.singleton.Singleton;
import com.uaebarq.uaebarq.utils.Constant;
import com.uaebarq.uaebarq.utils.UaeBarqUtils;

import java.util.ArrayList;

public class CategoryWithChildAdapter extends BaseExpandableListAdapter {
    private SettingsActivity context;
    LayoutInflater inflateInflater;
    ArrayList<CategoriesWithChild> categoriesWithChildren;

    public CategoryWithChildAdapter(SettingsActivity context, ArrayList<CategoriesWithChild> categoriesWithChildren) {
        this.context = context;
        this.categoriesWithChildren = categoriesWithChildren;
        inflateInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return categoriesWithChildren.get(groupPosition).getSubCategoryies().get(childPosition);
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public View getChildView(final int groupPosition, final int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        categoriesWithChildren.get(groupPosition).getSubCategoryies().get(childPosition);
        final AppCompatCheckBox chBox;
        if (convertView == null) {
            convertView = inflateInflater.inflate(R.layout.row_item_with_child, null);
        }
        TextView tvChild = convertView.findViewById(R.id.addressLabel);
        tvChild.setText(categoriesWithChildren.get(groupPosition).getSubCategoryies().get(childPosition).getTitle());
        chBox = convertView.findViewById(R.id.checkbox_setting);
        chBox.setOnCheckedChangeListener(null);
        chBox.setButtonDrawable(ContextCompat.getDrawable(context, R.drawable.ic_check_box));
        boolean preSelected = categoriesWithChildren.get(groupPosition).getSubCategoryies().get(childPosition).isUserSelected();
        chBox.setChecked(preSelected);
        if(preSelected){
            chBox.setButtonDrawable(ContextCompat.getDrawable(context, R.drawable.ic_check_box_acive));
            Singleton.getInstance().childArrayList.add(categoriesWithChildren.get(groupPosition).getSubCategoryies().get(childPosition).getId());
        }
        chBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                if (isChecked) {
                    chBox.setButtonDrawable(ContextCompat.getDrawable(context, R.drawable.ic_check_box_acive));
                    Singleton.getInstance().childArrayList.add(categoriesWithChildren.get(groupPosition).getSubCategoryies().get(childPosition).getId());
                    categoriesWithChildren.get(groupPosition).getSubCategoryies().get(childPosition).setUserSelected(isChecked);
                } else {
                    chBox.setButtonDrawable(ContextCompat.getDrawable(context, R.drawable.ic_check_box));
                    Singleton.getInstance().childArrayList.remove(categoriesWithChildren.get(groupPosition).getSubCategoryies().get(childPosition).getId());
                    categoriesWithChildren.get(groupPosition).getSubCategoryies().get(childPosition).setUserSelected(isChecked);
                }

            }
        });

        if (UaeBarqUtils.getAppUtils(context).isEnglish(context)) {
            UaeBarqUtils.FontUtils.applyFonts(context, Constant.strTypefaceEnglishRegular, tvChild);
        } else {
            UaeBarqUtils.FontUtils.applyFonts(context, Constant.strTypefaceArabicRegular, tvChild);
        }
        return convertView;
    }


    @Override
    public int getChildrenCount(int groupPosition) {
        return categoriesWithChildren.get(groupPosition).getSubCategoryies().size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return categoriesWithChildren.get(groupPosition);
    }

    @Override
    public int getGroupCount() {
        return categoriesWithChildren.size();
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = inflateInflater.inflate(R.layout.row_header_with_child, null);
        }
        TextView tvListHeader = convertView.findViewById(R.id.headerTitle);
        tvListHeader.setText(categoriesWithChildren.get(groupPosition).getTitle());
        if (UaeBarqUtils.getAppUtils(context).isEnglish(context)) {
            int bgImage = groupPosition % 2;
            tvListHeader.setBackgroundResource(categoriesWithChildren.get(groupPosition).getCategoryEngImages()[bgImage]);
        } else {
            int bgImage = groupPosition % 4;
            tvListHeader.setBackgroundResource(categoriesWithChildren.get(groupPosition).getCategoryArabicImages()[bgImage]);
        }
        if (UaeBarqUtils.getAppUtils(context).isEnglish(context)) {
            UaeBarqUtils.FontUtils.applyFonts(context, Constant.strTypefaceEnglishRegular, tvListHeader);
        } else {
            UaeBarqUtils.FontUtils.applyFonts(context, Constant.strTypefaceArabicRegular, tvListHeader);
        }
        return convertView;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }
}