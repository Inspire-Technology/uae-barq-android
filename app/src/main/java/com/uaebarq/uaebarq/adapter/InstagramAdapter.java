package com.uaebarq.uaebarq.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.uaebarq.uaebarq.GalleryViewActivity;
import com.uaebarq.uaebarq.R;
import com.uaebarq.uaebarq.utils.SquaredImageView;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by naweed on 8/8/17.
 */

public class InstagramAdapter extends RecyclerView.Adapter<InstagramAdapter.RecyclerViewHolder> {

    // recyclerview adapter
    private List<String> arrayList;
    private Context context;

    public InstagramAdapter(Context context, List<String> arrayList) {
        this.context = context;
        this.arrayList = arrayList;

    }

    @Override
    public int getItemCount() {
        return (null != arrayList ? arrayList.size() : 0);

    }

    @Override
    public void onBindViewHolder(RecyclerViewHolder holder, int position) {
        final String model = arrayList.get(position);

        RecyclerViewHolder mainHolder = holder;// holder
        Picasso.with(context)
                .load(model)
                .into(mainHolder.ivInstagramPhoto);
        mainHolder.ivInstagramPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, GalleryViewActivity.class);
                intent.putExtra("image", model);
                context.startActivity(intent);
            }
        });
    }

    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {

        // This method will inflate the custom layout and return as viewholder
        LayoutInflater mInflater = LayoutInflater.from(viewGroup.getContext());
        ViewGroup mainGroup = (ViewGroup) mInflater.inflate(R.layout.instagarm_item, viewGroup, false);
        RecyclerViewHolder listHolder = new RecyclerViewHolder(mainGroup);
        return listHolder;

    }


    public class RecyclerViewHolder extends RecyclerView.ViewHolder  {
        public SquaredImageView ivInstagramPhoto;

        public RecyclerViewHolder(View view) {
            super(view);
            // Find all views ids
            this.ivInstagramPhoto = view.findViewById(R.id.iv_instagram_photo);
        }
    }
}
