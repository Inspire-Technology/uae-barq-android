package com.uaebarq.uaebarq.adapter;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.uaebarq.uaebarq.R;
import com.uaebarq.uaebarq.models.DashBoardModel.CategoryPost;
import com.uaebarq.uaebarq.utils.Constant;
import com.uaebarq.uaebarq.utils.UaeBarqUtils;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;


@SuppressWarnings("ALL")
public class CategoryListAdapter extends BaseAdapter {
    private Context context;
    ArrayList<CategoryPost> categoryPosts;

    public CategoryListAdapter(Context context, ArrayList<CategoryPost> payloads) {
        this.context = context;
        this.categoryPosts = payloads;
    }

    @Override
    public int getCount() {
        return categoryPosts.size();
    }

    @Override
    public CategoryPost getItem(int position) {
        if (categoryPosts.size() == 0) {
            return null;
        }
        return categoryPosts.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder viewHolder;
        final CategoryPost payload = getItem(position);
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.category_list_item, null, false);
            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        if (payload != null && !UaeBarqUtils.getAppUtils(context).isNullOrEmpty(payload.getCategoryTitle())) {
            viewHolder.tvTagTitle.setText(payload.getCategoryTitle());
        }
        if (payload != null && !UaeBarqUtils.getAppUtils(context).isNullOrEmpty(payload.getImage())) {
            Picasso.with(context)
                    .load(payload.getImage())
                    .into(viewHolder.ivImage);
        }

        Drawable drawable = UaeBarqUtils.getAppUtils(context).getDrawable(context, payload.getCategoryTitle());
        viewHolder.rlTagImage.setBackground(drawable);

        if (payload != null && !UaeBarqUtils.getAppUtils(context).isNullOrEmpty(payload.getTitle())) {
            viewHolder.tvTitle.setText(payload.getTitle());
        }

        if (payload != null && !UaeBarqUtils.getAppUtils(context).isNullOrEmpty(payload.getCreated())) {
            viewHolder.tvDate.setText(payload.getCreated() + " | " + payload.getTimeAgo());
        }
        if (payload.isFree()) {
            viewHolder.ivTiangle.setVisibility(View.INVISIBLE);
        }else {
            viewHolder.ivTiangle.setVisibility(View.VISIBLE);
        }
        if (UaeBarqUtils.getAppUtils(context).isEnglish(context)) {
            UaeBarqUtils.FontUtils.applyFonts(context, Constant.strTypefaceEnglishMedium, viewHolder.tvTitle);
            UaeBarqUtils.FontUtils.applyFonts(context, Constant.strTypefaceEnglishRegular, viewHolder.tvDate);
            UaeBarqUtils.FontUtils.applyFonts(context, Constant.strTypefaceEnglishRegular, viewHolder.tvTagTitle);
        } else {
            UaeBarqUtils.FontUtils.applyFonts(context, Constant.strTypefaceArabicMedium, viewHolder.tvTitle);
            UaeBarqUtils.FontUtils.applyFonts(context, Constant.strTypefaceArabicRegular, viewHolder.tvDate);
            UaeBarqUtils.FontUtils.applyFonts(context, Constant.strTypefaceArabicRegular, viewHolder.tvTagTitle);
        }
        return convertView;
    }

    private static class ViewHolder {

        private TextView tvTitle;
        private TextView tvDate, tvTagTitle;
        private ImageView ivTiangle, ivImage;
        private View rlTagImage;

        public ViewHolder(View v) {
            tvTitle = (TextView) v.findViewById(R.id.tv_title);
            tvDate = (TextView) v.findViewById(R.id.tv_date);
            ivTiangle = (ImageView) v.findViewById(R.id.iv_triangle);
            ivImage = (ImageView) v.findViewById(R.id.iv_image);
            tvTagTitle = (TextView) v.findViewById(R.id.tv_tag_title);
            rlTagImage = (View) v.findViewById(R.id.rl_tag_image);
        }
    }
}