package com.uaebarq.uaebarq;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.uaebarq.uaebarq.singleton.Singleton;
import com.uaebarq.uaebarq.utils.Constant;
import com.uaebarq.uaebarq.utils.FireBaseEvents;
import com.uaebarq.uaebarq.utils.UaeBarqUtils;
import com.uaebarq.uaebarq.utils.Utils;

import butterknife.BindView;
import butterknife.OnClick;

public class WebViewActivity extends BaseActivity {

    WebViewActivity activity;
    @BindView(R.id.progressbar)
    ProgressBar progressBar;
    @BindView(R.id.browserView)
    WebView webView;
    private int index;
    private String strUrl = "";
    Context context;
    @BindView(R.id.iv_logo)
    ImageView ivLogo;
    @BindView(R.id.tv_name)
    TextView tvName;
    private String intentUrl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activity = this;
        context = this;
        ivLogo.setVisibility(View.GONE);
        tvName.setVisibility(View.VISIBLE);
        tvName.setText(getResources().getString(R.string.web_view));
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            index = bundle.getInt("index");
            intentUrl = bundle.getString("url");
            if (index == 1) {
                tvName.setText(Singleton.getInstance().bannerText);
                strUrl = Singleton.getInstance().bannerImage;
            } else if (index == 2) {
                tvName.setText(Singleton.getInstance().bannerText);
                strUrl = Singleton.getInstance().bannerImage;
            } else if (index == 3) {
                tvName.setText(Singleton.getInstance().bannerText);
                strUrl = Singleton.getInstance().bannerImage;
            }else if(!isNullOrEmpty(intentUrl)){
                strUrl = intentUrl;
            }else if (index == 4) {
                strUrl = intentUrl;
            }
        }
        if (webView != null) {
            webView.getSettings().setSupportZoom(true);
            webView.getSettings().setLoadWithOverviewMode(true);
        }
        startWebView(strUrl);
        if (isEnglishLanguage()) {
            UaeBarqUtils.FontUtils.applyFonts(this, Constant.strTypefaceEnglishMedium, tvName);
        } else {
            UaeBarqUtils.FontUtils.applyFonts(this, Constant.strTypefaceArabicMedium, tvName);
        }

        webView.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
            }

            @Override
            public void onLoadResource(WebView view, String url) {
                super.onLoadResource(view, url);
            }

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                return super.shouldOverrideUrlLoading(view, url);
            }
        });
    }

    @Override
    protected int setLayoutId() {
        return R.layout.webview_layout;
    }


    private void startWebView(String url) {
        webView.setWebChromeClient(new WebChromeClient() {

            @Override
            public void onProgressChanged(WebView view, int progress) {
                progressBar = findViewById(R.id.progressbar);
                progressBar.setProgress(progress);
                if (progress == 100)
                    progressBar.setVisibility(View.GONE);
                else
                    progressBar.setVisibility(View.VISIBLE);
            }

        });
        webView.setWebViewClient(new WebViewClient() {

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                return true;
            }

        });
        //enable Javascript
        webView.getSettings().setJavaScriptEnabled(true);
        webView.loadUrl(url);
    }

    @OnClick(R.id.iv_back)
    void click() {
        finish();
    }
}