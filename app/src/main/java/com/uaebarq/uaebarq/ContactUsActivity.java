package com.uaebarq.uaebarq;

import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.github.silvestrpredko.dotprogressbar.DotProgressBar;
import com.uaebarq.uaebarq.models.DetailModel.DetailModel;
import com.uaebarq.uaebarq.models.OTPModel.OtpModel;
import com.uaebarq.uaebarq.models.ResponseBase;
import com.uaebarq.uaebarq.models.unsubcribe_errormodel.UnSubcribeErrorModel;
import com.uaebarq.uaebarq.preferences.AppConstants;
import com.uaebarq.uaebarq.preferences.SharedPreferenceManager;
import com.uaebarq.uaebarq.retrofit.ApiUtils2;
import com.uaebarq.uaebarq.retrofit.NetworkInterface.NetworkPostServices;
import com.uaebarq.uaebarq.utils.Constant;
import com.uaebarq.uaebarq.utils.FireBaseEvents;
import com.uaebarq.uaebarq.utils.UaeBarqUtils;
import com.uaebarq.uaebarq.utils.Utils;

import butterknife.BindView;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ContactUsActivity extends BaseActivity {

    @BindView(R.id.iv_back)
    ImageView ivBack;

    @BindView(R.id.scroll)
    ScrollView scroll;

    @BindView(R.id.progress_layout)
    View pLayout;

    @BindView(R.id.et_full_name)
    EditText etName;

    @BindView(R.id.et_number)
    EditText etPhoneNumber;

    @BindView(R.id.et_email)
    EditText etEmail;

    @BindView(R.id.et_message)
    EditText etMessage;

    @BindView(R.id.ll_social_media)
    LinearLayout llSocialMedia;

    @BindView(R.id.ll_send)
    LinearLayout llSend;

    @BindView(R.id.ll_unsub)
    LinearLayout ll_unsub;

    @BindView(R.id.dot_progress_bar)
    DotProgressBar dotProgressBar;

    @BindView(R.id.ll_fb)
    LinearLayout llFb;

    @BindView(R.id.ll_tw)
    LinearLayout llTw;

    @BindView(R.id.ll_inst)
    LinearLayout llInst;

    @BindView(R.id.main)
    LinearLayout main;

    @BindView(R.id.tv_touch)
    TextView tvTouch;

    @BindView(R.id.tv_contact_title)
    TextView tvContactTitle;

    @BindView(R.id.iv_logo)
    ImageView ivLogo;

    @BindView(R.id.tv_name)
    TextView tvName;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ivLogo.setVisibility(View.GONE);
        tvName.setVisibility(View.VISIBLE);
        ll_unsub.setVisibility(View.GONE);
        tvName.setText(getResources().getString(R.string.contact_us));
        dotProgressBar.setEndColor(ContextCompat.getColor(this, R.color.tab_gray));
        dotProgressBar.setStartColor(ContextCompat.getColor(this, R.color.tab_gray));
        etName.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int actionId, KeyEvent keyEvent) {
                if (actionId == EditorInfo.IME_ACTION_NEXT) {
                    etEmail.requestFocus();
                    etEmail.setFocusable(true);
                    etEmail.setFocusableInTouchMode(true);
                    return true;
                }
                return false;
            }
        });
        etEmail.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int actionId, KeyEvent keyEvent) {
                if (actionId == EditorInfo.IME_ACTION_NEXT) {
                    etPhoneNumber.requestFocus();
                    etPhoneNumber.setFocusable(true);
                    etPhoneNumber.setFocusableInTouchMode(true);
                    return true;
                }
                return false;
            }
        });
        etPhoneNumber.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int actionId, KeyEvent keyEvent) {
                if (actionId == EditorInfo.IME_ACTION_NEXT) {
                    etMessage.requestFocus();
                    etMessage.setFocusable(true);
                    etMessage.setFocusableInTouchMode(true);
                    return true;
                }
                return false;
            }
        });
        etName.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {

                v.setFocusable(true);
                v.setFocusableInTouchMode(true);
                return false;
            }
        });

        etEmail.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {

                v.setFocusable(true);
                v.setFocusableInTouchMode(true);
                return false;
            }
        });

        etPhoneNumber.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {

                v.setFocusable(true);
                v.setFocusableInTouchMode(true);
                return false;
            }
        });

        etMessage.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {

                v.setFocusable(true);
                v.setFocusableInTouchMode(true);
                return false;
            }
        });

        if (isEnglishLanguage()) {
            UaeBarqUtils.FontUtils.applyFonts(this, Constant.strTypefaceEnglishRegular, main);
            UaeBarqUtils.FontUtils.applyFonts(this, Constant.strTypefaceEnglishMedium, tvContactTitle);
            UaeBarqUtils.FontUtils.applyFonts(this, Constant.strTypefaceEnglishMedium, tvTouch);
            UaeBarqUtils.FontUtils.applyFonts(this, Constant.strTypefaceEnglishMedium, tvName);
        } else {
            UaeBarqUtils.FontUtils.applyFonts(this, Constant.strTypefaceArabicRegular, main);
            UaeBarqUtils.FontUtils.applyFonts(this, Constant.strTypefaceArabicMedium, tvContactTitle);
            UaeBarqUtils.FontUtils.applyFonts(this, Constant.strTypefaceArabicMedium, tvTouch);
            UaeBarqUtils.FontUtils.applyFonts(this, Constant.strTypefaceArabicMedium, tvName);
        }

        String getPhonenumber= SharedPreferenceManager.getSharedInstance().getStringFromPreferances(AppConstants.SharedPreferenceKeys.PHONE_NUMBER.getKey());
       // getPhonenumber="971521756294";
        try {
            if (getPhonenumber.length() > 5) {
                pLayout.setVisibility(View.VISIBLE);
                getissub(getPhonenumber);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


    }


    @OnClick(R.id.ll_send)
    void sendClick() {

        if (isValidate()) {
            if (isOnline()) {
                Utils.trackEvent(this.getClass().getSimpleName(), FireBaseEvents.contactUsSendClick);
                pLayout.setVisibility(View.VISIBLE);
                scroll.setVisibility(View.GONE);
                llSocialMedia.setVisibility(View.GONE);
                llSend.setVisibility(View.GONE);
                UaeBarqUtils.getAppUtils(this).hideKeyBoard(etMessage, this);
                String strName = etName.getText().toString().trim();
                String strPhoneNumber = etPhoneNumber.getText().toString().trim();
                String strEmail = etEmail.getText().toString().trim();
                String strMessage = etMessage.getText().toString().trim();
                contactRequest(strName, strPhoneNumber, strEmail, strMessage);
            } else {
                UaeBarqUtils.getAppUtils(this).showToast(this, getResources().getString(R.string.please_check_internet));
            }
        }
    }


    @OnClick(R.id.ll_unsub)
    void unsubClick() {

        pLayout.setVisibility(View.VISIBLE);
        //getUnsubscribeErrorMessage();
        openAppUpdateDialog();
    }


    private void contactRequest(String strName, String strPhoneNumber, String strEmail, String strMessage) {
        Call<ResponseBase> contactModel = ApplicationData.getInstance().getRestClient().createService(NetworkPostServices.class).
                postContactUsData(Constant.API_KEY, strName, strPhoneNumber, strMessage, strEmail,
                        UaeBarqUtils.getAppUtils(this).getGCMToken());
        contactModel.enqueue(new Callback<ResponseBase>() {
            @Override
            public void onResponse(Call<ResponseBase> call, Response<ResponseBase> response) {
                try {
                    llSocialMedia.setVisibility(View.VISIBLE);
                    llSend.setVisibility(View.VISIBLE);
                    pLayout.setVisibility(View.GONE);
                    scroll.setVisibility(View.VISIBLE);

                    if (response.body() != null && response.body().getSuccess().equalsIgnoreCase("true")) {
                        UaeBarqUtils.getAppUtils(getApplicationContext()).showToast(getApplicationContext(), response.body().getMessage());
                        etName.setText("");
                        etPhoneNumber.setText("");
                        etEmail.setText("");
                        etMessage.setText("");
                    } else {
                        UaeBarqUtils.getAppUtils(getApplicationContext()).showToast(getApplicationContext(), response.body().getMessage());
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBase> call, Throwable t) {
                llSocialMedia.setVisibility(View.VISIBLE);
                llSend.setVisibility(View.VISIBLE);
                pLayout.setVisibility(View.GONE);
                scroll.setVisibility(View.VISIBLE);
            }
        });
    }

    public boolean isValidate() {
        boolean valid = true;
        String strName = etName.getText().toString().trim();
        String strEmail = etEmail.getText().toString().trim();
        String strPhoneNumber = etPhoneNumber.getText().toString().trim();
        String strMessage = etMessage.getText().toString().trim();

        if (isNullOrEmpty(strName)) {
            UaeBarqUtils.getAppUtils(this).showToast(this, getResources().getString(R.string.enter_form_name));
            valid = false;
        } else if (isNullOrEmpty(strEmail)) {
            UaeBarqUtils.getAppUtils(this).showToast(this, getResources().getString(R.string.enter_email_address));
            valid = false;
        } else if (!UaeBarqUtils.getAppUtils(this).isEmailValid(strEmail)) {
            UaeBarqUtils.getAppUtils(this).showToast(this, getResources().getString(R.string.enter_valid_email));
            valid = false;
        } else if (isNullOrEmpty(strPhoneNumber)) {
            UaeBarqUtils.getAppUtils(this).showToast(this, getResources().getString(R.string.enter_form_phone));
            valid = false;
        } else if (isNullOrEmpty(strMessage)) {
            UaeBarqUtils.getAppUtils(this).showKeyBoard(etMessage, this);
            UaeBarqUtils.getAppUtils(this).showToast(this, getResources().getString(R.string.enter_message));
            valid = false;
        }
        return valid;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @OnClick(R.id.ll_fb)
    void facebook() {
        Utils.trackEvent(this.getClass().getSimpleName(), FireBaseEvents.contactUsFacebookClick);
        Intent facebookIntent = new Intent(Intent.ACTION_VIEW);
        String facebookUrl = UaeBarqUtils.getAppUtils(this).openFacebookPage(this);
        facebookIntent.setData(Uri.parse(facebookUrl));
        startActivity(facebookIntent);
    }

    @OnClick(R.id.ll_tw)
    void twitter() {
        Utils.trackEvent(this.getClass().getSimpleName(), FireBaseEvents.contactUsTwitterClick);
       UaeBarqUtils.getAppUtils(this).openTwitterPage(this);
    }

    @OnClick(R.id.ll_inst)
    void instagram() {

        Utils.trackEvent(this.getClass().getSimpleName(), FireBaseEvents.contactUsInstagaramClick);
        UaeBarqUtils.getAppUtils(this).openInstagramPage(Constant.instagramLink, this);
    }


    @OnClick(R.id.iv_back)
    void setImageClick() {
        finish();
    }

    @Override
    protected int setLayoutId() {
        return R.layout.contact_us_layout;
    }

    public void getissub(String msisdn) {
        {
            NetworkPostServices mServiceotp = ApiUtils2.getAPIServiceotp();
            Log.v("Function", "verify");


            mServiceotp = ApiUtils2.getAPIServiceotp();
            mServiceotp.getsubIS(msisdn).enqueue(new Callback<OtpModel>() {
                @Override
                public void onResponse(Call<OtpModel> call, retrofit2.Response<OtpModel> response) {

                    pLayout.setVisibility(View.GONE);
                    if (response.isSuccessful()) {
                        OtpModel model = response.body();
                        if (model != null) {
                            if (model.getStatus() == 0) {
                                ll_unsub.setVisibility(View.INVISIBLE);

                            } else {

                                ll_unsub.setVisibility(View.VISIBLE);
                            }

                        } else {
                            Toast.makeText(getApplicationContext(),
                                    getResources().getString(R.string.alreadyUsedCoupon),
                                    Toast.LENGTH_LONG)
                                    .show();
                        }
                    } else {

                    }
                }


                @Override
                public void onFailure(Call<OtpModel> call, Throwable t) {
                    pLayout.setVisibility(View.GONE);
                    showErrorMessage();
                }

            });

        }
    }
    private void showErrorMessage() {

        String error = getResources().getString(R.string.errormessage);
        Toast.makeText(this, error, Toast.LENGTH_SHORT).show();
    }
    public void getUnsubscribeErrorMessage() {
        String mobileNumber = SharedPreferenceManager.getSharedInstance().getStringFromPreferances(AppConstants.SharedPreferenceKeys.PHONE_NUMBER.getKey());
        //mobileNumber="020303030303";
        int action = 4;
        int service = 5;
        String reference = "appUNSUB";
        Call<UnSubcribeErrorModel> unSubcribeErrorModelCall = ApplicationData.getInstance().getRestClient().createService(NetworkPostServices.class)
                .getErrorMessage("http://mybarq.com/tpay/", action, service, reference, mobileNumber);



        unSubcribeErrorModelCall.enqueue(new Callback<UnSubcribeErrorModel>() {
            @Override
            public void onResponse(Call<UnSubcribeErrorModel> call, Response<UnSubcribeErrorModel> response) {
                try {
                    pLayout.setVisibility(View.GONE);
                    if (response.isSuccessful()) {
                        //  Toast.makeText(ContactUS.this, "Thats=" + response.body().getErrorMsg(), Toast.LENGTH_SHORT).show();

                        if (response.body().isSucess()) {
                            Toast.makeText(ContactUsActivity.this, "" + getResources().getString(R.string.unsub), Toast.LENGTH_SHORT).show();
                            ll_unsub.setVisibility(View.INVISIBLE);

                            // finish();

                        } else {
                            Toast.makeText(ContactUsActivity.this, "" + response.body().getErrorMsg(), Toast.LENGTH_SHORT).show();


                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<UnSubcribeErrorModel> call, Throwable t) {
                pLayout.setVisibility(View.GONE);
            }
        });
    }


    public void openAppUpdateDialog() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setTitle(getResources().getString(R.string.unsub));
        alertDialogBuilder.setMessage(getResources().getString(R.string.unsub_text));
        alertDialogBuilder.setPositiveButton(getResources().getString(R.string.dialog_yes), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface arg0, int arg1) {
                final String appPackageName = getPackageName();
                try {
                    pLayout.setVisibility(View.VISIBLE);
                   getUnsubscribeErrorMessage();
                } catch (android.content.ActivityNotFoundException anfe) {
                   // startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                }
                arg0.dismiss();
            }
        });
        alertDialogBuilder.setNegativeButton(getResources().getString(R.string.no), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface arg0, int arg1) {

                arg0.dismiss();
            }
        });
        alertDialog = alertDialogBuilder.create();
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setOnKeyListener(new DialogInterface.OnKeyListener() {
            @Override
            public boolean onKey(DialogInterface arg0, int keyCode, KeyEvent event) {
                // TODO Auto-generated method stub
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    if(alertDialog !=null && alertDialog.isShowing()){
                        alertDialog.dismiss();
                    }
                    finish();
                }
                return true;
            }
        });
        alertDialog.show();
    }

}



