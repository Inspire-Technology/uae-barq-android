package com.uaebarq.uaebarq;

import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.uaebarq.uaebarq.preferences.AppConstants;
import com.uaebarq.uaebarq.preferences.SharedPreferenceManager;
import com.uaebarq.uaebarq.utils.Constant;
import com.uaebarq.uaebarq.utils.FireBaseEvents;
import com.uaebarq.uaebarq.utils.UaeBarqUtils;
import com.uaebarq.uaebarq.utils.Utils;

import butterknife.BindView;
import butterknife.OnClick;

public class TutorialActivity extends BaseActivity {

    @BindView(R.id.fr_main)
    FrameLayout frMain;
    @BindView(R.id.iv_image)
    ImageView ivImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        super.onCreate(savedInstanceState);
        if (isEnglishLanguage()) {
            UaeBarqUtils.FontUtils.applyFonts(this, Constant.strTypefaceEnglishRegular, frMain);
            ivImage.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.tutorial_text_new));
        } else {
            UaeBarqUtils.FontUtils.applyFonts(this, Constant.strTypefaceArabicMedium, frMain);
            ivImage.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.tutorial_text_ar));
        }
    }

    @Override
    protected int setLayoutId() {
        return R.layout.tutorial;
    }

    @OnClick(R.id.rl_understand)
    public void Click(){
        Utils.trackEvent(this.getClass().getSimpleName(), FireBaseEvents.tutorialScreenUnderstandClick);
        SharedPreferenceManager.getSharedInstance().setBooleanInPreferences(AppConstants.SharedPreferenceKeys.SAVE_TUTORIAL.getKey(), true);
        finish();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}