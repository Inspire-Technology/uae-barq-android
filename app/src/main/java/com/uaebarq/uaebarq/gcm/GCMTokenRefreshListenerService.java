package com.uaebarq.uaebarq.gcm;

import android.content.Intent;

import com.google.android.gms.iid.InstanceIDListenerService;
import com.uaebarq.uaebarq.utils.FireBaseEvents;
import com.uaebarq.uaebarq.utils.Utils;

public class GCMTokenRefreshListenerService extends InstanceIDListenerService {
    @Override
    public void onTokenRefresh() {

        Utils.trackEvent(this.getClass().getSimpleName(), FireBaseEvents.gcmrefresh);
        Intent intent = new Intent(this, GCMRegistrationIntentService.class);
        startService(intent);
    }
}