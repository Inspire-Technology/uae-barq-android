package com.uaebarq.uaebarq.gcm;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.ContextCompat;

import com.google.android.gms.gcm.GcmListenerService;
import com.uaebarq.uaebarq.HomeActivity;
import com.uaebarq.uaebarq.R;
import com.uaebarq.uaebarq.utils.FireBaseEvents;
import com.uaebarq.uaebarq.utils.UaeBarqUtils;
import com.uaebarq.uaebarq.utils.Utils;


public class GCMPushReceiverService extends GcmListenerService {

    private String postId;
    private String premium;

    private String CHANNEL_ID = "latest_id";
    private String CHANNEL_NAME = "latest_name";
    private String TAG = "verifyNotification";


    @Override
    public void onMessageReceived(String from, final Bundle data) {
        String type = null, categoryId = null;
        String message = null;
        if (data.containsKey("type")) {
            type = data.getString("type");
        }
        if (data.containsKey("message")) {
            message = data.getString("message");
        }
        if (data.containsKey("category_id")) {
            categoryId = data.getString("category_id");
        }
        if (data.containsKey("post_id")) {
            postId = data.getString("post_id");
            categoryId = postId;
        }
        if (data.containsKey("premium")) {
            premium = data.getString("premium");
        }
        sendNotification(type, message, categoryId, premium);
    }

    private void sendNotification(String type, String message, String categoryId, String premium) {

        Utils.trackEvent(this.getClass().getSimpleName(), FireBaseEvents.notification);
        Intent intent;
        intent = new Intent(this, HomeActivity.class);
        if (type.equalsIgnoreCase("Generic")) {
            intent.putExtra("Generic", true);
            intent.putExtra("message", message);

        } else if (type.equalsIgnoreCase("Category")) {
            intent.putExtra("Category", true);
            intent.putExtra("categoryId", categoryId);
        }
        if (!UaeBarqUtils.getAppUtils(getApplicationContext()).isNullOrEmpty(postId)) {
            if (type.equalsIgnoreCase("text")) {
                intent.putExtra("text", true);

            } else if (type.equalsIgnoreCase("image")) {
                intent.putExtra("image", true);

            } else if (type.equalsIgnoreCase("video")) {
                intent.putExtra("video", true);
            }
            intent.putExtra("type", type);
            intent.putExtra("message", message);
            intent.putExtra("categoryId", categoryId);
            intent.putExtra("premium", premium);
        }

        NotificationManager mNotifyMgr = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel mChannel = new NotificationChannel(CHANNEL_ID, CHANNEL_NAME, NotificationManager.IMPORTANCE_HIGH);
            mChannel.setDescription(message);
            mChannel.setBypassDnd(true);
            mChannel.enableLights(true);
            mChannel.enableVibration(true);
            mNotifyMgr.createNotificationChannel(mChannel);
        }

        NotificationCompat.BigTextStyle bigText = new NotificationCompat.BigTextStyle();
        bigText.bigText(message);

        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        int id = (int) (System.currentTimeMillis() / 1000);
        NotificationCompat.Builder notification = new NotificationCompat.Builder(this, CHANNEL_ID)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher))
                .setContentText(message)
                .setAutoCancel(true)
                .setContentIntent(PendingIntent.getActivity(getBaseContext(), id, intent, PendingIntent.FLAG_UPDATE_CURRENT))
                .addAction(0, "", PendingIntent.getActivity(getBaseContext(), id + 1, intent, PendingIntent.FLAG_ONE_SHOT))
                .setCategory(Notification.CATEGORY_STATUS)
                .setVibrate(new long[]{200, 100, 200})
                .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                .setLights(Color.parseColor("#00BFA5"), 500, 500)
                .setStyle(bigText)
                .setColor(ContextCompat.getColor(getBaseContext(), R.color.colorAccent));

        Notification built = notification.build();
        built.flags |= Notification.FLAG_NO_CLEAR | Notification.FLAG_ONGOING_EVENT;
        mNotifyMgr.notify(TAG, 69420666, built);
    }
}