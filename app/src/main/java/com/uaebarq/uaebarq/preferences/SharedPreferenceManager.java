package com.uaebarq.uaebarq.preferences;

import android.content.Context;
import android.content.SharedPreferences;

import com.uaebarq.uaebarq.models.DashBoardModel.DashboardModel;
import com.uaebarq.uaebarq.models.FetchSettingsModel.FetchSettingsModel;
import com.uaebarq.uaebarq.models.InstagramModel.InstagramModel;
import com.uaebarq.uaebarq.models.TwitterModel.TwitterModel;
import com.uaebarq.uaebarq.models.YouTubeModel.YouTubeModel;
import com.google.gson.Gson;

public class SharedPreferenceManager {

    private static final String EMPTY_STRING = "";
    private static SharedPreferenceManager instance;
    public SharedPreferences sharedPreferences;
    private String storeSharedPreferences = "ae";

    /**
     * Create a shared instance of the class
     *
     * @return instance of the class
     */
    public static SharedPreferenceManager getSharedInstance() {
        if (instance == null)
            instance = new SharedPreferenceManager();
        return instance;
    }

    /**
     * Default Constructor to register Observables
     */
    private SharedPreferenceManager() {
        super();
    }

    /**
     * Method to initiate Shared Preferences
     */
    public void initiateSharedPreferences(Context context) {
//        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        sharedPreferences = context.getSharedPreferences(storeSharedPreferences, Context.MODE_PRIVATE);
    }

    /**
     * Method to set string in preferences
     *
     * @param key
     * @param value
     * @return editor commit
     */
    public boolean setStringInPreferences(String key, String value) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(key, value);
        return editor.commit();
    }

    public boolean setIntInPreferences(String key, int value) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt(key, value);
        return editor.commit();
    }

    /**
     * Method to get string from preferences
     *
     * @param key
     * @return data
     */
    public String getStringFromPreferances(String key) {
        String data = sharedPreferences.getString(key, EMPTY_STRING);
        return data;
    }

    public int getIntFromPreferences(String key) {
        int data = sharedPreferences.getInt(key, 0);
        return data;
    }

//    /**
//     * Method to set int in preferences
//     *
//     * @param key
//     * @param value
//     * @return editor commit
//     */
//    public boolean setIntInPreferences(String key, int value) {
//        SharedPreferences.Editor editor = sharedPreferences.edit();
//        editor.putInt(key, value);
//        return editor.commit();
//    }
//
//    /**
//     * Method to get int from preferences
//     *
//     * @param key
//     * @return data
//     */
//    public int getIntFromPreferences(String key) {
//        int data = sharedPreferences.getInt(key, -1);
//        return data;
//    }

//    /**
//     * Method to set double in preferences
//     *
//     * @param key
//     * @param value
//     * @return editor commit
//     */
//    public boolean setDoubleInPreferences(String key, Double value) {
//        SharedPreferences.Editor editor = sharedPreferences.edit();
//        editor.putLong(key, Double.doubleToLongBits(value));
//        return editor.commit();
//    }
//
//    /**
//     * Method to get double from preferences
//     *
//     * @param key
//     * @return data
//     */
//    public Double getDoubleFromPreferences(String key) {
//        Double data = Double.longBitsToDouble(sharedPreferences.getLong(key, Double.doubleToLongBits(-1.0)));
//        return data;
//    }

    /**
     * Method to set boolean in preferences
     *
     * @param key
     * @param value
     * @return editor commit
     */
    public boolean setBooleanInPreferences(String key, boolean value) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(key, value);
        return editor.commit();
    }

    /**
     * Method to get boolean from preferences
     *
     * @param key
     * @return data
     */
    public boolean getBooleanFromPreferences(String key) {
        boolean data = sharedPreferences.getBoolean(key, false);
        return data;
    }

    /**
     * Method to set long in preferences
     *
     * @param key
     * @param value
     * @return data
     */
    public boolean setLongInPreferences(String key, long value) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putLong(key, value);
        return editor.commit();
    }

    /**
     * Method to get long from preferences
     *
     * @param key
     * @return data
     */
    public long getLongFromPreferences(String key) {
        long data = sharedPreferences.getLong(key, -1);
        return data;
    }

//    /**
//     * Method to set float in preferences
//     *
//     * @param key
//     * @param value
//     * @return data
//     */
//    public boolean setFloatInPreferences(String key, float value) {
//        SharedPreferences.Editor editor = sharedPreferences.edit();
//        editor.putFloat(key, value);
//        return editor.commit();
//    }
//
//    /**
//     * Method to get float from preferences
//     *
//     * @param key
//     * @return data
//     */
//    public float getFloatFromPreferences(String key) {
//        float data = sharedPreferences.getFloat(key, -1.0f);
//        return data;
//    }

//    /**
//     * Method to delete data on specific key from preferences
//     *
//     * @param key
//     * @return data
//     */
//    public boolean deleteDataFromPreferences(String key) {
//        SharedPreferences.Editor editor = sharedPreferences.edit();
//        editor.remove(key);
//        return editor.commit();
//    }

    /**
     * Removing all preferences.
     */

    public void clearUserModelFromPreferences() {
        SharedPreferences.Editor editor = sharedPreferences.edit().clear();
        String model = getStringFromPreferances(AppConstants.SharedPreferenceKeys.DASHBOARD_MODEL.getKey());
        //String pass = getStringFromPreferances(AppConstants.SharedPreferenceKeys.PASS.getKey());
        //editor.remove(pass);
        editor.remove(model);
        editor.commit();
    }

    public boolean setFloatInPreferences(String key, float value) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putFloat(key, value);
        return editor.commit();
    }

    public float getFloatFromPreferences(String key) {
        float data = sharedPreferences.getFloat(key, -1.0f);
        return data;
    }

    public void saveDashBoardModel(DashboardModel dashboardModel) {
        if (dashboardModel == null)
            return;
        Gson gson = new Gson();
        String json = gson.toJson(dashboardModel);
        setStringInPreferences(AppConstants.SharedPreferenceKeys.DASHBOARD_MODEL.getKey(), json.toString());
    }

    public DashboardModel getDashBoardModelFromPreferences() {
        DashboardModel goalsModel = null;
        Gson gson = new Gson();
        String model = getStringFromPreferances(AppConstants.SharedPreferenceKeys.DASHBOARD_MODEL.getKey());
        goalsModel = gson.fromJson(model, DashboardModel.class);
        return goalsModel;
    }

    public void saveYouTubeModel(YouTubeModel youTubeModel) {
        if (youTubeModel == null)
            return;
        Gson gson = new Gson();
        String json = gson.toJson(youTubeModel);
        setStringInPreferences(AppConstants.SharedPreferenceKeys.YOUTUBE_MODEL.getKey(), json.toString());
    }

    public YouTubeModel getYouTubeModelFromPreferences() {
        YouTubeModel youTubeModel = null;
        Gson gson = new Gson();
        String model = getStringFromPreferances(AppConstants.SharedPreferenceKeys.YOUTUBE_MODEL.getKey());
        youTubeModel = gson.fromJson(model, YouTubeModel.class);
        return youTubeModel;
    }

    public void saveTwitterModel(TwitterModel twitterModel) {
        if (twitterModel == null)
            return;
        Gson gson = new Gson();
        String json = gson.toJson(twitterModel);
        setStringInPreferences(AppConstants.SharedPreferenceKeys.TWITTER_MODEL.getKey(), json.toString());
    }

    public TwitterModel getTwitterModelFromPreferences() {
        TwitterModel twitterModel = null;
        Gson gson = new Gson();
        String model = getStringFromPreferances(AppConstants.SharedPreferenceKeys.TWITTER_MODEL.getKey());
        twitterModel = gson.fromJson(model, TwitterModel.class);
        return twitterModel;
    }

    public void saveInstagramModel(InstagramModel instagramModel) {
        if (instagramModel == null)
            return;
        Gson gson = new Gson();
        String json = gson.toJson(instagramModel);
        setStringInPreferences(AppConstants.SharedPreferenceKeys.INSTAGRAM_MODEL.getKey(), json.toString());
    }

    public InstagramModel getInstagramModelFromPreferences() {
        InstagramModel instagramModel = null;
        Gson gson = new Gson();
        String model = getStringFromPreferances(AppConstants.SharedPreferenceKeys.INSTAGRAM_MODEL.getKey());
        instagramModel = gson.fromJson(model, InstagramModel.class);
        return instagramModel;
    }

    public FetchSettingsModel getSettingsModelFromPreferences() {
        FetchSettingsModel fetchSettingsModel;
        Gson gson = new Gson();
        String model = getStringFromPreferances(AppConstants.SharedPreferenceKeys.SETTINGS_MODEL.getKey());
        fetchSettingsModel = gson.fromJson(model, FetchSettingsModel.class);
        return fetchSettingsModel;
    }

    public void saveSettingsModel(FetchSettingsModel fetchSettingsModel) {
        if (fetchSettingsModel == null)
            return;
        Gson gson = new Gson();
        String json = gson.toJson(fetchSettingsModel);
        setStringInPreferences(AppConstants.SharedPreferenceKeys.SETTINGS_MODEL.getKey(), json.toString());
    }
    public void savePhoneNumber(String  phoneNumber) {
        if (phoneNumber == null)
            return;

        setStringInPreferences(AppConstants.SharedPreferenceKeys.PHONE_NUMBER.getKey(),phoneNumber);
    }

    public String getPhoneNumber() {


      return   getStringFromPreferances(AppConstants.SharedPreferenceKeys.PHONE_NUMBER.getKey());
    }
}