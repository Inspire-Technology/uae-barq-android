package com.uaebarq.uaebarq.preferences;

public class AppConstants {

    public enum SharedPreferenceKeys {

        DASHBOARD_MODEL("dashBoardModel"),
        SPLASH_SCREEN("shown"),
        YOUTUBE_MODEL("youTubeModel"),
        TWITTER_MODEL("twitterModel"),
        INSTAGRAM_MODEL("instagramModel"),
        PREMIUM("is_premium"),
        DASHBOARD_REFRESH_DB_TIME("refreshTime"),
        GCM_TOKEN("gcm_token"),
        ACTIVE_SMS("active"),
        USER_ID("user_id"),
        PREMIUM_MODEL("premium_model"),
        SDK("is_sdk"),
        SAVE_TUTORIAL("save_tutorial"),
        SETTINGS_MODEL("settings"),
        PHONE_NUMBER("phone_number"),;;

        private String value;

        SharedPreferenceKeys(String value) {
            this.value = value;
        }

        public String getKey() {
            return value;
        }
    }
}