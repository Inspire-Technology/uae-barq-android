package com.uaebarq.uaebarq;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.github.silvestrpredko.dotprogressbar.DotProgressBar;
import com.uaebarq.uaebarq.models.Coupon.CouponModel;
import com.uaebarq.uaebarq.payment.InAppProductActivity;
import com.uaebarq.uaebarq.preferences.SharedPreferenceManager;
import com.uaebarq.uaebarq.retrofit.NetworkInterface.NetworkPostServices;
import com.uaebarq.uaebarq.utils.Constant;
import com.uaebarq.uaebarq.utils.FireBaseEvents;
import com.uaebarq.uaebarq.utils.UaeBarqUtils;
import com.uaebarq.uaebarq.utils.Utils;

import butterknife.BindView;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.uaebarq.uaebarq.ApplicationData.image;
import static com.uaebarq.uaebarq.ApplicationData.text;
import static com.uaebarq.uaebarq.ApplicationData.video;

public class PremiumCodeActivity extends BaseActivity {

    @BindView(R.id.et_code)
    EditText etCode;
    private Call<CouponModel> couponCodeCall;

    @BindView(R.id.dot_progress_bar)
    DotProgressBar dotProgressBar;
    @BindView(R.id.ll_congratx)
    View llCongrats;
    @BindView(R.id.progress_bar)
    View progressBar;
    @BindView(R.id.first_view)
    LinearLayout first_view;
    boolean fromNotification;
    private String videoId;
    @BindView(R.id.ll_main)
    View ll_main;
    @BindView(R.id.tv_premium)
    TextView tv_premium;
 /*   @BindView(R.id.ll_inapp)
    RelativeLayout ll_inapp;*/
    boolean isTakeMargin = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            Window w = getWindow();
            w.setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
        }
        super.onCreate(savedInstanceState);
     /*   if (isAndroidSDK()) {
            ll_inapp.setVisibility(View.VISIBLE);
        } else {
            ll_inapp.setVisibility(View.GONE);
        }*/
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            fromNotification = bundle.getBoolean("fromNotification");
            videoId = bundle.getString("videoId");
        }

        dotProgressBar.setEndColor(ContextCompat.getColor(this, R.color.red_coupn));
        dotProgressBar.setStartColor(ContextCompat.getColor(this, R.color.red_coupn));
        if (isEnglishLanguage()) {
            UaeBarqUtils.FontUtils.applyFonts(this, Constant.strTypefaceEnglishRegular, ll_main);
            UaeBarqUtils.FontUtils.applyFonts(this, Constant.strTypefaceEnglishMedium, tv_premium);
        } else {
            UaeBarqUtils.FontUtils.applyFonts(this, Constant.strTypefaceArabicRegular, ll_main);
            UaeBarqUtils.FontUtils.applyFonts(this, Constant.strTypefaceArabicMedium, tv_premium);
        }

        etCode.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                setViewHeight(first_view, isTakeMargin);
                UaeBarqUtils.getAppUtils(getApplicationContext()).showKeyBoard(etCode, getApplicationContext());
                return true;
            }
        });

        etCode.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int actionId, KeyEvent keyEvent) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    isTakeMargin = true;
                    UaeBarqUtils.getAppUtils(getApplicationContext()).hideKeyBoard(etCode, getApplicationContext());
                    setViewHeight(first_view, isTakeMargin);
                    return true;
                }
                return false;
            }
        });
    }


    public void setViewHeight(LinearLayout layout, boolean isTrue) {
        FrameLayout.LayoutParams relativeParams = null;
        if (isTrue) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                relativeParams = new FrameLayout.LayoutParams(new LinearLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT,
                        FrameLayout.LayoutParams.WRAP_CONTENT));
            }
            relativeParams.gravity = Gravity.CENTER;
            relativeParams.setMargins(0, 0, 0, 0);
        } else {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                relativeParams = new FrameLayout.LayoutParams(new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT,
                        FrameLayout.LayoutParams.WRAP_CONTENT));
            }
            relativeParams.setMargins(0, 0, 0, 10);
        }
        layout.setLayoutParams(relativeParams);
        layout.requestLayout();
    }

    @OnClick(R.id.iv_cross_image)
    void crossClick() {
        setBackData();
        finish();
    }

    private void setBackData() {
        text = false;
        image = false;
        video = false;
        if (fromNotification) {
            //  intentForHomeScreen();
            return;
        } else if (!isNullOrEmpty(videoId) && fromNotification) {
            setIntent();
        }
        setResult(RESULT_OK);
    }

    @OnClick(R.id.ll_submit)
    void submitClick() {
        if (isValidate()) {
            if (isOnline()) {

                Utils.trackEvent(this.getClass().getSimpleName(), FireBaseEvents.premiumScreenSubmitButton);
                isTakeMargin = true;
                UaeBarqUtils.getAppUtils(getApplicationContext()).hideKeyBoard(etCode, getApplicationContext());
                setViewHeight(first_view, isTakeMargin);
                progressBar.setVisibility(View.VISIBLE);
                UaeBarqUtils.getAppUtils(this).hideKeyBoard(etCode, this);
                String code = etCode.getText().toString().trim();
                postUserCouponCode(code);
            } else {
                UaeBarqUtils.getAppUtils(this).showToast(this, getResources().getString(R.string.please_check_internet));
            }
        }
    }

    public boolean isValidate() {
        boolean valid = true;
        String strCode = etCode.getText().toString().trim();
        if (isNullOrEmpty(strCode)) {
            UaeBarqUtils.getAppUtils(this).showKeyBoard(etCode, this);
            UaeBarqUtils.getAppUtils(this).showToast(this, getResources().getString(R.string.enter_coupon_code));
            valid = false;
        }
        return valid;
    }

    private void postUserCouponCode(String code) {
        couponCodeCall = ApplicationData.getRestClient().createService(NetworkPostServices.class)
                .postCouponCode(getUserId(),
                        code,
                        UaeBarqUtils.getAppUtils(this).getCurrentLang(this),
                        Constant.API_KEY);
        couponCodeCall.enqueue(new Callback<CouponModel>() {
            @Override
            public void onResponse(Call<CouponModel> call, Response<CouponModel> response) {
                try {
                    progressBar.setVisibility(View.GONE);
                    if (response.body() != null && response.body().getSuccess().equalsIgnoreCase("true")) {
                        llCongrats.setVisibility(View.VISIBLE);
                        first_view.setVisibility(View.GONE);

                        String phoneNumber=response.body().getPayload().getMsisdn();
                        if(phoneNumber!=null)
                        {
                            SharedPreferenceManager.getSharedInstance().savePhoneNumber(phoneNumber);
                        }


                        setPremiumUser(true);
                        return;
                    } else {
                        UaeBarqUtils.getAppUtils(getApplicationContext()).showToast(getApplicationContext(), response.body().getMessage());
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    progressBar.setVisibility(View.GONE);
                }
            }

            @Override
            public void onFailure(Call<CouponModel> call, Throwable t) {
                progressBar.setVisibility(View.GONE);
                UaeBarqUtils.getAppUtils(getApplicationContext()).showToast(getApplicationContext(), t.getMessage().toString());
            }
        });
    }

    @OnClick(R.id.ll_contact)
    void contactClick() {
        Utils.trackEvent(this.getClass().getSimpleName(), FireBaseEvents.premiumScreenContactUs);
        Intent intent = new Intent(PremiumCodeActivity.this, ContactUsActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.ll_no_premium)
    void premiumClick() {
        Utils.trackEvent(this.getClass().getSimpleName(), FireBaseEvents.premiumScreenOpenInAppProduct);
        Intent intent = new Intent(PremiumCodeActivity.this, InAppProductActivity.class);
        startActivityForResult(intent, Constant.REQUEST_CODE_PREM);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Constant.REQUEST_CODE_PREM) {
            if (resultCode == RESULT_OK) {
                if (!isNullOrEmpty(videoId) && fromNotification) {
                    setIntent();
                } else {
                    intentForHomeScreen();
                }
                finish();
            }
        }
        if (requestCode == Constant.BILLING_CODE) {
            if (resultCode == RESULT_OK) {
                ApplicationData.getInstance().fromSDK = true;
            }
            finish();
        }
    }

    public void intentForHomeScreen() {
        Utils.trackEvent(this.getClass().getSimpleName(), FireBaseEvents.premiumScreenHomeActivity);
        Intent intent = new Intent(this, HomeActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        finish();
    }

    void setIntent() {
        Utils.trackEvent(this.getClass().getSimpleName(), FireBaseEvents.premiumScreenHomeActivity);
        Intent intent = new Intent(this, HomeActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra("fromNotification", fromNotification);
        intent.putExtra("videoId", videoId);
        startActivity(intent);
        finish();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        setBackData();
        finish();
    }

    @OnClick(R.id.ll_ok)
    void okClick() {

        Utils.trackEvent(this.getClass().getSimpleName(), FireBaseEvents.premiumScreenOKClick);
        setBackData();
        finish();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        fetchAppSettings();
    }

    @Override
    protected int setLayoutId() {
        return R.layout.activity_premium_code;
    }
}
